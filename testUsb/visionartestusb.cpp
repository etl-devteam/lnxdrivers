#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <fstream>

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "alog.h"


#define USB_INPUT   "/dev/usb/VisionAR1"
#define IMUACC      "/sys/class/usbmisc/VisionAR1/device/imuacc"
#define IMUGYRO     "/sys/class/usbmisc/VisionAR1/device/imugyro"
#define IMUMAG      "/sys/class/usbmisc/VisionAR1/device/imumag"
#define HAPTIC      "/sys/class/usbmisc/VisionAR1/device/haptic"
#define VERSION     "/sys/module/visionarusb/version"
#define VERBOSE     "/sys/module/visionarusb/parameters/verbose"

using namespace std;

#ifdef ALOG_DEBUG
static const char *TAG = "TESTUSB";
static int aLogLevel = ALOG_INFO_LEVEL;
#endif

struct swversion {
        unsigned char protocol_major;
	unsigned char protocol_minor;
	unsigned char boot_major;
	unsigned char boot_minor;
	unsigned char appl_major;
	unsigned char appl_minor;
	unsigned char appl_patch;
	unsigned char appl_day;
	unsigned char appl_month;
	unsigned char appl_year;
	unsigned char hw_revision;
} version;


void readImuAcc(void)
{
    int fp;
    char buffer[100];

    ALOGV("opening %s...", IMUACC);
    if ((fp = open(IMUACC, O_RDONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", IMUACC, strerror(errno));
    }
    else
    {
        if (read(fp, &buffer[0], sizeof(buffer)) > 0)
        {
            ALOGI("IMU ACC: %s", &buffer[0]);
        }
        else
        {
            ALOGE("Error: %s", strerror(errno));
        }

        ALOGV("closing %s...", IMUACC);
        close(fp);
    }
}

void readImuGyro(void)
{
    int fp;
    char buffer[100];

    ALOGV("opening %s...", IMUGYRO);
    if ((fp = open(IMUGYRO, O_RDONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", IMUGYRO, strerror(errno));
    }
    else
    {
        if (read(fp, &buffer[0], sizeof(buffer)) > 0)
        {
            ALOGI("IMU GYRO: %s", &buffer[0]);
        }
        else
        {
            ALOGE("Error: %s", strerror(errno));
        }

        ALOGV("closing %s...", IMUGYRO);
        close(fp);
    }
}

void readImuMag(void)
{
    int fp;
    char buffer[100];

    ALOGV("opening %s...", IMUMAG);
    if ((fp = open(IMUMAG, O_RDONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", IMUMAG, strerror(errno));
    }
    else
    {
        if (read(fp, &buffer[0], sizeof(buffer)) > 0)
        {
            ALOGI("IMU MAG: %s", &buffer[0]);
        }
        else
        {
            ALOGE("Error: %s", strerror(errno));
        }

        ALOGV("closing %s...", IMUMAG);
        close(fp);
    }
}

void writeHaptic(int ms)
{
    int fp;
    char buffer[100];

    ALOGV("opening %s...", HAPTIC);
    if ((fp = open(HAPTIC, O_WRONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", HAPTIC, strerror(errno));
    }
    else
    {
        snprintf(&buffer[0], sizeof(buffer), "%d", ms);
        if (write(fp, &buffer[0], sizeof(buffer)) > 0)
        {
            ALOGI("HAPTIC OK");
        }
        else
        {
            ALOGE("HAPTIC ERROR: %s", strerror(errno));
        }

        ALOGV("closing %s...", HAPTIC);
        close(fp);
    }
}

void readDriverVersion(void)
{
    int fp;
    char buffer[100];

    ALOGV("opening %s...", VERSION);
    if ((fp = open(VERSION, O_RDONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", VERSION, strerror(errno));
    }
    else
    {
        if (read(fp, &buffer[0], sizeof(buffer)) > 0)
        {
            ALOGI("Driver Version: %s", &buffer[0]);
        }
        else
        {
            ALOGE("Error: %s", strerror(errno));
        }

        ALOGV("closing %s...", VERSION);
        close(fp);
    }
}

void readVerbose(void)
{
    int fp;
    char buffer[100];

    ALOGV("opening %s...", VERBOSE);
    if ((fp = open(VERBOSE, O_RDONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", VERSION, strerror(errno));
    }
    else
    {
        if (read(fp, &buffer[0], sizeof(buffer)) > 0)
        {
            ALOGI("Log level: %s", &buffer[0]);
        }
        else
        {
            ALOGE("Error: %s", strerror(errno));
        }

        ALOGV("closing %s...", VERBOSE);
        close(fp);
    }
}

void readVersion(void)
{
    int fp;
    struct swversion version;

    ALOGV("opening %s...", USB_INPUT);
    if ((fp = open(USB_INPUT, O_RDONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", USB_INPUT, strerror(errno));
    }
    else
    {
        if (ioctl(fp, 0x81, &version) >= 0)
        {
            ALOGI("Protocol: %u.%u", (unsigned int)version.protocol_major, (unsigned int)version.protocol_minor);
            ALOGI("Boot: %u.%u", (unsigned int)version.boot_major, (unsigned int)version.boot_minor);
            ALOGI("Appl: %u.%u.%u (%02u/%02u/%02u) %c",
			    (unsigned int)version.appl_major, (unsigned int)version.appl_minor, (unsigned int)version.appl_patch,
			    (unsigned int)version.appl_day, (unsigned int)version.appl_month, (unsigned int)version.appl_year,
			    version.hw_revision);
        }
        else
        {
            ALOGE("Error: %s", strerror(errno));
        }

        ALOGV("closing %s...", USB_INPUT);
        close(fp);
    }
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    int counter = 10000;
    ALOGI("Start application...");

    writeHaptic(100);

    readDriverVersion();
    readVerbose();
    readVersion();
    while (counter--)
    {
        readImuAcc();
        readImuGyro();
        readImuMag();

        sleep(1);
    }

    ALOGI("Application stop!");

    return 0;
}

