PWD := $(shell pwd)

KLIB_PATH := /lib/modules/`uname -r`/build
KCU_PATH := /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/opt64/cdk-build/linux-4.1.15

USB_PATH := usb
FB_PATH := fb
TEST_PATH := testLib
TESTFB_PATH := testFb
TESTUSB_PATH := testUsb

LIB_PATH := libvisionarusb


obj-y := fb/ usb/


default: all

all:
	$(MAKE) -C $(KLIB_PATH) M=$(PWD) modules

clean:
	$(MAKE) -C $(KLIB_PATH) M=$(PWD) clean


cu:
	$(MAKE) -C $(KCU_PATH) M=$(PWD) modules \
		ARCH=arm CROSS_COMPILE=arm-SKDinema-linux-gnueabihf- LOADADDR=0x10008000 \
		CC=/home/dinex/Cdk/SKDinema.imx6/trunk/cdk/opt64/usr/bin/arm-SKDinema-linux-gnueabihf-gcc \
		LD=/home/dinex/Cdk/SKDinema.imx6/trunk/cdk/opt64/usr/bin/arm-SKDinema-linux-gnueabihf-ld


install: usb_install fb_install

usb_install: usb
	sudo make -C $(USB_PATH) M=$(PWD)/$< install

fb_install: fb
	sudo make -C $(FB_PATH) M=$(PWD)/$< install


uninstall: usb_uninstall fb_uninstall

usb_uninstall:
	sudo make -C $(USB_PATH) uninstall

fb_uninstall:
	sudo make -C $(FB_PATH) uninstall


lib:
	make -C $(LIB_PATH) all

lib_clean:
	make -C $(LIB_PATH) clean


testlib: lib
	make -C $(TEST_PATH) all

testlib_clean:
	make -C $(TEST_PATH) clean

testlib_run: testlib
	sudo make -C $(TEST_PATH) run


testfb:
	make -C $(TESTFB_PATH) all

testfb_clean:
	make -C $(TESTFB_PATH) clean

testfb_run: testfb
	make -C $(TESTFB_PATH) run


testusb:
	make -C $(TESTUSB_PATH) all

testusb_clean:
	make -C $(TESTUSB_PATH) clean

testusb_run: testusb
	make -C $(TESTUSB_PATH) run


.PHONY: all cu clean install uninstall
.PHONY: usb_install usb_uninstall
.PHONY: fb_install fb_uninstall
.PHONY: lib lib_clean testlib testlib_clean testlib_run testfb testfb_clean testfb_run testusb testusb_clean testusb_run
