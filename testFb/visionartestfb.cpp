#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <fstream>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "alog.h"


#define WIDTH       419
#define HEIGHT      138
#define FB_SIZE     (4*WIDTH*HEIGHT)

#define FB_INPUT    "/dev/fb1"
#define BRIGHTNESS  "/sys/class/graphics/fb1/brightness"
#define CONTRAST    "/sys/class/graphics/fb1/contrast"

using namespace std;

#ifdef ALOG_DEBUG
static const char *TAG = "TESTFB";
static int aLogLevel = ALOG_INFO_LEVEL;
#endif

static void BMP2Pixels(const char *filename, unsigned char *pixels)
{
    char header[14];
    unsigned int address = 0;
    char *buffer = new char [4*WIDTH];

    ifstream bmp (filename, ifstream::binary);
    if (bmp.is_open() == false)
    {
        ALOGE("error on opening %s", filename);
    }

    bmp.read(&header[0], sizeof(header));
    for (int i = 4; i>0; i--) {
        address <<= 8;
        address |= (header[10+i-1] & 0xFF);
    }
    bmp.seekg (address, bmp.beg);

    for (int row = 0; row < HEIGHT; row++)
    {
        bmp.read(buffer, 4*WIDTH);
        memcpy(&pixels[4*(HEIGHT-1-row)*WIDTH], buffer, 4*WIDTH);
    } /* for (row) */

    bmp.close();

    delete buffer;
}

static void fillPixels(int type, unsigned char *pel)
{
    int row, col;

    if (type == 1)
    {
        ALOGI("VisionAR FB Test > %s: %s", __func__, "UP");

        for (row = 0; row < HEIGHT/2; row++) {
            for (col = 0; col < WIDTH; col++) {
                pel[4*(row*WIDTH+col)+0] = 0xFF;
                pel[4*(row*WIDTH+col)+1] = 0xFF;
                pel[4*(row*WIDTH+col)+2] = 0xFF;
                pel[4*(row*WIDTH+col)+3] = 0xFF;
            }
        }
        for (row = HEIGHT/2; row < HEIGHT; row++) {
            for (col = 0; col < WIDTH; col++) {
                pel[4*(row*WIDTH+col)+0] = 0x00;
                pel[4*(row*WIDTH+col)+1] = 0x00;
                pel[4*(row*WIDTH+col)+2] = 0x00;
                pel[4*(row*WIDTH+col)+3] = 0x00;
            }
        }
    }
    else
    {
        ALOGI("VisionAR FB Test > %s: %s", __func__, "DOWN");

        for (row = 0; row < HEIGHT/2; row++) {
            for (col = 0; col < WIDTH; col++) {
                pel[4*(row*WIDTH+col)+0] = 0x00;
                pel[4*(row*WIDTH+col)+1] = 0x00;
                pel[4*(row*WIDTH+col)+2] = 0x00;
                pel[4*(row*WIDTH+col)+3] = 0x00;
            }
        }
        for (row = HEIGHT/2; row < HEIGHT; row++) {
            for (col = 0; col < WIDTH; col++) {
                pel[4*(row*WIDTH+col)+0] = 0xFF;
                pel[4*(row*WIDTH+col)+1] = 0xFF;
                pel[4*(row*WIDTH+col)+2] = 0xFF;
                pel[4*(row*WIDTH+col)+3] = 0xFF;
            }
        }
    }

}

void writeBrightness(int value)
{
    int fp;
    char buffer[100];

    ALOGV("opening %s...", BRIGHTNESS);
    if ((fp = open(BRIGHTNESS, O_WRONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", BRIGHTNESS, strerror(errno));
    }
    else
    {
        snprintf(&buffer[0], sizeof(buffer), "%d", value);
        if (write(fp, &buffer[0], sizeof(buffer)) > 0)
        {
            ALOGI("Brightness(%d): OK", value);
        }
        else
        {
            ALOGE("Brightness(%d) ERROR: %s", value, strerror(errno));
        }

        ALOGV("closing %s...", BRIGHTNESS);
        close(fp);
    }
}

void writeContrast(int value)
{
    int fp;
    char buffer[100];

    ALOGV("opening %s...", CONTRAST);
    if ((fp = open(CONTRAST, O_WRONLY)) < 0)
    {
        ALOGE("error on opening %s (%s)", CONTRAST, strerror(errno));
    }
    else
    {
        snprintf(&buffer[0], sizeof(buffer), "%d", value);
        if (write(fp, &buffer[0], sizeof(buffer)) > 0)
        {
            ALOGI("Contrast(%d): OK", value);
        }
        else
        {
            ALOGE("Contrast(%d) ERROR: %s", value, strerror(errno));
        }

        ALOGV("closing %s...", CONTRAST);
        close(fp);
    }
}

const char *images[] = {
    "../resources/ETL.bmp",
    "../resources/test.bmp",
    "../resources/visionar.bmp",
};


//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    static unsigned char pixels[4*WIDTH*HEIGHT];

    int fbFp = 0;
    char *fbMmap = nullptr;
    int index = 0;
    int contrast = 128;
    int brightness = 128;
    char buffer[100];

    ALOGI("opening %s...", FB_INPUT);
    if ((fbFp = open(FB_INPUT, O_RDWR)) < 0)
    {
        ALOGE("error on opening %s (%s)", FB_INPUT, strerror(errno));
        exit(1);
    }

    if ((fbMmap = (char *)mmap(0, FB_SIZE, PROT_WRITE, MAP_SHARED, fbFp, 0 )) == MAP_FAILED)
    {
        close(fbFp);
        ALOGE("failure on mmap opening (%s)", strerror(errno));
        exit(1);
    }

    if (fbMmap != nullptr)
    {
        while (1)
        {
            index = (index+1)%3;
            BMP2Pixels(images[index], pixels);
            memcpy ( fbMmap, pixels, FB_SIZE);

	    if (index%2)
	    {
		contrast = (contrast + 30) % 256;
		writeContrast(contrast);
	    }
	    else
	    {
		brightness = (brightness + 30) % 256;
		writeBrightness(brightness);
	    }

	    sleep(1);
        }
    }

    if (fbMmap != nullptr)
    {
        ALOGI("unmapping fb...");
        if (munmap(fbMmap, FB_SIZE) != 0)
        {
            ALOGE("error on munmap: %s", strerror(errno));
        }
    }

    if (fbFp > 0)
    {
        ALOGI("closing %s...", FB_INPUT);
        close(fbFp);
    }

    return 0;
}

