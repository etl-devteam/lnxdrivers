/**
 * VisionARUSBChannel class definition file
 *
 * VisionARUSBChannel class provides methods to transfer data via USB interface.
 * It finds required interface/endpoints and calls right transfer function,
 * USB interface type.
 * Moreover, it checks data transfers are performed one at time.
 * No checks about messages are performed by methods of this class.
 */

#include <cstring>
#include <libusb-1.0/libusb.h>

#include "alog.h"
#include "visionarusbchannel.h"


#if DEBUG
static const char *TAG = "VAR_CHN";     // used by ALOGx macros
#endif

static const int USB_TRANSFER_TIMEOUT = 100;



static int usb_transfer(libusb_device_handle *dev_handle, unsigned char ep, unsigned char attr, unsigned char *data, int length, int *transferred, unsigned int timeout)
{
    switch (attr & LIBUSB_TRANSFER_TYPE_MASK)
    {
        case LIBUSB_TRANSFER_TYPE_CONTROL:
            ALOGE("%s: ERROR: unexpected type (CONTROL)", __func__);
            return -1;
        case LIBUSB_TRANSFER_TYPE_ISOCHRONOUS:
            ALOGE("%s: ERROR: unexpected type (ISOCHRONOUS)", __func__);
            return -1;
        case LIBUSB_TRANSFER_TYPE_BULK:
            return libusb_bulk_transfer (dev_handle, ep, data, length, transferred, timeout);
        case LIBUSB_TRANSFER_TYPE_INTERRUPT:
            return libusb_interrupt_transfer (dev_handle, ep, data, length, transferred, timeout);
    }
}


VisionARUSBChannel::VisionARUSBChannel(unsigned char ifClass, unsigned char ifSubclass, unsigned char ifProt):
            mIfClass(ifClass), mIfSubclass(ifSubclass), mIfProt(ifProt),
            mIsOpened(false), mHandle(nullptr),
            mIfNumber(0), mEpAddrIn(0), mEpAttrIn(0), mEpAddrOut(0), mEpAttrOut(0)
{
    ALOGI("%s: ifClass = %02Xh, ifSubclass = %02Xh, ifProt = %02Xh", __func__, mIfClass, mIfSubclass, mIfProt);
}

VisionARUSBChannel::~VisionARUSBChannel()
{
    ALOGI("%s", __func__);
}

bool VisionARUSBChannel::open(libusb_device_handle *hndl, libusb_device *dev, unsigned char num)
{
    bool ret;

    int counterCfg, counterIf, counterAlt, counterEp;
    int rc = LIBUSB_ERROR_NOT_FOUND;
    struct libusb_config_descriptor *config;
    bool found = false;

    ALOGI("%s", __func__);

    mLock.lock();

    mHandle = hndl;

    for (counterCfg = 0; counterCfg < num; counterCfg++)
    {
        if ((rc = libusb_get_config_descriptor(dev, counterCfg, &config)) ==  LIBUSB_SUCCESS)
        {
            if (config->interface != nullptr)
            {
                for (counterIf = 0; counterIf < config->bNumInterfaces; counterIf++)
                {
                    for (counterAlt = 0; counterAlt < config->interface[counterIf].num_altsetting; counterAlt++)
                    {
                        if ((config->interface[counterIf].altsetting[counterAlt].bInterfaceClass == mIfClass)
                                && (config->interface[counterIf].altsetting[counterAlt].bInterfaceSubClass == mIfSubclass)
                                && (config->interface[counterIf].altsetting[counterAlt].bInterfaceProtocol == mIfProt)
                                )
                        {
                            mIfNumber = config->interface[counterIf].altsetting[counterAlt].bInterfaceNumber;
                            if (config->interface[counterIf].altsetting[counterAlt].endpoint != nullptr)
                            {
                                for (counterEp = 0; counterEp < config->interface[counterIf].altsetting[counterAlt].bNumEndpoints; counterEp++)
                                {
                                    if (config->interface[counterIf].altsetting[counterAlt].endpoint[counterEp].bEndpointAddress & LIBUSB_ENDPOINT_IN)
                                    {
                                        mEpAttrIn = config->interface[counterIf].altsetting[counterAlt].endpoint[counterEp].bmAttributes;
                                        mEpAddrIn = config->interface[counterIf].altsetting[counterAlt].endpoint[counterEp].bEndpointAddress;
                                    }

                                    if ((config->interface[counterIf].altsetting[counterAlt].endpoint[counterEp].bEndpointAddress & LIBUSB_ENDPOINT_IN) == 0)
                                    {
                                        mEpAttrOut = config->interface[counterIf].altsetting[counterAlt].endpoint[counterEp].bmAttributes;
                                        mEpAddrOut = config->interface[counterIf].altsetting[counterAlt].endpoint[counterEp].bEndpointAddress;
                                    }
                                } // endpoints cycle

                                ALOGI("%s: Endpoints found!", __func__);
                                rc = LIBUSB_SUCCESS;
                            }
                            else
                            {
                                ALOGE("%s: ERROR: No endpoints!", __func__);
                                rc = LIBUSB_ERROR_NOT_FOUND;
                            }

                            // No need to check other configurations
                            found = true;
                        }

                        if (found) break;
                    } // alt settings cycle

                    if (found) break;
                } // interfaces cycle
            }
            else
            {
                ALOGE("%s: ERROR: No interfaces!", __func__);
                rc = LIBUSB_ERROR_NOT_FOUND;
            }

            libusb_free_config_descriptor(config);
        }
        else
        {
            ALOGE("%s: ERROR on getting config descriptor (%s  - %s)", __func__, libusb_error_name(rc), libusb_strerror((libusb_error)rc));
        }

        if (found) break;
    } // configurations cycle

    if (mEpAddrIn || mEpAddrOut)
    {
        mIsOpened = true;
    }
    else
    {
        mIsOpened = false;
    }
    ret = mIsOpened;

    mLock.unlock();

    if (ret == false)
    {
        reset();
    }

    ALOGI("%s: %s", __func__, ((ret)?"OK":"FAIL"));

    return ret;
}

bool VisionARUSBChannel::close(void)
{
    reset();

    return false;
}

bool VisionARUSBChannel::reset(void)
{
    mLock.lock();
    mIsOpened = false;
    mHandle = nullptr;
    mIfNumber = 0;
    mEpAddrIn = 0;
    mEpAttrIn = 0;
    mEpAddrOut = 0;
    mEpAttrOut = 0;
    mLock.unlock();

    return false;
}

bool VisionARUSBChannel::status(void)
{
    bool ret;

    mLock.lock();
    ret = mIsOpened;
    mLock.unlock();

    return ret;
}

int VisionARUSBChannel::transfer(unsigned char *sndBuffer, int sndSize, unsigned char *rcvBuffer, int rcvSize)
{
    int rc;
    int nBytesTransferred;

   ALOGI("%s(%d,%d)", __func__, sndSize, rcvSize);

    mLock.lock();

    if (mIsOpened)
    {
        libusb_claim_interface(mHandle, mIfNumber);
        rc = usb_transfer(mHandle, mEpAddrOut, mEpAttrOut, sndBuffer, sndSize, &nBytesTransferred, USB_TRANSFER_TIMEOUT);
        if (rc != LIBUSB_SUCCESS)
        {
            ALOGE("%s(%02Xh): [OUT] USB transfer error : %s %s", __func__, mEpAddrOut, libusb_error_name(rc), libusb_strerror((libusb_error)rc));
            nBytesTransferred = -1;
        }
        else if (nBytesTransferred < sndSize)
        {
            ALOGE("%s(%02Xh): [OUT] USB transfer incompleted : %d (%d)", __func__, mEpAddrOut, nBytesTransferred, sndSize);
            nBytesTransferred = -1;
        }
        else
        {
            ALOGI("%s(%02Xh): [OUT] USB transfer success", __func__, mEpAddrOut);

            if ((rcvBuffer != nullptr) && (rcvSize > 0))
            {
                rc = usb_transfer(mHandle, mEpAddrIn, mEpAttrIn, &rcvBuffer[0], rcvSize, &nBytesTransferred, USB_TRANSFER_TIMEOUT);
                if (rc != LIBUSB_SUCCESS)
                {
                    ALOGE("%s(%02Xh): [IN] USB transfer error : %s %s", __func__, mEpAddrIn, libusb_error_name(rc), libusb_strerror((libusb_error)rc));
                    nBytesTransferred = -1;
                }
                else
                {
                    ALOGI("%s(%02Xh): [IN] USB transfer success : %d bytes were received", __func__, mEpAddrIn, nBytesTransferred);
                }
            }
            else
            {
                nBytesTransferred = 0;
            }
        }
        libusb_release_interface(mHandle, mIfNumber);
    }
    else
    {
        ALOGE("%s: ERROR: Channel not ready", __func__);
        nBytesTransferred = -1;
    }

    mLock.unlock();

    return nBytesTransferred;
}
