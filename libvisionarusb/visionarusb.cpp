/*
 * API source file
 *
 * This module implements all the exported functions defined in visionarusb.h
 */


#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <pthread.h>
#include <libusb-1.0/libusb.h>
#include <cmath>

#include "alog.h"

#include "visionarusb.h"
#include "visionarusbdevice.h"



/******************************************************************************
 * CONSTANTS
 ******************************************************************************/
#if DEBUG
/* Used by ALOG* macros */
static const char *TAG = "VARUSB";
#endif

/* Info for conversion factors */
static const float GI        = 9.80665;

static const float MAX_ACC   = (2.0*GI);
static const float MAX_GYR   = 245.0;
static const float MAX_MAG   = 50.0;
static const float MAX_COMP  = M_PI;

static const float CONV_ACC  = (MAX_ACC/SHRT_MAX);
static const float CONV_GYR  = ((MAX_GYR*M_PI)/(SHRT_MAX*180));
static const float CONV_MAG  = (MAX_MAG/SHRT_MAX);
static const float CONV_COMP = (MAX_COMP/SHRT_MAX);

/* General commands
 * Bit  Description                         Value
 *  0   Reset board                         0 = do NOT care; 1 = reset
 *  1
 *  2   Power OFF LCD                       0 = switch ON;   1 = switch OFF
 *  3   Power OFF camera                    0 = switch ON;   1 = switch OFF
 *  4
 */
static const unsigned short int VISIONAR_RESET_BOARD_CMD        = 0x0001;
static const unsigned short int VISIONAR_POWER_OFF_LCD_MASK     = 0x0004;
static const unsigned short int VISIONAR_POWER_OFF_CAMERA_MASK  = 0x0008;

static const unsigned char VISIONAR_CAMERA_LED_ADDR             =   0xC0;
static const unsigned short int VISIONAR_CAMERA_LED_REG         = 0x0005;

/******************************************************************************
 * VARIABLES
 ******************************************************************************/

/**
 * Last general command sent to VisionAR device
 */
static unsigned short int visionARCommand = 0x0000;


/******************************************************************************
 * FUNCTIONS
 ******************************************************************************/

bool Startup(EventCallback cb)
{
    return VisionARUSBDevice::getInstance()->init(cb);
}

bool CloseDev(void)
{
    VisionARUSBDevice::destroyInstance();

    return true;
}

bool WriteImg(unsigned char *data)
{
    return VisionARUSBDevice::getInstance()->draw(data);
}

bool Contrast(int val)
{
    return VisionARUSBDevice::getInstance()->setContrast(val);
}

bool Brightness(int val)
{
    return VisionARUSBDevice::getInstance()->setBrightness(val);
}

bool Haptic(unsigned int ms)
{
    return VisionARUSBDevice::getInstance()->setHaptic(ms);
}

bool StartTouchpad(TouchpadCallback callback, unsigned int ms)
{
    return VisionARUSBDevice::getInstance()->enableTouchpad(callback, ms);
}

bool StopTouchpad(void)
{
    return VisionARUSBDevice::getInstance()->disableTouchpad();
}


bool GetImuAcc(float *pImuAcc)
{
    short int reg[3];

    if (GetImuAccReg(&reg[0]))
    {
        pImuAcc[0] = ((float)reg[0]) * CONV_ACC;
        pImuAcc[1] = ((float)reg[1]) * CONV_ACC;
        pImuAcc[2] = ((float)reg[2]) * CONV_ACC;

        return true;
    }
    else
    {
        return false;
    }
}

bool GetImuAccReg(short int *pImuAcc)
{
    VisionARStatus status;

    if (VisionARUSBDevice::getInstance()->getStatus(&status))
    {
        pImuAcc[0] = status.accelX;
        pImuAcc[1] = status.accelY;
        pImuAcc[2] = status.accelZ;

        return true;
    }
    else
    {
        return false;
    }
}

bool GetImuGyro(float *pImuGyro)
{
    short int reg[3];

    if (GetImuGyroReg(&reg[0]))
    {
        pImuGyro[0] = ((float)reg[0]) * CONV_GYR;
        pImuGyro[1] = ((float)reg[1]) * CONV_GYR;
        pImuGyro[2] = ((float)reg[2]) * CONV_GYR;

        return true;
    }
    else
    {
        return false;
    }
}

bool GetImuGyroReg(short int *pImuGyro)
{
    VisionARStatus status;

    if (VisionARUSBDevice::getInstance()->getStatus(&status))
    {
        pImuGyro[0] = status.gyroX;
        pImuGyro[1] = status.gyroY;
        pImuGyro[2] = status.gyroZ;

        return true;
    }
    else
    {
        return false;
    }
}


bool GetImuMag(float *pImuMag)
{
    short int reg[3];

    if (GetImuMagReg(&reg[0]))
    {
        pImuMag[0] = ((float)reg[0]) * CONV_MAG;
        pImuMag[1] = ((float)reg[1]) * CONV_MAG;
        pImuMag[2] = ((float)reg[2]) * CONV_MAG;

        return true;
    }
    else
    {
        return false;
    }
}

bool GetImuMagReg(short int *pImuMag)
{
    VisionARStatus status;

    if (VisionARUSBDevice::getInstance()->getStatus(&status))
    {
        pImuMag[0] = status.magX;
        pImuMag[1] = status.magY;
        pImuMag[2] = status.magZ;

        return true;
    }
    else
    {
        return false;
    }
}


bool GetProtVers(unsigned char *pProtVers)
{
    VisionARSWVersion ver;

    if (VisionARUSBDevice::getInstance()->getVersion(&ver))
    {
        pProtVers[0] = ver.protocol_major;
        pProtVers[1] = ver.protocol_minor;

        return true;
    }
    else
    {
        return false;
    }
}

bool GetBootVers(unsigned char *pBootVers)
{
    VisionARSWVersion ver;

    if (VisionARUSBDevice::getInstance()->getVersion(&ver))
    {
        pBootVers[0] = ver.boot_major;
        pBootVers[1] = ver.boot_minor;

        return true;
    }
    else
    {
        return false;
    }
}

bool GetApplVers(unsigned char *pApplVers)
{
    VisionARSWVersion ver;

    if (VisionARUSBDevice::getInstance()->getVersion(&ver))
    {
        pApplVers[0] = ver.appl_major;
        pApplVers[1] = ver.appl_minor;
        pApplVers[2] = ver.appl_patch;

        pApplVers[3] = ver.appl_day;
        pApplVers[4] = ver.appl_month;
        pApplVers[5] = ver.appl_year;

        return true;
    }
    else
    {
        return false;
    }
}

bool GetStatus(unsigned int *stat)
{
    VisionARStatus status;

    if (VisionARUSBDevice::getInstance()->getStatus(&status))
    {
        *stat = status.status;

        return true;
    }
    else
    {
        return false;
    }
}

bool GetErrors(unsigned char *num, unsigned short *codes)
{
    if (VisionARUSBDevice::getInstance()->getErrors(num, codes))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Reset(void)
{
    return VisionARUSBDevice::getInstance()->setGeneralCommands(VISIONAR_RESET_BOARD_CMD);
}

bool PowerOffLcd(bool off)
{
    bool ret;

    if (off)
    {
        ret = VisionARUSBDevice::getInstance()->setGeneralCommands(visionARCommand | VISIONAR_POWER_OFF_LCD_MASK);
        if (ret)
        {
            visionARCommand |= VISIONAR_POWER_OFF_LCD_MASK;
        }
    }
    else
    {
        ret = VisionARUSBDevice::getInstance()->setGeneralCommands(visionARCommand & ~VISIONAR_POWER_OFF_LCD_MASK);
        if (ret)
        {
            visionARCommand &= ~VISIONAR_POWER_OFF_LCD_MASK;
        }
    }

    return ret;
}

bool PowerOffCamera(bool off)
{
    bool ret;

    if (off)
    {
        ret = VisionARUSBDevice::getInstance()->setGeneralCommands(visionARCommand | VISIONAR_POWER_OFF_CAMERA_MASK);
        if (ret)
        {
            visionARCommand |= VISIONAR_POWER_OFF_CAMERA_MASK;
        }
    }
    else
    {
        ret = VisionARUSBDevice::getInstance()->setGeneralCommands(visionARCommand & ~VISIONAR_POWER_OFF_CAMERA_MASK);
        if (ret)
        {
            visionARCommand &= ~VISIONAR_POWER_OFF_CAMERA_MASK;
        }
    }

    return ret;
}

bool LedCamera(VisionARLedCameraColor color)
{
    unsigned char data[1];

    switch(color)
    {
        case VISIONAR_CAMERA_GREEN_COLOR:
            data[0] = 0xF4;
            break;

        case VISIONAR_CAMERA_RED_COLOR:
            data[0] = 0xF1;
            break;

        default:
            data[0] = 0xF5;
            break;
    }

    return VisionARUSBDevice::getInstance()->setI2C(VISIONAR_CAMERA_LED_ADDR, 0, VISIONAR_CAMERA_LED_REG, 1, &data[0]);
}
