/**
 * VisionARUSBDevice class header file
 *
 * VisionARUSBDevice class allows access to VisionAR device, using USB in user mode:
 * it hides registers map and used USB protocol, providing method to get required info.
 */

#include <cstring>
#include <linux/input.h>
#include <libusb-1.0/libusb.h>

#include "alog.h"
#include "visionarusbdevice.h"
#include "visionarusbmessage.h"


#if DEBUG
static const char *TAG = "VAR_DEV";     // used by ALOGx macros
#endif

static const int WIDTH = 419;
static const int HEIGHT = 138;

static const uint16_t VISIONAR_VID = 0x0483;
static const uint16_t VISIONAR_PID = 0xA306;

static const uint8_t VISIONAR_FB_IF_CLASS = 0x0A;
static const uint8_t VISIONAR_FB_IF_SUBCLASS = 0x00;
static const uint8_t VISIONAR_FB_IF_PROTOCOL = 0x00;

static const uint8_t VISIONAR_CDC_IF_CLASS = 0xFF;
static const uint8_t VISIONAR_CDC_IF_SUBCLASS = 0x01;
static const uint8_t VISIONAR_CDC_IF_PROTOCOL = 0x00;

static const int VISIONAR_CONNECTION_PERIOD_IN_MS = 500;
static const int VISIONAR_REFRESHFB_PERIOD_IN_MS = 50;
static const int VISIONAR_TOUCHPAD_PERIOD_IN_MS = 250;

/**
 * Protocol Commands
 */
// CDC Commands
static const unsigned char VISIONAR_CDC_VERSION_CMD     = 0x81;
static const unsigned char VISIONAR_CDC_STATUS_CMD      = 0x90;
static const unsigned char VISIONAR_CDC_ERRORS_CMD      = 0x91;
static const unsigned char VISIONAR_CDC_COMMANDS_CMD    = 0xA0;
static const unsigned char VISIONAR_CDC_RGB_CMD         = 0xA1;
static const unsigned char VISIONAR_CDC_HAPTIC_CMD      = 0xA2;
static const unsigned char VISIONAR_CDC_I2CWRITE_CMD    = 0xB0;
static const unsigned char VISIONAR_CDC_I2CREAD_CMD     = 0xB1;
// FB Commands
static const unsigned char VISIONAR_FB_BITMAP_CMD       = 0x01;
static const unsigned char VISIONAR_FB_REGISTER_CMD     = 0x11;

// LCD Registers
static const unsigned char VISIONAR_LCD_CONTRAST_REG    = 0x24;
static const unsigned char VISIONAR_LCD_BRIGHTNESS_REG  = 0x25;


static unsigned char capacitiveKey2Value(unsigned char val)
{
    if (val & 0x40) return KEY_F10;
    if (val & 0x20) return KEY_F9;
    if (val & 0x10)
    {
        // double tap
        if (val & 0x08) return KEY_F8;
        if (val & 0x04) return KEY_F7;
        if (val & 0x02) return KEY_F6;
        if (val & 0x01) return KEY_F5;
    }
    else
    {
        // single tap
        if (val & 0x08) return KEY_F4;
        if (val & 0x04) return KEY_F3;
        if (val & 0x02) return KEY_F2;
        if (val & 0x01) return KEY_F1;
    }

    return 0;
}


VisionARUSBDevice *VisionARUSBDevice::mInstance = nullptr;


VisionARUSBDevice *VisionARUSBDevice::getInstance(void)
{
    if (mInstance == nullptr)
    {
        mInstance = new VisionARUSBDevice;
    }

    return mInstance;
}

void VisionARUSBDevice::destroyInstance(void)
{
    if (mInstance != nullptr)
    {
        delete mInstance;
        mInstance = nullptr;
    }
}

VisionARUSBDevice::VisionARUSBDevice(void):
        mStatus(DEVICE_ERROR),
        mHandle(nullptr),
        mFb(VISIONAR_FB_IF_CLASS, VISIONAR_FB_IF_SUBCLASS, VISIONAR_FB_IF_PROTOCOL),
        mCdc(VISIONAR_CDC_IF_CLASS, VISIONAR_CDC_IF_SUBCLASS, VISIONAR_CDC_IF_PROTOCOL),
        mConnectionCb(nullptr),
        mTouchpadEnabled(false),
        mTouchpadCb(nullptr),
        mTouchpadPeriod(0),
        mDeviceVersionValidity(false),
        mDeviceStatusValidity(false),
        mContext(nullptr),
        tConnection(&VisionARUSBDevice::checkConnection, this),
        tRefreshFb(&VisionARUSBDevice::refreshFramebuffer, this),
        tTouchpad(&VisionARUSBDevice::checkTouchpad, this)
{
    const struct libusb_version *version = libusb_get_version();

    ALOGI("%s(%04Xh,%04Xh)", __func__, VISIONAR_VID, VISIONAR_PID);
    ALOGV("%s: libusb version: %hu.%hu.%hu.%hu%s (%s)",
                __func__,
                version->major, version->minor, version->micro, version->nano,
                version->rc, version->describe);
    ALOGV("CAPABILITY capability    -> %s", (libusb_has_capability(LIBUSB_CAP_HAS_CAPABILITY)?"true":"false"));
    ALOGV("HOT PLUG capability      -> %s", (libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG)?"true":"false"));
    ALOGV("HID ACCESS capability    -> %s", (libusb_has_capability(LIBUSB_CAP_HAS_HID_ACCESS)?"true":"false"));
    ALOGV("DETACH DRIVER capability -> %s", (libusb_has_capability(LIBUSB_CAP_SUPPORTS_DETACH_KERNEL_DRIVER)?"true":"false"));

    int rc = libusb_init(&mContext);
    if (rc != LIBUSB_SUCCESS)
    {
        ALOGE("%s: ERROR on libusb initializing: %s (%s)", __func__, libusb_error_name(rc), libusb_strerror((libusb_error)rc));
    }
    else
    {
#if DEBUG
///        libusb_set_debug(mContext, LIBUSB_LOG_LEVEL_DEBUG );
#endif

        mPixels = new unsigned char [WIDTH*HEIGHT];
        bzero(mPixels, WIDTH*HEIGHT);

        mStatus = DEVICE_IDLE;
    }
}

VisionARUSBDevice::~VisionARUSBDevice(void)
{
    ALOGI("%s", __func__);

    delete mPixels;

    mLock.lock();
    mStatus = DEVICE_DESTROYED;
    mLock.unlock();

    mRefreshFbCv.notify_all();
    mTouchpadCv.notify_all();

    tConnection.join();
    tRefreshFb.join();
    tTouchpad.join();

    ALOGI("%s: threads finished", __func__);

    if (mContext != nullptr)
    {
        libusb_exit(mContext);
    }
}

bool VisionARUSBDevice::init(EventCallback cb)
{
    bool ret = false;

    mLock.lock();

    if (mStatus == DEVICE_IDLE)
    {
        mConnectionCb = cb;
        mStatus = DEVICE_CLOSED;

        ret = true;
    }

    mLock.unlock();

    return ret;
}

void VisionARUSBDevice::reset(void)
{
    mLock.lock();

    if (mStatus != DEVICE_ERROR)
    {
        mDeviceStatusValidity = false;
        mDeviceVersionValidity = false;

        mTouchpadEnabled = false;
        mTouchpadCb = nullptr;
        mTouchpadPeriod = 0;

        mConnectionCb = nullptr;
        mCdc.close();
        mFb.close();
        if (mHandle)
        {
            libusb_close(mHandle);
            mHandle = nullptr;
        }

        mStatus = DEVICE_IDLE;
    }

    mLock.unlock();
}

VisionARUSBDevice::DeviceStatus VisionARUSBDevice::status(void)
{
    DeviceStatus ret;

    mLock.lock();
    ret = mStatus;
    mLock.unlock();

    return ret;
}

bool VisionARUSBDevice::setGeneralCommands(unsigned short int cmd)
{
    bool ret;

    if (status() == DEVICE_OPENED)
    {
        int size;
        unsigned char src[2];
        unsigned char toSend[16];

        unsigned char rcv[16];
        unsigned char cmd;

        *((unsigned short int *)&src[0]) = libusb_cpu_to_le16(cmd);

        size = VisionARUSBMessage::compose(VISIONAR_CDC_COMMANDS_CMD, &src[0], sizeof(src), &toSend[0], sizeof(toSend));
        if (size > 0)
        {
            if (mCdc.status())
            {
                mUsbLock.lock();
                size = mCdc.transfer(&toSend[0], size, &rcv[0], sizeof(rcv));
                mUsbLock.unlock();
            }
            else
            {
                size = -1;
            }

            if (size > 0)
            {
                size = VisionARUSBMessage::parse(&rcv[0], size, &cmd, nullptr, 0);
                if (cmd == VISIONAR_CDC_COMMANDS_CMD)
                {
                    ret = true;
                }
                else
                {
                    ret = false;
                }
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = false;
        }
    }
    else
    {
        // Device not opened!
        ret = false;
    }

    return ret;
}

bool VisionARUSBDevice::setHaptic(int ms)
{
    bool ret;

    if (status() == DEVICE_OPENED)
    {
        int size;
        unsigned char src[4];
        unsigned char toSend[16];

        unsigned char rcv[16];
        unsigned char cmd;

        src[0] = 1;                 // Command: ON
        src[1] = 0;
        *((unsigned short int *)&src[2]) = libusb_cpu_to_le16(ms);

        size = VisionARUSBMessage::compose(VISIONAR_CDC_HAPTIC_CMD, &src[0], sizeof(src), &toSend[0], sizeof(toSend));
        if (size > 0)
        {
            if (mCdc.status())
            {
                mUsbLock.lock();
                size = mCdc.transfer(&toSend[0], size, &rcv[0], sizeof(rcv));
                mUsbLock.unlock();
            }
            else
            {
                size = -1;
            }

            if (size > 0)
            {
                size = VisionARUSBMessage::parse(&rcv[0], size, &cmd, nullptr, 0);
                if (cmd == VISIONAR_CDC_HAPTIC_CMD)
                {
                    ret = true;
                }
                else
                {
                    ret = false;
                }
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = false;
        }
    }
    else
    {
        // Device not opened!
        ret = false;
    }

    return ret;
}

bool VisionARUSBDevice::getVersion(VisionARSWVersion *ver)
{
    bool ret;

    mLock.lock();
    if ((mStatus == DEVICE_OPENED) && (mDeviceVersionValidity))
    {
        memcpy(ver, &mDeviceVersion, sizeof(mDeviceVersion));
        ret = true;
    }
    else
    {
        ret = false;
    }
    mLock.unlock();

    return ret;
}

bool VisionARUSBDevice::getStatus(VisionARStatus *st)
{
    bool ret;

    if (status() == DEVICE_OPENED)
    {
        if (touchpad())
        {
            ret = true;
        }
        else
        {
            // Status register is not read by touchpad polling thread
            ret = getStatusRegister();
        }

        if (ret)
        {
            mLock.lock();
            if (mDeviceStatusValidity)
            {
                memcpy(st, &mDeviceStatus, sizeof(mDeviceStatus));
            }
            ret = mDeviceStatusValidity;
            mLock.unlock();
        }
    }
    else
    {
        ret = false;
    }

    return ret;
}

bool VisionARUSBDevice::getErrors(unsigned char *num, unsigned short *codes)
{
    bool ret;

    int size;
    unsigned char toSend[16];

    unsigned char rcv[128];
    unsigned char data[128];
    unsigned char cmd;

    size = VisionARUSBMessage::compose(VISIONAR_CDC_ERRORS_CMD, nullptr, 0, &toSend[0], sizeof(toSend));
    if (size > 0)
    {
        if (mCdc.status())
        {
            mUsbLock.lock();
            size = mCdc.transfer(&toSend[0], size, &rcv[0], sizeof(rcv));
            mUsbLock.unlock();
        }
        else
        {
            size = -1;
        }

        if (size > 0)
        {
            size = VisionARUSBMessage::parse(&rcv[0], size, &cmd, &data[0], sizeof(data));
            if (cmd == VISIONAR_CDC_ERRORS_CMD)
            {
                *num = data[0];
                for (int counter = 0; counter < size/2; counter++)
                {
                    codes[counter] = data[2*counter+1] + (((data[2*counter+2])&0xFF) << 8);
                }
                ret = true;
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = false;
        }
    }
    else
    {
        ret = false;
    }

    return ret;
}

bool VisionARUSBDevice::getI2C(unsigned char addr, unsigned char conf, unsigned short reg, unsigned char num, unsigned char *data)
{
    bool ret;

    if (status() == DEVICE_OPENED)
    {
        int size;
        unsigned char src[128];
        unsigned char toSend[128];

        unsigned char rcv[128];
        unsigned char cmd;
        unsigned char values[128];

        src[0] = addr | 0x01;  // address for reading data
        src[1] = conf;
        *((unsigned short int *)&src[2]) = libusb_cpu_to_le16(reg);
        src[4] = num;

        size = VisionARUSBMessage::compose(VISIONAR_CDC_I2CREAD_CMD, &src[0], (1+1+2+1), &toSend[0], sizeof(toSend));
        if (size > 0)
        {
            if (mCdc.status())
            {
                mUsbLock.lock();
                size = mCdc.transfer(&toSend[0], size, &rcv[0], sizeof(rcv));
                mUsbLock.unlock();
            }
            else
            {
                size = -1;
            }

            if (size > 0)
            {
                size = VisionARUSBMessage::parse(&rcv[0], size, &cmd, &values[0], sizeof(values));
                if ((cmd == VISIONAR_CDC_I2CREAD_CMD) && (size > 1))
                {
                    if (values[0] == num)
                    {
                        memcpy(data, &values[1], num);
                        ret = true;
                    }
                    else
                    {
                        ALOGE("%s: ERROR on getting I2C data: unexpected data size = %u (%d)", __func__, data[0], num);
                        ret = false;
                    }
                }
                else
                {
                    ALOGE("%s: ERROR on getting I2C data: cmd = %02Xh, size = %d (%d)", __func__, cmd, size, num);
                    ret = false;
                }
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = false;
        }
    }
    else
    {
        // Device not opened!
        ret = false;
    }

    return ret;
}

bool VisionARUSBDevice::setI2C(unsigned char addr, unsigned char conf, unsigned short reg, unsigned char num, unsigned char *data)
{
    bool ret;

    if (status() == DEVICE_OPENED)
    {
        int size;
        unsigned char src[128];
        unsigned char toSend[128];

        unsigned char rcv[128];
        unsigned char cmd;

        src[0] = addr;
        src[1] = conf;
        *((unsigned short int *)&src[2]) = libusb_cpu_to_le16(reg);
        src[4] = num;
        memcpy(&src[5], data, num);

        size = VisionARUSBMessage::compose(VISIONAR_CDC_I2CWRITE_CMD, &src[0], (1+1+2+1+num), &toSend[0], sizeof(toSend));
        if (size > 0)
        {
            if (mCdc.status())
            {
                mUsbLock.lock();
                size = mCdc.transfer(&toSend[0], size, &rcv[0], sizeof(rcv));
                mUsbLock.unlock();
            }
            else
            {
                size = -1;
            }

            if (size > 0)
            {
                size = VisionARUSBMessage::parse(&rcv[0], size, &cmd, nullptr, 0);
                if (cmd == VISIONAR_CDC_I2CWRITE_CMD)
                {
                    ret = true;
                }
                else
                {
                    ret = false;
                }
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = false;
        }
    }
    else
    {
        // Device not opened!
        ret = false;
    }

    return ret;
}


bool VisionARUSBDevice::setContrast(int val)
{
    if (status() == DEVICE_OPENED)
    {
        if ((val >= 0) && (val <= 255))
        {
            return setLcdRegister(VISIONAR_LCD_CONTRAST_REG, val);
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool VisionARUSBDevice::setBrightness(int val)
{
    if (status() == DEVICE_OPENED)
    {
        if ((val >= 0) && (val <= 255))
        {
            return setLcdRegister(VISIONAR_LCD_BRIGHTNESS_REG, val);
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool VisionARUSBDevice::getVersionRegister(void)
{
    bool ret;

    int size;
    unsigned char toSend[16];

    unsigned char rcv[128];
    unsigned char data[128];
    unsigned char cmd;

    size = VisionARUSBMessage::compose(VISIONAR_CDC_VERSION_CMD, nullptr, 0, &toSend[0], sizeof(toSend));
    if (size > 0)
    {
        if (mCdc.status())
        {
            mUsbLock.lock();
            size = mCdc.transfer(&toSend[0], size, &rcv[0], sizeof(rcv));
            mUsbLock.unlock();
        }
        else
        {
            size = -1;
        }

        if (size > 0)
        {
            size = VisionARUSBMessage::parse(&rcv[0], size, &cmd, &data[0], sizeof(data));
            if ((size == sizeof(VisionARSWVersion)) && (cmd == VISIONAR_CDC_VERSION_CMD))
            {
                ret = true;
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = false;
        }
    }
    else
    {
        ret = false;
    }

    mLock.lock();
    if (ret == true)
    {
        memcpy(&mDeviceVersion, &data[0], size);
        mDeviceVersionValidity = true;
    }
    else
    {
        mDeviceVersionValidity = false;
    }
    mLock.unlock();

    return ret;
}

bool VisionARUSBDevice::getStatusRegister(void)
{
    bool ret;

    int size;
    unsigned char toSend[16];

    unsigned char rcv[128];
    unsigned char data[128];
    unsigned char cmd;

    size = VisionARUSBMessage::compose(VISIONAR_CDC_STATUS_CMD, nullptr, 0, &toSend[0], sizeof(toSend));
    if (size > 0)
    {
        if (mCdc.status())
        {
            mUsbLock.lock();
            size = mCdc.transfer(&toSend[0], size, &rcv[0], sizeof(rcv));
            mUsbLock.unlock();
        }
        else
        {
            size = -1;
        }

        if (size > 0)
        {
            size = VisionARUSBMessage::parse(&rcv[0], size, &cmd, &data[0], sizeof(data));
            if ((size == sizeof(VisionARStatus)) && (cmd == VISIONAR_CDC_STATUS_CMD))
            {
                ret = true;
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = false;
        }
    }
    else
    {
        ret = false;
    }

    mLock.lock();
    if (ret == true)
    {
        int index = 0;

        mDeviceStatus.status = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8)
                                        + ((data[index++] & 0xFF) << 16)
                                        + ((data[index++] & 0xFF) << 24);
        mDeviceStatus.accelX = (data[index++] & 0xFF)
                                + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.accelY = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.accelZ = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.gyroX = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.gyroY = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.gyroZ = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.magX = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.magY = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.magZ = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.alsensor = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);
        mDeviceStatus.CapKeys1 = data[index++];
        mDeviceStatus.CapKeys2 = data[index++];
        mDeviceStatus.CapKeys3 = data[index++];
        mDeviceStatus.CapKeys4 = data[index++];
        mDeviceStatus.compass = (data[index++] & 0xFF)
                                        + ((data[index++] & 0xFF) << 8);

        mDeviceStatusValidity = true;
    }
    else
    {
        mDeviceStatusValidity = false;
    }
    mLock.unlock();

    return ret;
}

bool VisionARUSBDevice::setLcdRegister(unsigned char reg, unsigned char val)
{
    bool ret;

    int size;
    unsigned char src[2];
    unsigned char toSend[16];

    unsigned char rcv[16];
    unsigned char cmd;

    src[0] = reg;   // Register address
    src[1] = val;   // Register value

    size = VisionARUSBMessage::compose(VISIONAR_FB_REGISTER_CMD, &src[0], sizeof(src), &toSend[0], sizeof(toSend));
    if (size > 0)
    {
        if (mFb.status())
        {
            mUsbLock.lock();
            size = mFb.transfer(&toSend[0], size, nullptr, 0);
            mUsbLock.unlock();
        }
        else
        {
            size = -1;
        }

        if (size > 0)
        {
            size = VisionARUSBMessage::parse(&rcv[0], size, &cmd, nullptr, 0);
            if (cmd == VISIONAR_FB_REGISTER_CMD)
            {
                ret = true;
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = true;
        }
    }
    else if (size == 0)
    {
        ret = true;
    }
    else
    {
        ret = false;
    }

    return ret;
}

bool VisionARUSBDevice::draw(unsigned char *pixels)
{
    bool ret = false;

    int inputIndex = 0;
    int visionARIndex = 0;

    unsigned int a, r, g, b;

    if (status() == DEVICE_OPENED)
    {
        mFbLock.lock();

        // image must be converted to 8bit/pixel
        while (inputIndex < 4*WIDTH*HEIGHT)
        {
            a = (unsigned int)(pixels[inputIndex++]) & 0xFF;
            r = (unsigned int)(pixels[inputIndex++]) & 0xFF;
            g = (unsigned int)(pixels[inputIndex++]) & 0xFF;
            b = (unsigned int)(pixels[inputIndex++]) & 0xFF;

            mPixels[visionARIndex++] = (unsigned char)((r * 229 + g * 587 + b * 114) / 1000) * a / 255;
        } // while (true)

        mFbLock.unlock();

        ret = true;
    }

    return ret;
}

bool VisionARUSBDevice::touchpad(void)
{
    bool ret;

    mLock.lock();
    ret = mTouchpadEnabled;
    mLock.unlock();

    return ret;
}

bool VisionARUSBDevice::enableTouchpad(TouchpadCallback callback, unsigned int ms)
{
    bool ret = false;

    if (callback != nullptr)
    {
        mLock.lock();

        if (mTouchpadEnabled == false)
        {
            mTouchpadCb = callback;
            if (ms == 0)
            {
                mTouchpadPeriod = VISIONAR_TOUCHPAD_PERIOD_IN_MS;
            }
            else
            {
                mTouchpadPeriod = ms;
            }

            mTouchpadEnabled = true;
            if (mStatus == DEVICE_OPENED)
            {
                mTouchpadCv.notify_all();
            }

            ret = true;
        }

        mLock.unlock();
    }

    return ret;
}

bool VisionARUSBDevice::disableTouchpad(void)
{
    bool ret = false;

    mLock.lock();
    mTouchpadEnabled = false;
    mTouchpadCb = nullptr;
    mTouchpadPeriod = 0;
    mLock.unlock();

    return ret;
}

void VisionARUSBDevice::checkConnection(void)
{
    libusb_device **list;
    libusb_device *found;
    ssize_t counter;
    ssize_t num;
    int rc;
    struct libusb_device_descriptor desc;
    bool changeStatus;

    while(1)
    {
        ALOGI("%s", __func__);

        std::this_thread::sleep_for(std::chrono::milliseconds(VISIONAR_CONNECTION_PERIOD_IN_MS));
        if (status() == DEVICE_DESTROYED) break;    // ending the loop
        if (status() <= DEVICE_IDLE) continue;      // nothing to do

        if ((num = libusb_get_device_list(mContext, &list)) < 0)
        {
            ALOGE("%s: ERROR on getting USB devices (%s - %s)", __func__, libusb_error_name(num), libusb_strerror((libusb_error)num));
        }
        else if (num == 0)
        {
            ALOGE("%s: no USB devices were found.", __func__);

            libusb_free_device_list(list, 1);   // devices are unreferred before freeing the list
        }
        else
        {
            // USB devices were found
            found = nullptr;
            changeStatus = false;

            for (counter = 0; counter < num; counter++)
            {
                if ((rc = libusb_get_device_descriptor(list[counter], &desc)) == LIBUSB_SUCCESS)
                {
                    if ((desc.idVendor == VISIONAR_VID) && (desc.idProduct == VISIONAR_PID))
                    {
                        found = list[counter];
                        break;
                    }
                }
                else
                {
                    ALOGE("%s: ERROR on getting USB device descriptor (%s - %s)\n", __func__, libusb_error_name(rc), libusb_strerror((libusb_error)rc));
                }
            } // for (devices list)

            if (found != nullptr)
            {
                mLock.lock();

                if (mStatus == DEVICE_CLOSED)
                {
                    if ((rc = libusb_open(found, &mHandle)) == LIBUSB_SUCCESS)
                    {
                        mFb.open(mHandle, found);
                        mCdc.open(mHandle, found);

                        mStatus = DEVICE_OPENED;

                        mRefreshFbCv.notify_all();
                        if (mTouchpadEnabled)
                        {
                            mTouchpadCv.notify_all();
                        }

                        changeStatus = true;
                    }
                }

                mLock.unlock();

                if (changeStatus)
                {
                    ALOGI("%s: VisionAR device found", __func__);
                    getVersionRegister();

                    if (mConnectionCb != nullptr)
                    {
                        mConnectionCb(VISIONAR_CONNECTION_EVENT);
                    }
                }
            }
            else
            {
                // No VisionAR device found!
                mLock.lock();

                if (mStatus == DEVICE_OPENED)
                {
                    libusb_close(mHandle);

                    mFb.close();
                    mCdc.close();

                    mDeviceVersionValidity = false;

                    mStatus = DEVICE_CLOSED;

                    changeStatus = true;
                }

                mLock.unlock();

                if (changeStatus)
                {
                    ALOGI("%s: VisionAR device NOT found", __func__);
                    if (mConnectionCb != nullptr)
                    {
                        mConnectionCb(VISIONAR_DISCONNECTION_EVENT);
                    }
                }
            }

            libusb_free_device_list(list, 1);   // devices are unreferred before freeing the list
        }
    } // while(1)

    ALOGI("%s: loop terminated", __func__);
}

void VisionARUSBDevice::refreshFramebuffer(void)
{
    int size;
    unsigned char *toSend = new unsigned char [5 + 1 + WIDTH*HEIGHT + 1];
    unsigned char rcv[16];
    unsigned char cmd;

    std::unique_lock<std::mutex> lck(mRefreshFbLock);

    ALOGI("%s", __func__);

    if (toSend != nullptr)
    {
        ALOGI("%s A", __func__);
        while(status() != DEVICE_DESTROYED)
        {
            ALOGI("%s B", __func__);
            mRefreshFbCv.wait(lck);
            ALOGD("%s: post sem", __func__);

            while (status() == DEVICE_OPENED)
            {
                mFbLock.lock();
                size = VisionARUSBMessage::compose(VISIONAR_FB_BITMAP_CMD, mPixels, WIDTH*HEIGHT, &toSend[0], 7+WIDTH*HEIGHT);
                mFbLock.unlock();

                if (size > 0)
                {
                    if (mFb.status())
                    {
                        mUsbLock.lock();
                        size = mFb.transfer(&toSend[0], size, nullptr, 0);
                        mUsbLock.unlock();
                    }
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(VISIONAR_REFRESHFB_PERIOD_IN_MS));
            } // while(DEVICE_OPENED)
        } // while(DEVICE_DESTROYED)

        delete toSend;
    } // if message not null

    ALOGI("%s: loop terminated", __func__);
}

void VisionARUSBDevice::checkTouchpad(void)
{
    int ret;
    unsigned char ev;

    std::unique_lock<std::mutex> lck(mTouchpadLock);

    ALOGI("%s", __func__);

    while(status() != DEVICE_DESTROYED)
    {
        mTouchpadCv.wait(lck);
        ALOGD("%s: post sem", __func__);

        while ((status() == DEVICE_OPENED) && (touchpad()))
        {
            if (getStatusRegister())
            {
                mLock.lock();

                if ((ev = mDeviceStatus.CapKeys1) != 0x00)
                {
                    mTouchpadCb(capacitiveKey2Value(ev));
                }
                if ((ev = mDeviceStatus.CapKeys2) != 0x00)
                {
                    mTouchpadCb(capacitiveKey2Value(ev));
                    ALOGI("%s: Key 2 = %02Xh", __func__, capacitiveKey2Value(ev));
                }
                if ((ev = mDeviceStatus.CapKeys3) != 0x00)
                {
                    mTouchpadCb(capacitiveKey2Value(ev));
                    ALOGI("%s: Key 3 = %02Xh", __func__, capacitiveKey2Value(ev));
                }
                if ((ev = mDeviceStatus.CapKeys4) != 0x00)
                {
                    mTouchpadCb(capacitiveKey2Value(ev));
                    ALOGI("%s: Key 4 = %02Xh", __func__, capacitiveKey2Value(ev));
                }

                mLock.unlock();
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(mTouchpadPeriod));
        } // while(DEVICE_OPENED)
    } // while(DEVICE_DESTROYED)

    ALOGI("%s: loop terminated", __func__);
}
