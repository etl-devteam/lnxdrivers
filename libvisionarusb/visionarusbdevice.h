/**
 * VisionARUSBDevice class header file
 *
 * VisionARUSBDevice class allows access to VisionAR device, using USB in user mode:
 * it hides registers map and used USB protocol, providing method to get required info.
 */

#ifndef VISIONARUSBDEVICE_H_
#define VISIONARUSBDEVICE_H_

#include <mutex>
#include <thread>
#include <condition_variable>
#include <libusb-1.0/libusb.h>

#include "visionarusbtypes.h"
#include "visionarusbchannel.h"


struct __attribute__((packed)) VisionARStatus
{
    unsigned int status;
    short accelX;
    short accelY;
    short accelZ;
    short gyroX;
    short gyroY;
    short gyroZ;
    short magX;
    short magY;
    short magZ;
    short alsensor;
    unsigned char CapKeys1;
    unsigned char CapKeys2;
    unsigned char CapKeys3;
    unsigned char CapKeys4;
    short compass;
};

struct __attribute__((packed)) VisionARSWVersion
{
    unsigned char protocol_major;
    unsigned char protocol_minor;
    unsigned char boot_major;
    unsigned char boot_minor;
    unsigned char appl_major;
    unsigned char appl_minor;
    unsigned char appl_patch;
    unsigned char appl_day;
    unsigned char appl_month;
    unsigned char appl_year;
    unsigned char hw_revision;
};


class VisionARUSBDevice {

    public:
        enum DeviceStatus {
            DEVICE_ERROR,
            DEVICE_IDLE,
            DEVICE_CLOSED,
            DEVICE_OPENED,
            DEVICE_DESTROYED,
        };
        static VisionARUSBDevice *getInstance(void);
        static void destroyInstance(void);

        virtual ~VisionARUSBDevice();

        bool init(EventCallback cb);
        void reset(void);
        DeviceStatus status(void);

        // CDC Commands
        bool getVersion(VisionARSWVersion *ver);
        bool getStatus(VisionARStatus *status);
        bool getErrors(unsigned char *num, unsigned short *codes);
        bool setGeneralCommands(unsigned short int cmd);
        bool setHaptic(int ms);
        bool getI2C(unsigned char addr, unsigned char conf, unsigned short reg, unsigned char num, unsigned char *data); // ERGLASSER_CMD_I2CREAD
        bool setI2C(unsigned char addr, unsigned char conf, unsigned short reg, unsigned char num, unsigned char *data);

        // FB Commands
        bool setContrast(int val);
        bool setBrightness(int val);
        bool draw(unsigned char* pixels);

        // Touchpad
        bool touchpad(void);
        bool enableTouchpad(TouchpadCallback callback, unsigned int ms = 0);
        bool disableTouchpad(void);

    private:
        VisionARUSBDevice(void);

        bool getVersionRegister(void);
        bool getStatusRegister(void);

        bool setLcdRegister(unsigned char reg, unsigned char val);

        void checkConnection(void);
        void refreshFramebuffer(void);
        void checkTouchpad(void);


        DeviceStatus mStatus;

        libusb_device_handle *mHandle;
        VisionARUSBChannel mFb;
        VisionARUSBChannel mCdc;
        unsigned char *mPixels;

        EventCallback mConnectionCb;

        bool mTouchpadEnabled;
        TouchpadCallback mTouchpadCb;
        int mTouchpadPeriod;

        bool mDeviceVersionValidity;
        VisionARSWVersion mDeviceVersion;
        bool mDeviceStatusValidity;
        VisionARStatus mDeviceStatus;

        libusb_context *mContext;

        std::thread tConnection;
        std::thread tRefreshFb;
        std::thread tTouchpad;

        std::mutex mLock;       // lock for object attributes (but mPixels)
        std::mutex mFbLock;     // lock for mPixels attribute
        std::mutex mUsbLock;    // lock to sync USB access

        std::mutex mRefreshFbLock;
        std::mutex mTouchpadLock;

        std::condition_variable mRefreshFbCv;
        std::condition_variable mTouchpadCv;

        static VisionARUSBDevice *mInstance;
};

#endif /* VISIONARUSBDEVICE_H_ */
