/**
 * VisionARUSBChannel class header file
 *
 * VisionARUSBChannel class provides methods to transfer data via USB interface.
 * It finds required interface/endpoints and calls right transfer function,
 * USB interface type.
 * Moreover, it checks data transfers are performed one at time.
 * No checks about messages are performed by methods of this class.
 */

#ifndef VISIONARUSBCHANNEL_H_
#define VISIONARUSBCHANNEL_H_

#include <mutex>
#include <libusb-1.0/libusb.h>


class VisionARUSBChannel
{
    public:
        VisionARUSBChannel(unsigned char ifClass, unsigned char ifSubclass, unsigned char ifProt);
        virtual ~VisionARUSBChannel();

        bool open(libusb_device_handle *hndl, libusb_device *dev, unsigned char num = 1);
        bool close(void);
        bool reset(void);

        bool status(void);

        int transfer(unsigned char *sndBuffer, int sndSize, unsigned char *rcvBuffer, int rcvSize);

    private:
        const unsigned char mIfClass;
        const unsigned char mIfSubclass;
        const unsigned char mIfProt;

        std::mutex mLock;

        bool mIsOpened;
        libusb_device_handle *mHandle;
        unsigned char mIfNumber;
        unsigned char mEpAddrIn;
        unsigned char mEpAttrIn;
        unsigned char mEpAddrOut;
        unsigned char mEpAttrOut;
};

#endif /* VISIONARUSBCHANNEL_H_ */
