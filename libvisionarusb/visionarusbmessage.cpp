/**
 * VisionARUSBMessage class definition file
 *
 * VisionARUSBMessage class provides methods to manage messages to send to USB
 * channels, according to defined protocol.
 */

#include <cstring>

#include "alog.h"
#include "visionarusbmessage.h"


#define HEADER_SIZE     5

#define DATA_MASK       0x01
#define ACK_MASK        0x02
#define NACK_MASK       0x04


#if DEBUG
static const char *TAG = "VAR_MSG";     // used by ALOGx macros
#endif


//--------------------------------------------------------------------------------------
// Compute the CRC of the buffer starting from specified index
//--------------------------------------------------------------------------------------
static unsigned char CalcCsum(unsigned char *buf, int size)
{
    unsigned char csum = 0;

    for (int i = 0; i < size; i++)
        csum ^= buf[i];

    return csum;
}


int VisionARUSBMessage::compose(unsigned char srcCmd, unsigned char *src, int srcSize, unsigned char *dst, int dstSizeMax)
{
    // Output buffer size must be greater than
    //    header size (5)
    //  + cmd field size (1)
    //  + input size (srcSize)
    //  + payload checksum field size (1)
    if (dst && (dstSizeMax >= (HEADER_SIZE + 1 + srcSize + 1)))
    {
        dst[0] = 0x01;                                      // SOH
        dst[1] = DATA_MASK;                                 // CTRL
        dst[2] = (unsigned char)(srcSize + 2);              // PAYLOAD LEN LSB
        dst[3] = (unsigned char)((srcSize + 2) >> 8);       // PAYLOAD LEN MSB
        dst[4] = CalcCsum(&dst[0], 4);                      // HDR CSUM

        dst[5] = (unsigned char)srcCmd;                     // CMD
        if (src != nullptr)
        {
            memcpy(&dst[6], src, srcSize);                  // DATA
        }
        else
        {
            memset(&dst[6], 0, srcSize);                    // DUMMY DATA
        }
        dst[6 + srcSize] = CalcCsum(&dst[5], srcSize + 1);  // PAYLOAD CSUM

        return (6+srcSize+1);
    }
    else
    {
        ALOGE("[OUT] ERROR: no room enough to contain message");
        return -1;
    }
}

int VisionARUSBMessage::parse(unsigned char *src, int srcSize, unsigned char *dstCmd, unsigned char *dst, int dstSizeMax)
{
    int ret;

    if (src == nullptr)
    {
        // Nothing to parse!
        ret = -1;
    }
    else if (srcSize < HEADER_SIZE)
    {
        ALOGE("[IN] ERROR: no header was found");
        ret = -1;
    }
    else if (CalcCsum(&src[0], 4) != src[4])
    {
        ALOGE("[IN] ERROR: wrong header checksum");
        ret = -1;
    }
    else if (src[1] & NACK_MASK)
    {
        ALOGE("[IN] ERROR: NACK message");
        ret = -1;
    }
    else
    {
        // some data should be retrieved
        int dataLen = src[2] + (src[3] * 256);
        if (((src[1] & DATA_MASK) == 0) && (dataLen == 0))
        {
            // No data received
            ALOGI("[IN] No data received");
            ret = 0;
        }
        else if (srcSize < (HEADER_SIZE + dataLen))
        {
            ALOGE("[IN] ERROR: transfer incompleted: %d (%d)", srcSize, (HEADER_SIZE + dataLen));
            ret = -1;
        }
        else if (dataLen < 2)
        {
            ALOGE("[IN] ERROR: data field too short: %d", dataLen);
            ret = -1;
        }
        else if (CalcCsum(&src[5], dataLen - 1) != src[5 + dataLen - 1])
        {
            ALOGE("[IN] ERROR: wrong data checksum");
            ret = -1;
        }
        else
        {
            ALOGI("[IN] Message correct: cmd = %02Xh", src[5]);

            *dstCmd = src[5];
            if (dst)
            {
                dataLen -= 2;   // command and data checksum were removed
                if (dstSizeMax < dataLen)
                {
                    ALOGE("[IN] ERROR: no room enough for data");
                    memset(dst, 0, dstSizeMax);
                    ret = -1;
                }
                else
                {
                    ALOGI("[IN] Data retrieved");
                    memcpy(dst, &src[6], dataLen);
                    ret = dataLen;
                }
            }
            else
            {
                ALOGI("[IN] No need to retrieve data");
                ret = 0;
            }
        }
    }

    return ret;
}
