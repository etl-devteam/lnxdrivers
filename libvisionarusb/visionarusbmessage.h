/**
 * VisionARUSBMessage class header file
 *
 * VisionARUSBMessage class provides methods to manage messages to send to USB
 * channels, according to defined protocol.
 */

#ifndef VISIONARUSBMESSAGE_H_
#define VISIONARUSBMESSAGE_H_


class VisionARUSBMessage
{
    public:
        /**
         * Prepare a message for USB interface
         *
         * \param srcCmd command of the message
         * \param src buffer with sending data
         * \param srcSize size of sending data buffer
         * \param dst buffer for message
         * \param srcSizeMax maximum size of sending message
         * \returns
         *      - if greater than 0, size of found message
         *      - if 0, no message to send
         *      - if less than 0, there was an error in arguments
         */
        static int compose(unsigned char srcCmd, unsigned char *src, int srcSize, unsigned char *dst, int dstSizeMax);
        /**
         * Decode a message received from USB interface
         *
         * \param src buffer with received message
         * \param srcSize size of received message
         * \param dstCmd command of received message
         * \param dst buffer for received data
         * \param dstSizeMax maximum size of received data
         * \returns
         *      - if greater than 0, size of received data
         *      - if 0, no data received
         *      - if less than 0, there was an error
         */
        static int parse  (unsigned char *src, int srcSize, unsigned char *dstCmd, unsigned char *dst, int dstSizeMax);
};

#endif /* VISIONARUSBMESSAGE_H_ */
