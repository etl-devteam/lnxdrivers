/*
 * logger header file
 *
 * This module defines a simple set of logging function for all the OS (using GCC)
 */


#ifndef ALOG_H_
#define ALOG_H_

#if defined(ALOG_DEBUG)
#if defined(__ANDROID__)
    #include <android/log.h>

    #if defined(__cplusplus)
        #include <exception>
    #endif
#elif defined(__linux__)
    #if defined(__KERNEL__)
        #include <linux/printk.h>
    #else
        #include <libgen.h>
        #if defined(__cplusplus__)
            #include <cstdio>
        #else
            #include <stdio.h>
        #endif
        #include <time.h>
    #endif
#elif defined(__WIN32)
#endif
#endif

#if defined(__linux__)
    // Logging
    #define ALOG_DEBUG_LEVEL    5
    #define ALOG_VERBOSE_LEVEL  4
    #define ALOG_INFO_LEVEL     3
    #define ALOG_WARNING_LEVEL  2
    #define ALOG_ERROR_LEVEL    1
    #define ALOG_FATAL_LEVEL    0
#endif

#if defined(ALOG_DEBUG)
    #if defined(__ANDROID__)
        #define ALOGD(FMT, ...) __android_log_print(ANDROID_LOG_DEBUG, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
        #define ALOGV(FMT, ...) __android_log_print(ANDROID_LOG_VERBOSE, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
        #define ALOGI(FMT, ...) __android_log_print(ANDROID_LOG_INFO, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
        #define ALOGW(FMT, ...) __android_log_print(ANDROID_LOG_WARN, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
        #define ALOGE(FMT, ...) __android_log_print(ANDROID_LOG_ERROR, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
        #define ALOGF(FMT, ...) __android_log_print(ANDROID_LOG_FATAL, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
    #elif defined(__linux__)
        #if defined(__KERNEL__)
            #define ALOGD(FMT, ...) { if (aLogLevel >= ALOG_DEBUG_LEVEL) pr_info("%-8s: [%s:%d]: " FMT, TAG, kbasename(__FILE__), __LINE__, ## __VA_ARGS__); }
            #define ALOGV(FMT, ...) { if (aLogLevel >= ALOG_VERBOSE_LEVEL) pr_info("%-8s: [%s:%d]: " FMT, TAG, kbasename(__FILE__), __LINE__, ## __VA_ARGS__); }
            #define ALOGI(FMT, ...) { if (aLogLevel >= ALOG_INFO_LEVEL) pr_notice("%-8s: [%s:%d]: " FMT, TAG, kbasename(__FILE__), __LINE__, ## __VA_ARGS__); }
            #define ALOGW(FMT, ...) { if (aLogLevel >= ALOG_WARNING_LEVEL) pr_warn("%-8s: [%s:%d]: " FMT, TAG, kbasename(__FILE__), __LINE__, ## __VA_ARGS__); }
            #define ALOGE(FMT, ...) pr_err("%-8s: [%s:%d]: " FMT, TAG, kbasename(__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGF(FMT, ...) pr_crit("%-8s: [%s:%d]: " FMT, TAG, kbasename(__FILE__), __LINE__, ## __VA_ARGS__)
        #else
            #define ALOGD(FMT, ...)                                             \
                {                                                               \
                    if (aLogLevel >= ALOG_DEBUG_LEVEL)                          \
                    {                                                           \
                        struct timespec tp;                                     \
                        clockid_t clk_id = CLOCK_MONOTONIC;                     \
                        clock_gettime(clk_id, &tp);                             \
                        printf("%6ld.%03ld D %-8s: [%s:%d]: " FMT "\n",         \
                                    tp.tv_sec, (tp.tv_nsec/1000000), TAG,       \
                                    basename((char *)__FILE__), __LINE__,       \
                                    ## __VA_ARGS__);                            \
                    }                                                           \
                }
            #define ALOGV(FMT, ...)                                             \
                {                                                               \
                    if (aLogLevel >= ALOG_VERBOSE_LEVEL)                        \
                    {                                                           \
                        struct timespec tp;                                     \
                        clockid_t clk_id = CLOCK_MONOTONIC;                     \
                        clock_gettime(clk_id, &tp);                             \
                        printf("%6ld.%03ld V %-8s: [%s:%d]: " FMT "\n",         \
                                    tp.tv_sec, (tp.tv_nsec/1000000), TAG,       \
                                    basename((char *)__FILE__), __LINE__,       \
                                    ## __VA_ARGS__);                            \
                    }                                                           \
                }
            #define ALOGI(FMT, ...)                                             \
                {                                                               \
                    if (aLogLevel >= ALOG_INFO_LEVEL)                           \
                    {                                                           \
                        struct timespec tp;                                     \
                        clockid_t clk_id = CLOCK_MONOTONIC;                     \
                        clock_gettime(clk_id, &tp);                             \
                        printf("%6ld.%03ld I %-8s: [%s:%d]: " FMT "\n",         \
                                    tp.tv_sec, (tp.tv_nsec/1000000), TAG,       \
                                    basename((char *)__FILE__), __LINE__,       \
                                    ## __VA_ARGS__);                            \
                    }                                                           \
                }
            #define ALOGW(FMT, ...)                                             \
                {                                                               \
                    if (aLogLevel >= ALOG_WARNING_LEVEL)                        \
                    {                                                           \
                        struct timespec tp;                                     \
                        clockid_t clk_id = CLOCK_MONOTONIC;                     \
                        clock_gettime(clk_id, &tp);                             \
                        printf("%6ld.%03ld W %-8s: [%s:%d]: " FMT "\n",         \
                                    tp.tv_sec, (tp.tv_nsec/1000000), TAG,       \
                                    basename((char *)__FILE__), __LINE__,       \
                                    ## __VA_ARGS__);                            \
                    }                                                           \
                }
            #define ALOGE(FMT, ...)                                             \
                {                                                               \
                    struct timespec tp;                                         \
                    clockid_t clk_id = CLOCK_MONOTONIC;                         \
                    clock_gettime(clk_id, &tp);                                 \
                    printf("%6ld.%03ld E %-8s: [%s:%d]: " FMT "\n",             \
                                tp.tv_sec, (tp.tv_nsec/1000000), TAG,           \
                                basename((char *)__FILE__), __LINE__,           \
                                ## __VA_ARGS__);                                \
                }
            #define ALOGF(FMT, ...)                                             \
                {                                                               \
                    struct timespec tp;                                         \
                    clockid_t clk_id = CLOCK_MONOTONIC;                         \
                    clock_gettime(clk_id, &tp);                                 \
                    printf("%6ld.%03ld F %-8s: [%s:%d]: " FMT "\n",             \
                                tp.tv_sec, (tp.tv_nsec/1000000), TAG,           \
                                basename((char *)__FILE__), __LINE__,           \
                                ## __VA_ARGS__);                                \
                }
        #endif
    #endif
#else
    #define ALOGD(FMT, ...)
    #define ALOGV(FMT, ...)
    #define ALOGI(FMT, ...)
    #define ALOGW(FMT, ...)
    #define ALOGE(FMT, ...)
    #define ALOGF(FMT, ...)
#endif


#if defined(__ANDROID__)
    //Exception
    #if defined(__cplusplus)
        #define AEXCP(...) new Exception (basename(__FILE__), __LINE__, ## __VA_ARGS__)
    #endif
#endif

#endif /* ALOG_H_ */
