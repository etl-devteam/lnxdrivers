/*
 * visionarusbtype header file
 *
 * exported enum, types and structures of libvisionarusb.a library are declared
 */


#ifndef LIBVISIONAR_VISIONARUSBTYPES_H_
#define LIBVISIONAR_VISIONARUSBTYPES_H_


/******************************************************************************
 * ENUM
 ******************************************************************************/
/** VisionAR Smart Glasses connection events */
enum VisionAREvent {
    VISIONAR_DISCONNECTION_EVENT = 0,
    VISIONAR_CONNECTION_EVENT
};

/** VisionAR Smart Camera LED colors */
enum VisionARLedCameraColor {
    VISIONAR_CAMERA_GREEN_COLOR = 0,
    VISIONAR_CAMERA_RED_COLOR = 2,
    VISIONAR_CAMERA_OFF_COLOR = 3
};


/******************************************************************************
 * TYPEDEF
 ******************************************************************************/
// Callback function prototypes
typedef void (*EventCallback) (VisionAREvent);
typedef void (*TouchpadCallback) (unsigned char);


#endif /* LIBVISIONAR_VISIONARUSBTYPES_H_ */
