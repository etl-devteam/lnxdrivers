/*
 * libvisionarusb header file
 *
 * In this module, exported API of libvisionarusb.a library are declared
 */


#ifndef TEST_LIBVISIONARUSB_H_
#define TEST_LIBVISIONARUSB_H_


#include "visionarusbtypes.h"


/******************************************************************************
 * FUNCTIONS
 ******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif


/* Init/reset functions */
/**
 * Initialize environment to use USB interface
 *
 * \param cb callback function, invoked when VisionAR Smart Glasses is plugged-in or
 * out according to passed argument
 * \returns true if initialization is completed without errors; false otherwise
 */
bool Startup(EventCallback cb);

/**
 * Destroy all the objects related to USB interface for VisionAR
 *
 * \returns true if operation is completed without errors; false otherwise
 */
bool CloseDev(void);


/* Framebuffer functions */
/**
 * Store image to be sent to VisionAR Glass
 *
 * \param data contains image in ARGB32 format (4 bytes for each pixel), starting
 * from top left pixel to bottom right one
 * \returns true if image is stored on device; false otherwise
 */
bool WriteImg(unsigned char *data);

/**
 * Set contrast value of VisionAR Glass
 *
 * \param val contrast value (range: 0 - 255)
 * \returns true if value is properly set; false otherwise
 */
bool Contrast(int val);
/**
 * Set brightness value of VisionAR Glass
 *
 * \param val brightness value (range: 0 - 255)
 * \returns true if value is properly set; false otherwise
 */
bool Brightness(int val);

/* CDC functions */
/**
 * let VisionAR Glass vibrate for given period
 *
 * \param vibration time in ms
 * \returns true if operations was performed; false otherwise
 */
bool Haptic(unsigned int ms);

/**
 * Enable reading of VisionAR Smart Glasses touchpad input
 *
 * \param cb callback function, invoked when an input from VisionAR Smart Glasses
 * touchpad was received
 * \param ms polling time of thread, reading inputs from VisionAR Smart Glasses touchpad
 * (if 0, default value - 250ms - is used)
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool StartTouchpad(TouchpadCallback callback, unsigned int ms);
/**
 * Disable reading of VisionAR Smart Glasses touchpad input
 *
 * \returns true if reading procedure is disabled; false otherwise
 */
bool StopTouchpad();

/**
 * Get acceleration values from IMU sensor (in m/s^2)
 *
 * \param pImuAcc output location for 3-axis values
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetImuAcc(float *pImuAcc);
/**
 * Get acceleration values from IMU sensor (register values)
 *
 * \param pImuAcc output location for 3-axis values
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetImuAccReg(short int *pImuAcc);

/**
 * Get angular rate values from IMU sensor (in degree per seconds)
 *
 * \param pImuGyro output location for 3-axis values
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetImuGyro(float *pImuGyro);
/**
 * Get angular rate values from IMU sensor (register values)
 *
 * \param pImuGyro output location for 3-axis values
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetImuGyroReg(short int *pImuGyro);

/**
 * Get magnetic field values from IMU sensor (in gauss)
 *
 * \param pImuMag output location for 3-axis values
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetImuMag(float *pImuMag);
/**
 * Get magnetic field values from IMU sensor (register values)
 *
 * \param pImuMag output location for 3-axis values
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetImuMagReg(short int *pImuMag);

/**
 * Get protocol version of VisionAR Glass (x.y)
 *
 * \param pProtVers output location for major and minor version
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetProtVers(unsigned char *pProtVers);
/**
 * Get boot version of VisionAR Glass (x.y)
 *
 * \param pBootVers output location for major and minor version
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetBootVers(unsigned char *pBootVers);
/**
 * Get application version of VisionAR Glass (x.y.z)
 *
 * \param pBootVers output location for major and minor version and patch number
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetApplVers(unsigned char *pApplVers);

/**
 * Get VisionAR Glass device status
 * Bit      Description                     Value
 *  0 -  3  Status of FrameBuffer automa    0 = Init
 *                                          1 = Start
 *                                          2 = Idle
 *                                          3 = Header
 *                                          4 = Checksum
 *                                          5 = Payload
 *                                          6 = Error
 *  4 -  7  Status of Serial COM automa     0 = Init
 *                                          1 = Start
 *                                          2 = Idle
 *                                          3 = Header
 *                                          4 = Checksum
 *                                          5 = Payload
 *                                          6 = Reply
 *                                          7 = Send
 *                                          8 = Error
 *  8       Errors active                   0 = NO errors
 *                                          1 = At least 1 error active
 *  9       IMU calibrated                  0 = IMU NOT calibrated
 *                                          1 = IMU calibrated
 * 10       IMU calibration                 0 = NOT running
 *                                          1 = Calibration is running
 *
 * \param pBootVers output location for status value
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetStatus(unsigned int *stat);

/**
 * Get VisionAR Glass errors list
 * Code     Description
 * 0x0001   Touch controller not responding (missing I2C)
 * 0x0002   Error while setting up touch registers
 * 0x0003   Error while reading touch registers
 * 0x0004   Error while setting up lux sensor registers
 * 0x0005   Error while reading lux registers
 * 0x0006   Accelerometer not responding (missing I2C)
 * 0x0007   Error while setting up accelerometer registers
 * 0x0008   Error while reading accelerometer registers
 * 0x0009   Magnetometer not responding (missing I2C)
 * 0x000A   Error while setting up magnetometer registers
 * 0x000B   Error while reading magnetometer registers
 * 0x000C   Error while setting up display registers
 *
 * \param num output location for number of errors in list
 * \param errors output location for errors list
 * \returns true if reading procedure is completed without errors; false otherwise
 */
bool GetErrors(unsigned char *num, unsigned short *errors);

/**
 * VisionAR Glass reset command
 *
 * \returns true if procedure is completed without errors; false otherwise
 */
bool Reset(void);
/**
 * VisionAR Glass power command
 *
 * \param off if true, VisionAR Glass is switched off
 * \returns true if procedure is completed without errors; false otherwise
 */
bool PowerOffLcd(bool off);
/**
 * VisionAR Smart Camera power command
 *
 * \param off if true, camera is switched off
 * \returns true if procedure is completed without errors; false otherwise
 */
bool PowerOffCamera(bool off);

/**
 * VisionAR Smart Camera LED command
 *
 * \param color color of the camera LED
 * - VISIONAR_CAMERA_GREEN_COLOR -> green led
 * - VISIONAR_CAMERA_RED_COLOR -> red led
 * - VISIONAR_CAMERA_OFF_COLOR -> led off
 *
 * \returns true if procedure is completed without errors; false otherwise
 */
bool LedCamera(VisionARLedCameraColor color);


#ifdef __cplusplus
}
#endif

#endif /* TEST_LIBVISIONAR_H_ */
