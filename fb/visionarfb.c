/*
 * visionarfb.c -- Framebuffer driver for eRGlass device
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/usb.h>
#include <linux/mm.h>
#include <linux/fb.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/uaccess.h>

#include "alog.h"
#include "visionarfb.h"


/* warning! need write-all permission so overriding check */
#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)


/*****************************************************************************
 * ALog definitions
 *****************************************************************************/

static const char *TAG = "VAR_FB";

static int aLogLevel = ALOG_INFO_LEVEL;

/*****************************************************************************/


/*****************************************************************************
 * Device lock
 *****************************************************************************/

extern int visionar_lock(const char *);
extern void visionar_unlock(const char *);
extern int visionar_is_locked(const char *);

int (* p_lock)(const char *);
void (* p_unlock)(const char *);

/*****************************************************************************/


/*****************************************************************************
 * LCD DEFINES
 *****************************************************************************/

#define WIDTH               419
#define HEIGHT              138
#define BIT_X_PIXEL          32

/*****************************************************************************/


/*****************************************************************************
 * Structures for communication with eRGlass
 *****************************************************************************/

#define HEADER_LEN          5
#define DATA_LEN(l)         (2+l)

/* registers map */
#define eRCMD_BITMAP        0x01
#define eRCMD_REGISTER      0x11

#define REG_CONTRAST        0x24
#define REG_BRIGHTNESS      0x25

#define EDID_LENGTH         0x80

#pragma pack(push, 1)
typedef struct {
    uint8_t     _SOH;
    uint8_t     control;
    uint16_t    total;
    uint8_t     header_chk;
    uint8_t     command;
    uint8_t     data[0x10000];
} FRAME;
// general pipe 
typedef struct {
    void          *pfirst;
    void          *pw;
    void          *pr;
    void          *plast;
    unsigned char szUserType;
} PIPE;

// command store
typedef struct {
    unsigned char   command;
    unsigned char   data[32];
    unsigned char   len;
} COMMAND;
#pragma pack(pop)

static FRAME m_frame;

/*****************************************************************************/


/*****************************************************************************
 * Structures for driver
 *****************************************************************************/
static unsigned int m_Connect = 0;              /* device connection status */
static struct urb *m_Sent = NULL;               /* urb submitted, but not yet completed! */
static int CounterWaitCompletation = 0;
static struct fb_info *m_pInfo;



static struct fb_fix_screeninfo eRGlassfb_fix = {
    .id =           "eRGlassFb",
    .type =         FB_TYPE_PACKED_PIXELS,
    .visual =       FB_VISUAL_DIRECTCOLOR,
    .xpanstep =     0,
    .ypanstep =     0,
    .ywrapstep =    0,
    .accel =        FB_ACCEL_NONE,
};

const struct fb_videomode erglass_modes[] = {
    { NULL, 85, WIDTH, HEIGHT, 31746,  96, 32, 60, 32, 64, 3,
      FB_SYNC_HOR_HIGH_ACT, FB_VMODE_NONINTERLACED, FB_MODE_IS_VESA },
};

static const u32 ueRGlassfb_info_flags = FBINFO_DEFAULT | FBINFO_READS_FAST |
        FBINFO_VIRTFB |
        FBINFO_HWACCEL_IMAGEBLIT | FBINFO_HWACCEL_FILLRECT |
        FBINFO_HWACCEL_COPYAREA | FBINFO_MISC_ALWAYS_SETPAR;

static struct usb_device_id id_table[] = {
    {
        .idVendor = 0x0483,
        .idProduct = 0xA306,
        .bInterfaceClass = 0x0A,
        .bInterfaceSubClass = 0x00,
        .bInterfaceProtocol = 0x00,
        .match_flags = USB_DEVICE_ID_MATCH_VENDOR | USB_DEVICE_ID_MATCH_INT_CLASS | USB_DEVICE_ID_MATCH_INT_SUBCLASS | USB_DEVICE_ID_MATCH_INT_PROTOCOL,
    },
    {},
};
MODULE_DEVICE_TABLE(usb, id_table);

/*****************************************************************************/


/*****************************************************************************
 * Module options
 *****************************************************************************/

/* enable verbose */
static int verbose      = ALOG_INFO_LEVEL;
module_param(verbose, int, S_IWUSR | S_IRUGO);
MODULE_PARM_DESC(verbose, "print additional information");

/* holographic display refresh period (in ms) */
static int m_LcdRefresh = 250;
module_param(m_LcdRefresh, int,  S_IWUSR | S_IRUGO);
MODULE_PARM_DESC(m_LcdRefresh, "Time refresh in milliesecond");

/*****************************************************************************/


/*****************************************************************************
 * Driver functions
 *****************************************************************************/
/* Prototypes */
static void eRGlassfb_urb_completion(struct urb *urb);
#if 0
static struct urb *eRGlassfb_get_urb(struct eRGlassfb_data *dev);
static int eRGlassfb_submit_urb(struct eRGlassfb_data *dev, struct urb *urb, size_t len);
static int eRGlassfb_alloc_urb_list(struct eRGlassfb_data *dev, int count, size_t size);
static void eRGlassfb_free_urb_list(struct eRGlassfb_data *dev);
#endif
static void eRGlassfb_init_framebuffer_work(struct work_struct *work);

struct urb *m_FrameBufferUrb;

static PIPE pipeCommand;
static COMMAND commandBuffer[10];

//////////////////////////////////////////////////////////////////////////////
static void command_init(void)
{
    ALOGV("%s", __func__);

    pipeCommand.pfirst = (void *)&commandBuffer[0];
    pipeCommand.pw =(void *)&commandBuffer[0];
    pipeCommand.pr =(void *)&commandBuffer[0];
    pipeCommand.plast =(void *)&commandBuffer[9];
    pipeCommand.szUserType = sizeof(COMMAND);    
}

//////////////////////////////////////////////////////////////////////////////
static bool command_is(void)
{
    return (pipeCommand.pr != pipeCommand.pw);    
}

//////////////////////////////////////////////////////////////////////////////
static void command_put(COMMAND cmd)
{
    ALOGV("%s: command = %02X", __func__, (unsigned int)cmd.command);

    memcpy((unsigned char *)pipeCommand.pw, (unsigned char *)&cmd, pipeCommand.szUserType);
    
    if (pipeCommand.pw == pipeCommand.plast)
        pipeCommand.pw = pipeCommand.pfirst;  
    else              
        pipeCommand.pw = (void *)((char *)(pipeCommand.pw + pipeCommand.szUserType));
}

//////////////////////////////////////////////////////////////////////////////
static void command_get(COMMAND *cmd)
{
    memcpy((unsigned char *)cmd, (unsigned char *)pipeCommand.pr, pipeCommand.szUserType);
    
    if (pipeCommand.pr == pipeCommand.plast)
        pipeCommand.pr = pipeCommand.pfirst;  
    else              
        pipeCommand.pr = (void *)((char *)(pipeCommand.pr + pipeCommand.szUserType));

    ALOGV("%s: command = %02X", __func__, (unsigned int)cmd->command);
}


/* perform fb specific mmap */
static int eRGlassfb_ops_mmap(struct fb_info *info, struct vm_area_struct *vma)
{
    unsigned long start = vma->vm_start;
    unsigned long size = vma->vm_end - vma->vm_start;
    unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
    unsigned long page, pos;

    ALOGV("%s", __func__);

    if (vma->vm_pgoff > (~0UL >> PAGE_SHIFT))
        return -EINVAL;
    if (size > info->fix.smem_len)
        return -EINVAL;
    if (offset > info->fix.smem_len - size)
        return -EINVAL;

    pos = (unsigned long)info->fix.smem_start + offset;

    ALOGV("%s: framebuffer addr:%lX size:%lu", __func__, pos, size);

    while (size > 0) {
        page = vmalloc_to_pfn((void *)pos);
        if (remap_pfn_range(vma, start, page, PAGE_SIZE, PAGE_SHARED))
            return -EAGAIN;

        start += PAGE_SIZE;
        pos += PAGE_SIZE;
        if (size > PAGE_SIZE)
            size -= PAGE_SIZE;
        else
            size = 0;
    }

    return 0;
}


static void memcpyConvert(unsigned char *dst,  const unsigned char *src, int len)
{
    int ii, jj;
    int a,r,g,b;
    int width = WIDTH, height = HEIGHT;
    unsigned int *pSrc = (unsigned int *)src;

    ALOGV("%s", __func__);

    for(ii = 0; ii < height; ii++) {
        for(jj = 0; jj < width; jj++) {
            a = 0;
            r = 0;
            g = 0;
            b = 0;
            if(jj < width) {
                a = (pSrc[ii * width + jj] & 0xFF000000) >> 24;
                r = (pSrc[ii * width + jj] & 0x00FF0000) >> 16;
                g = (pSrc[ii * width + jj] & 0x0000FF00) >> 8;
                b =  pSrc[ii * width + jj] & 0x000000FF;
            }
            dst[ii * width + jj] = ((r * 229 + g * 587 + b * 114) / 1000) * a / 255;
        }
    }
}


int eRGlassfb_prepareframe(char command, const char *pdata, int len)
{
    int ii, framelen;
    char *p;

    ALOGV("%s(%02X, %d)", __func__, command, len);

    m_frame._SOH = 0x01;
    m_frame.control = 0x01;
    m_frame.total = DATA_LEN(len);
    m_frame.header_chk = 0x00;
    m_frame.command = command;

    if(command == eRCMD_BITMAP){
        memcpyConvert(m_frame.data, pdata, len);
    } else {
        memcpy(m_frame.data, pdata, len);
    }

    m_frame.data[len] = 0x00;

    // checksum header
    p = (char *)&m_frame._SOH;
    for (ii = 0; ii < 4; ii++)
        m_frame.header_chk ^= *p++;

    // checksum data
    p = (char *)&m_frame.command;
    for (ii = 0; ii < len+1; ii++)
        m_frame.data[len] ^= *p++;

    framelen = HEADER_LEN+DATA_LEN(len);

    ALOGD("%s:\n"
            "\tSOH             =   %02Xh\n"
            "\tcontrol         =   %02Xh\n"
            "\ttotal bytes     = %04Xh\n"
            "\theader checksum =   %02Xh\n"
            "\tcommand         =   %02Xh\n"
            "\tdata checksum   =   %02Xh",
            __func__,
            m_frame._SOH, m_frame.control, m_frame.total, m_frame.header_chk, m_frame.command, (int)m_frame.data[len]);

    return framelen;
}


static ssize_t eRGlassfb_ops_write(struct fb_info *info, const char __user *buf, size_t count, loff_t *ppos)
{
#if 0
    ssize_t result;
    struct eRGlassfb_data *dev = info->par;
    int lendata = WIDTH*HEIGHT;

    ALOGV("%s(%lu)", __func__, count);

    if (count != lendata) {
        ALOGW("%s: data size error [rx=%lu instead %d]", __func__, count, lendata);
    }

    result = fb_sys_write(info, buf, count, ppos);

    if (result > 0) {
        int len;
        char *bufptr;
        struct urb *urb;

        mutex_lock(&dev->info->lock);

        urb = eRGlassfb_get_urb(dev);
        if (!urb) {
            mutex_unlock(&dev->info->lock);
            ALOGW("%s: get urb failed [dev=%p]", __func__, dev);
            return 0;
        }

        len = eRGlassfb_prepareframe(eRCMD_BITMAP, buf, lendata);
        bufptr = (char *)urb->transfer_buffer;
        memcpy(bufptr, &m_frame, len);

        eRGlassfb_submit_urb(dev, urb, len);

        mutex_unlock(&dev->info->lock);
    }
    else {
        ALOGE("%s: error [result=%lu]", __func__, result);
    }

    return result;
#else
    return 0;
#endif
}

#if 0
int erglassfb_handle_lcdrefresh(struct eRGlassfb_data *dev, char *data)
{
    int len;
    char *cmd;
    struct urb *urb;
    char *bufptr;

    ALOGV("%s", __func__);

    mutex_lock(&dev->info->lock);

    urb = eRGlassfb_get_urb(dev);
    if (!urb) {
        ALOGE("%s: no urb found!", __func__);
        mutex_unlock(&dev->info->lock);
        return 0;
    }

    cmd = urb->transfer_buffer;

    len = eRGlassfb_prepareframe(eRCMD_BITMAP, data, WIDTH*HEIGHT);
    bufptr = (char *)urb->transfer_buffer;

    if(bufptr){
        memcpy(bufptr, &m_frame, len);
        eRGlassfb_submit_urb(dev, urb, len);
    }

    mutex_unlock(&dev->info->lock);

    return 0;
}
#endif

static int eRGlassfb_get_edid(struct eRGlassfb_data *dev, char *edid, int len)
{
    int i;
    int ret;
    char *rbuf;

    ALOGV("%s", __func__);

    rbuf = kmalloc(2, GFP_KERNEL);
    if (!rbuf)
        return 0;

    for (i = 0; i < len; i++) {
        ret = usb_control_msg(dev->udev,
                    usb_rcvctrlpipe(dev->udev, 0), (0x02),
                    (0x80 | (0x02 << 5)), i << 8, 0xA1, rbuf, 2,
                    HZ);
        if (ret < 1) {
            ALOGE("%s: Read EDID byte %d failed err %x", __func__, i, ret);
            i--;
            break;
        }
        edid[i] = rbuf[1];
    }

    kfree(rbuf);

    return i;
}


/* perform fb specific ioctl (optional) */
static int eRGlassfb_ops_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg)
{
    struct eRGlassfb_data *dev = info->par;

    ALOGV("%s(%02X)", __func__, cmd);

    if (!atomic_read(&dev->usb_active))
        return 0;

    if (cmd == ERGLASSFB_IOCTL_RETURN_EDID) {
        void __user *edid = (void __user *)arg;
        if (copy_to_user(edid, dev->edid, dev->edid_size))
            return -EFAULT;
    }
    else if (cmd == ERGLASSFB_IOCTL_LCD_REFRESH) {
#if 0
        erglassfb_handle_lcdrefresh(dev, (char *)info->fix.smem_start);
#endif
    }

    return 0;
}


static int eRGlassfb_ops_open(struct fb_info *info, int user)
{
    struct eRGlassfb_data *dev = info->par;

    ALOGV("%s (%d)", __func__, dev->fb_count);

    /*
     * fbcon aggressively connects to first framebuffer it finds,
     * preventing other clients (X) from working properly. Usually
     * not what the user wants. Fail by default with option to enable.
     */
    if ((user == 0))
        return -EBUSY;

    /* If the USB device is gone, we don't accept new opens */
    if (dev->virtualized)
        return -ENODEV;

    dev->fb_count++;

    kref_get(&dev->kref);

    ALOGI("%s: opened /dev/fb%d user=%d fb_info=%p count=%d", __func__, info->node, user, info, dev->fb_count);

    return 0;
}


/*
 * Called when all client interfaces to start transactions have been disabled,
 * and all references to our device instance (eRGlassfb_data) are released.
 * Every transaction must have a reference, so we know are fully spun down
 */
static void eRGlassfb_free(struct kref *kref)
{
    struct eRGlassfb_data *dev = container_of(kref, struct eRGlassfb_data, kref);

    if (dev->backing_buffer)
        vfree(dev->backing_buffer);

    kfree(dev->edid);

    ALOGI("%s: freeing eRGlassfb_data %p", __func__, dev);

    kfree(dev);
}


#if 0
static void eRGlassfb_release_urb_work(struct work_struct *work)
{
    struct urb_node *unode = container_of(work, struct urb_node, release_urb_work.work);

    ALOGV("%s", __func__);

    up(&unode->dev->urbs.limit_sem);
}
#endif


static void eRGlassfb_free_framebuffer(struct eRGlassfb_data *dev)
{
    ALOGV("%s", __func__);

    if (m_pInfo) {
        int node = m_pInfo->node;

        unregister_framebuffer(m_pInfo);
        ALOGI("%s: fb_info for /dev/fb%d has been freed", __func__, node);
        return;

#if 0
        if (m_pInfo->cmap.len != 0)
            fb_dealloc_cmap(&m_pInfo->cmap);

        if (m_pInfo->monspecs.modedb)
            fb_destroy_modedb(m_pInfo->monspecs.modedb);

        if (m_pInfo->screen_base)
            vfree(m_pInfo->screen_base);

        fb_destroy_modelist(&m_pInfo->modelist);
        m_pInfo = NULL;

        /* Assume info structure is freed after this point */
        framebuffer_release(m_pInfo);
#endif
    }

    /* ref taken in probe() as part of registering framebfufer */
    kref_put(&dev->kref, eRGlassfb_free);
}


static void eRGlassfb_free_framebuffer_work(struct work_struct *work)
{
    struct eRGlassfb_data *dev = container_of(work, struct eRGlassfb_data, free_framebuffer_work.work);
    eRGlassfb_free_framebuffer(dev);
}


static int eRGlassfb_ops_release(struct fb_info *info, int user)
{
    /*
     * Assumes caller is holding info->lock mutex (for open and release at least)
     */
    struct eRGlassfb_data *dev = info->par;

    ALOGV("%s (%d)", __func__, dev->fb_count);

    if(dev->fb_count){
        dev->fb_count--;
    }

#if 0
    /* performed by disconnection function */
    /* We can't free fb_info here - fbmem will touch it when we return */
    if (dev->virtualized && (dev->fb_count == 0))
        schedule_delayed_work(&dev->free_framebuffer_work, HZ);
#endif

    ALOGI("%s: released /dev/fb%d user=%d count=%d", __func__, info->node, user, dev->fb_count);

    kref_put(&dev->kref, eRGlassfb_free);

    return 0;
}


/*
 * Check whether a video mode is supported by the DisplayLink chip
 * We start from monitor's modes, so don't need to filter that here
 */
static int eRGlassfb_is_valid_mode(struct fb_videomode *mode, struct fb_info *info)
{
    struct eRGlassfb_data *dev = info->par;

    ALOGV("%s", __func__);

    if (mode->xres * mode->yres > dev->sku_pixel_limit) {
        ALOGW("%s: %dx%d beyond chip capabilities", __func__, mode->xres, mode->yres);
        return 0;
    }

    ALOGI("%s: %dx%d @ %d Hz valid mode", __func__, mode->xres, mode->yres, mode->refresh);

    return 1;
}


static void eRGlassfb_var_color_format(struct fb_var_screeninfo *var)
{
    // 32 bits_per_pixel
    const struct fb_bitfield red = {16, 8, 0};
    const struct fb_bitfield green =  {8, 8, 0};
    const struct fb_bitfield blue  = {0, 8, 0};
    const struct fb_bitfield transp =  {24, 8, 0};

    ALOGV("%s", __func__);

    var->bits_per_pixel = BIT_X_PIXEL;
    var->red = red;
    var->green = green;
    var->blue = blue;
    var->transp = transp;
}


static int eRGlassfb_ops_check_var(struct fb_var_screeninfo *var, struct fb_info *info)
{
    struct fb_videomode mode;

    ALOGV("%s", __func__);

    /* TODO: support dynamically changing framebuffer size */
    if ((var->xres * var->yres * 2) > info->fix.smem_len) {
        ALOGE("%s(%u, %u): resolution too big!", __func__, var->xres, var->yres);
        return -EINVAL;
    }

    /* set device-specific elements of var unrelated to mode */
    eRGlassfb_var_color_format(var);

    fb_var_to_videomode(&mode, var);

    if (!eRGlassfb_is_valid_mode(&mode, info)) {
        ALOGE("%s: not valid mode!", __func__);
        return -EINVAL;
    }

    return 0;
}


static int eRGlassfb_ops_set_par(struct fb_info *info)
{
#if 0
    struct eRGlassfb_data *dev = info->par;
    int result = 0;
    u16 *pix_framebuffer;
    int i;

    ALOGV("%s(%dx%d)", __func__, info->var.xres, info->var.yres);

    result = eRGlassfb_set_video_mode(dev, &info->var);

    if ((result == 0) && (dev->fb_count == 0)) {

        /* paint greenscreen */

        pix_framebuffer = (u16 *) info->screen_base;
        for (i = 0; i < info->fix.smem_len / 2; i++)
            pix_framebuffer[i] = 0x37e6;

        eRGlassfb_handle_damage(dev, 0, 0, info->var.xres, info->var.yres,
                   info->screen_base);
    }

    return result;
#else
    ALOGV("%s(%dx%d)", __func__, info->var.xres, info->var.yres);

    return 0;
#endif
}


/* set color registers in batch */
static int eRGlassfb_set_user_cmap(struct fb_cmap *cmap, struct fb_info *info)
{
#if 0
    //struct eRGlassfb_data *dev = info->par;
    int result = 0;
//    u16 *pix_framebuffer;
//    int i;

    ALOGV("%s(%dx%d)", __func__, info->var.xres, info->var.yres);

    result = eRGlassfb_set_video_mode(dev, &info->var);

    if ((result == 0) && (dev->fb_count == 0)) {

        /* paint greenscreen */

        pix_framebuffer = (u16 *) info->screen_base;
        for (i = 0; i < info->fix.smem_len / 2; i++)
            pix_framebuffer[i] = 0x37e6;

        eRGlassfb_handle_damage(dev, 0, 0, info->var.xres, info->var.yres,
                   info->screen_base);
    }
#else
    ALOGV("%s(%dx%d)", __func__, info->var.xres, info->var.yres);

    return 0;
#endif
}


/*
 *  Frame buffer operations
 */
static struct fb_ops visionarfb_ops = {
    .owner          = THIS_MODULE,
    .fb_open        = eRGlassfb_ops_open,
    .fb_release     = eRGlassfb_ops_release,
    .fb_read        = fb_sys_read,
    .fb_write       = eRGlassfb_ops_write,
    .fb_check_var   = eRGlassfb_ops_check_var,
    .fb_set_par     = eRGlassfb_ops_set_par,
#if 0
     int (*fb_setcolreg)(unsigned regno, unsigned red, unsigned green, unsigned blue, unsigned transp, struct fb_info *info);
#endif
    .fb_setcmap     = eRGlassfb_set_user_cmap,
#if 0
     int (*fb_blank)(int blank, struct fb_info *info);
     int (*fb_pan_display)(struct fb_var_screeninfo *var, struct fb_info *info);
     void (*fb_fillrect) (struct fb_info *info, const struct fb_fillrect *rect);
     void (*fb_copyarea) (struct fb_info *info, const struct fb_copyarea *region);
     void (*fb_imageblit) (struct fb_info *info, const struct fb_image *image);
#endif
    .fb_ioctl       = eRGlassfb_ops_ioctl,
#if 0
    int (*fb_compat_ioctl)(struct fb_info *info, unsigned cmd, unsigned long arg);
#endif
    .fb_mmap        = eRGlassfb_ops_mmap,
#if 0
     void (*fb_get_caps)(struct fb_info *info, struct fb_blit_caps *caps, struct fb_var_screeninfo *var);
     void (*fb_destroy)(struct fb_info *info);
     int (*fb_debug_enter)(struct fb_info *info);
     int (*fb_debug_leave)(struct fb_info *info);
#endif
};


/*
 * Assumes &info->lock held by caller
 * Assumes no active clients have framebuffer open
 */
static int eRGlassfb_realloc_framebuffer(struct eRGlassfb_data *dev, struct fb_info *info)
{
    int retval = -ENOMEM;
    int old_len = info->fix.smem_len;
    int new_len;
    unsigned char *old_fb = info->screen_base;
    unsigned char *new_fb;

    ALOGW("%s: Reallocating framebuffer. Addresses will change!", __func__);

    new_len = info->fix.line_length * info->var.yres;

    if (PAGE_ALIGN(new_len) > old_len) {
        /*
         * Alloc system memory for virtual framebuffer
         */
        new_fb = vmalloc(new_len);
        if (!new_fb) {
            ALOGE("%s: Virtual framebuffer alloc failed", __func__);
            goto error;
        }

        if (info->screen_base) {
            memcpy(new_fb, old_fb, old_len);
            vfree(info->screen_base);
        }

        info->screen_base = new_fb;
        info->fix.smem_len = PAGE_ALIGN(new_len);
        info->fix.smem_start = (unsigned long) new_fb;
        info->flags = ueRGlassfb_info_flags;
    }

    retval = 0;

error:
    return retval;
}


/*
 * 1) Get EDID from hw, or use sw default
 * 2) Parse into various fb_info structs
 * 3) Allocate virtual framebuffer memory to back highest res mode
 *
 * Parses EDID into three places used by various parts of fbdev:
 * fb_var_screeninfo contains the timing of the monitor's preferred mode
 * fb_info.monspecs is full parsed EDID info, including monspecs.modedb
 * fb_info.modelist is a linked list of all monitor & VESA modes which work
 *
 * If EDID is not readable/valid, then modelist is all VESA modes,
 * monspecs is NULL, and fb_var_screeninfo is set to safe VESA mode
 * Returns 0 if successful
 */
static int eRGlassfb_setup_modes(struct eRGlassfb_data *dev,
               struct fb_info *info,
               char *default_edid, size_t default_edid_size)
{
    int i;
    const struct fb_videomode *default_vmode = NULL;
    int result = 0;
    char *edid;
    int tries = 3;

    ALOGV("%s", __func__);

    if (info->dev) /* only use mutex if info has been registered */
        mutex_lock(&info->lock);

    edid = kmalloc(EDID_LENGTH, GFP_KERNEL);
    if (!edid) {
        result = -ENOMEM;
        goto error;
    }

    fb_destroy_modelist(&info->modelist);
    memset(&info->monspecs, 0, sizeof(info->monspecs));

    /*
     * Try to (re)read EDID from hardware first
     * EDID data may return, but not parse as valid
     * Try again a few times, in case of e.g. analog cable noise
     */
    while (tries--) {
        i = eRGlassfb_get_edid(dev, edid, EDID_LENGTH);

        if (i >= EDID_LENGTH)
            fb_edid_to_monspecs(edid, &info->monspecs);

        if (info->monspecs.modedb_len > 0) {
            dev->edid = edid;
            dev->edid_size = i;
            break;
        }
    }

    /* If that fails, use a previously returned EDID if available */
    if (info->monspecs.modedb_len == 0) {
        ALOGE("%s: Unable to get valid EDID from device/display", __func__);

        if (dev->edid) {
            fb_edid_to_monspecs(dev->edid, &info->monspecs);
            if (info->monspecs.modedb_len > 0)
                ALOGV("%s: Using previously queried EDID", __func__);
        }
    }

    /* If that fails, use the default EDID we were handed */
    if (info->monspecs.modedb_len == 0) {
        if (default_edid_size >= EDID_LENGTH) {
            fb_edid_to_monspecs(default_edid, &info->monspecs);
            if (info->monspecs.modedb_len > 0) {
                memcpy(edid, default_edid, default_edid_size);
                dev->edid = edid;
                dev->edid_size = default_edid_size;
                ALOGV("%s: Using default/backup EDID", __func__);
            }
        }
    }

    /* If we've got modes, let's pick a best default mode */
    if (info->monspecs.modedb_len > 0) {

        for (i = 0; i < info->monspecs.modedb_len; i++) {
            if (eRGlassfb_is_valid_mode(&info->monspecs.modedb[i], info))
                fb_add_videomode(&info->monspecs.modedb[i], &info->modelist);
            else {
                if (i == 0)
                    /* if we've removed top/best mode */
                    info->monspecs.misc &= ~FB_MISC_1ST_DETAIL;
            }
        }

        default_vmode = fb_find_best_display(&info->monspecs, &info->modelist);
    }

    /* If everything else has failed, fall back to safe default mode */
    if (default_vmode == NULL) {
        struct fb_videomode fb_vmode = {0};

        /*
         * Add the standard VESA modes to our modelist
         * Since we don't have EDID, there may be modes that
         * overspec monitor and/or are incorrect aspect ratio, etc.
         * But at least the user has a chance to choose
         */
        for (i = 0; i < 1; i++) {
            if (eRGlassfb_is_valid_mode((struct fb_videomode *) &erglass_modes[i], info))
                fb_add_videomode(&erglass_modes[i], &info->modelist);
        }

        /*
         * default to resolution safe for projectors
         * (since they are most common case without EDID)
         */
        fb_vmode.xres = WIDTH;
        fb_vmode.yres = HEIGHT;
        fb_vmode.refresh = 60;
        default_vmode = fb_find_nearest_mode(&fb_vmode, &info->modelist);
    }

    /* If we have good mode and no active clients*/
    if ((default_vmode != NULL) && (dev->fb_count == 0)) {
        fb_videomode_to_var(&info->var, default_vmode);
        eRGlassfb_var_color_format(&info->var);

        /*
         * with mode size info, we can now alloc our framebuffer.
         */
        memcpy(&info->fix, &eRGlassfb_fix, sizeof(eRGlassfb_fix));
        info->fix.line_length = info->var.xres * (info->var.bits_per_pixel / 8);

        result = eRGlassfb_realloc_framebuffer(dev, info);
    }
    else {
        ALOGE("%s: invalid mode!", __func__);
        result = -EINVAL;
    }

error:
    if (edid && (dev->edid != edid))
        kfree(edid);

    if (info->dev)
        mutex_unlock(&info->lock);

    return result;
}


static ssize_t contrast_show(struct device *fbdev, struct device_attribute *a, char *buf)
{
    struct fb_info *fb_info = dev_get_drvdata(fbdev);
    struct eRGlassfb_data *dev = fb_info->par;

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%u\n", atomic_read(&dev->contrast));
}


static ssize_t contrast_store(struct device *fbdev, struct device_attribute *a, const char *buf, size_t count)
{
    struct fb_info *fb_info = dev_get_drvdata(fbdev);
    struct eRGlassfb_data *dev = fb_info->par;
    long contrast;
#if 0
    struct urb *urb;
    char *bufptr;
    char buffer[2];
    int lendata, len;
#else
    COMMAND cmd;
#endif
    if (kstrtol(buf, 10, &contrast)) {
        ALOGW("%s(%s): kstrtol error", __func__, buf);
        return 0;
    }

    ALOGV("%s(%ld)", __func__, contrast);

    if (contrast < 0 || contrast > 255) {
        ALOGW("%s: contrast %ld out of range [0-255]", __func__, contrast);
    }

    atomic_set(&dev->contrast, contrast);

    mutex_lock(&dev->info->lock);

#if 0
    urb = eRGlassfb_get_urb(dev);
    if (!urb) {
        mutex_unlock(&dev->info->lock);
        ALOGW("%s: get urb failed [dev=%p]", __func__, dev);
        return 0;
    }

    buffer[0] = REG_CONTRAST; buffer[1] = contrast;
    lendata = 2;
    len = eRGlassfb_prepareframe(eRCMD_REGISTER, buffer, lendata);
    bufptr = (char *)urb->transfer_buffer;
    memcpy(bufptr, &m_frame, len);
    eRGlassfb_submit_urb(dev, urb, len);
#else
    cmd.command = eRCMD_REGISTER;
    cmd.data [0]= REG_CONTRAST;
    cmd.data [1]= contrast;
    cmd.len = 2;
    command_put(cmd);
#endif

    mutex_unlock(&dev->info->lock);

    return count;
}


static ssize_t brightness_show(struct device *fbdev, struct device_attribute *a, char *buf)
{
    struct fb_info *fb_info = dev_get_drvdata(fbdev);
    struct eRGlassfb_data *dev = fb_info->par;

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%u\n", atomic_read(&dev->brightness));
}


static ssize_t brightness_store(struct device *fbdev, struct device_attribute *a, const char *buf, size_t count)
{
    struct fb_info *fb_info = dev_get_drvdata(fbdev);
    struct eRGlassfb_data *dev = fb_info->par;
    long brightness;
#if 0
    struct urb *urb;
    char *bufptr;
    char buffer[2];
    int lendata, len;
#else
    COMMAND cmd;
#endif

    if (kstrtol(buf, 10, &brightness)) {
        ALOGW("%s(%s): kstrtol error", __func__, buf);
        return 0;
    }

    ALOGV("%s(%ld)", __func__, brightness);

    if (brightness < 0 || brightness > 255) {
        ALOGW("%s: contrast %ld out of range [0-255]\n", __func__, brightness);
    }

    atomic_set(&dev->brightness, brightness);

    mutex_lock(&dev->info->lock);

#if 0
    urb = eRGlassfb_get_urb(dev);
    if (!urb) {
        mutex_unlock(&dev->info->lock);
        ALOGW("%s: get urb failed [dev=%p]", __func__, dev);
        return 0;
    }

    buffer[0] = REG_BRIGHTNESS; buffer[1] = brightness;
    lendata = 2;
    len = eRGlassfb_prepareframe(eRCMD_REGISTER, (const char *)buffer, lendata);
    bufptr = (char *)urb->transfer_buffer;
    memcpy(bufptr, &m_frame, len);
    eRGlassfb_submit_urb(dev, urb, len);
#else
    cmd.command = eRCMD_REGISTER;
    cmd.data [0]= REG_BRIGHTNESS;
    cmd.data [1]= brightness;
    cmd.len = 2;
    command_put(cmd);
#endif

    mutex_unlock(&dev->info->lock);

    return count;
}


static ssize_t edid_show(struct file *filp,
                         struct kobject *kobj, struct bin_attribute *a,
                         char *buf, loff_t off, size_t count)
{
    struct device *fbdev = container_of(kobj, struct device, kobj);
    struct fb_info *fb_info = dev_get_drvdata(fbdev);
    struct eRGlassfb_data *dev = fb_info->par;

    ALOGV("%s", __func__);

    if (dev->edid == NULL)
        return 0;

    if ((off >= dev->edid_size) || (count > dev->edid_size))
        return 0;

    if (off + count > dev->edid_size)
        count = dev->edid_size - off;

    ALOGV("%s: sysfs edid copy %p to %p, %d bytes", __func__, dev->edid, buf, (int)count);

    memcpy(buf, dev->edid, count);

    return count;
}


static ssize_t edid_store(struct file *filp,
                          struct kobject *kobj, struct bin_attribute *a,
                          char *src, loff_t src_off, size_t src_size)
{
    struct device *fbdev = container_of(kobj, struct device, kobj);
    struct fb_info *fb_info = dev_get_drvdata(fbdev);
    struct eRGlassfb_data *dev = fb_info->par;
    int ret;

    ALOGV("%s", __func__);

    /* We only support write of entire EDID at once, no offset*/
    if ((src_size != EDID_LENGTH) || (src_off != 0))
        return -EINVAL;

    ret = eRGlassfb_setup_modes(dev, fb_info, src, src_size);
    if (ret)
        return ret;

    if (!dev->edid || memcmp(src, dev->edid, src_size))
        return -EINVAL;

    ALOGV("%s: sysfs written EDID is new default", __func__);

    eRGlassfb_ops_set_par(fb_info);

    return src_size;
}


static struct bin_attribute edid_attr = {
    .attr.name = "edid",
    .attr.mode = 0666,
    .size = EDID_LENGTH,
    .read = edid_show,
    .write = edid_store
};

static struct device_attribute fb_device_attrs[] = {
    __ATTR(contrast, S_IRUGO | S_IWUGO, contrast_show, contrast_store),
    __ATTR(brightness, S_IRUGO | S_IWUGO, brightness_show, brightness_store),
};


/*
 * This is necessary before we can communicate with the display controller.
 */
static int eRGlassfb_select_std_channel(struct eRGlassfb_data *dev)
{
    int ret;
    char *buffer;
    u8 set_def_chn[] = {
                0x57, 0xCD, 0xDC, 0xA7,
                0x1C, 0x88, 0x5E, 0x15,
                0x60, 0xFE, 0xC6, 0x97,
                0x16, 0x3D, 0x47, 0xF2  };

    ALOGV("%s", __func__);

    buffer = kmalloc(sizeof(set_def_chn), GFP_KERNEL); /* required by kernel >= 4.9 */
    buffer = memcpy(buffer, set_def_chn, sizeof(set_def_chn));
    ret = usb_control_msg(dev->udev, usb_sndctrlpipe(dev->udev, 0),
            NR_USB_REQUEST_CHANNEL,
            (USB_DIR_OUT | USB_TYPE_VENDOR), 0, 0,
            buffer, sizeof(set_def_chn), USB_CTRL_SET_TIMEOUT);
    kfree(buffer);

    return ret;
}


static int eRGlassfb_parse_vendor_descriptor(struct eRGlassfb_data *dev, struct usb_interface *interface)
{
    char *desc;
    char *buf;
    char *desc_end;

    int total_len = 0;

    ALOGV("%s", __func__);

    buf = kzalloc(MAX_VENDOR_DESCRIPTOR_SIZE, GFP_KERNEL);
    if (!buf)
        return false;
    desc = buf;

    total_len = usb_get_descriptor(interface_to_usbdev(interface),
                    0x5f, /* vendor specific */
                    0, desc, MAX_VENDOR_DESCRIPTOR_SIZE);

    /* if not found, look in configuration descriptor */
    if (total_len < 0) {
        if (0 == usb_get_extra_descriptor(interface->cur_altsetting,
            0x5f, &desc))
            total_len = (int) desc[0];
    }

    if (total_len > 5) {
        ALOGV("%s: vendor descriptor length:%x "
                "data:%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x",
            __func__,
            total_len,
            desc[0], desc[1], desc[2], desc[3],
            desc[4], desc[5], desc[6], desc[7],
            desc[8], desc[9], desc[10]);

        if ((desc[0] != total_len) || /* descriptor length */
            (desc[1] != 0x5f) ||   /* vendor descriptor type */
            (desc[2] != 0x01) ||   /* version (2 bytes) */
            (desc[3] != 0x00) ||
            (desc[4] != total_len - 2)) /* length after type */
            goto unrecognized;

        desc_end = desc + total_len;
        desc += 5; /* the fixed header we've already parsed */

        while (desc < desc_end) {
            u8 length;
            u16 key;

            key = le16_to_cpu(*((u16 *) desc));
            desc += sizeof(u16);
            length = *desc;
            desc++;

            switch (key) {
                case 0x0200: { /* max_area */
                    u32 max_area = le32_to_cpu(*((u32 *)desc));
                    ALOGW("%s: DL chip limited to %d pixel modes", __func__, max_area);
                    dev->sku_pixel_limit = max_area;
                    break;
                }
                default:
                    break;
            }
            desc += length;
        }
    } else {
        ALOGI("%s: vendor descriptor not available (%d)", __func__, total_len);
    }

    goto success;

unrecognized:
    /* allow udlfb to load for now even if firmware unrecognized */
    ALOGE("%s: Unrecognized vendor firmware descriptor", __func__);

success:
    kfree(buf);
    return true;
}


/**
 *  eRGlass_usb_send
 */
static size_t fBGlass_usb_send(struct eRGlassfb_data *dev, unsigned char *pBuf, size_t len)
{
    int ret;

    ALOGV("%s", __func__);

    memcpy(m_FrameBufferUrb->transfer_buffer, pBuf, len);
    m_FrameBufferUrb->transfer_buffer_length = len; /* set to actual payload len */
    ret = usb_submit_urb(m_FrameBufferUrb, GFP_KERNEL);

    if (ret) {
        usb_kill_urb(m_FrameBufferUrb);
////        eRGlassfb_urb_completion(m_FrameBufferUrb); /* because no one else will */
        ALOGE("fBGlass_usb_send: usb_submit_urb error %x\n", ret);
    }

    return ret;
}


int SendFrameBuffer(struct eRGlassfb_data *dev)
{
    int len;
    int ret =-1;

    ALOGV("%s", __func__);

    len = eRGlassfb_prepareframe(eRCMD_BITMAP, (char *)dev->info->fix.smem_start, WIDTH*HEIGHT);
    ret = fBGlass_usb_send(dev, (unsigned char *)&m_frame, len);
            
    if(ret > 0){
        ret = 0;
    }

    return ret;
}


static int SendCommandFb(struct eRGlassfb_data *dev, COMMAND *commandGlassFb)
{
    int len;
    int ret =-1;
    
    ALOGV("%s", __func__);

    len = eRGlassfb_prepareframe(commandGlassFb->command, commandGlassFb->data, commandGlassFb->len);
    ret = fBGlass_usb_send(dev, (unsigned char *)&m_frame, len);
            
    if(ret > 0){
        ret = 0;
    }

    return ret;
}


#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
static void fn_timer_lcdrefresh(unsigned long handle)
{
    struct eRGlassfb_data *dev = (void *)handle;

    ALOGV("%s", __func__);

    if(m_Connect == 0){
        return;
    }

    mod_timer(&dev->timer_lcdrefresh, jiffies + msecs_to_jiffies(m_LcdRefresh));
    queue_work(dev->workqueue, &dev->work_lcdrefresh);
}
#else
static void fn_timer_lcdrefresh(struct timer_list *t)
{
    struct eRGlassfb_data *dev = from_timer(dev, t, timer_lcdrefresh);

    ALOGV("%s", __func__);

    if(m_Connect == 0){
        return;
    }

    mod_timer(&dev->timer_lcdrefresh, jiffies + msecs_to_jiffies(m_LcdRefresh));
    queue_work(dev->workqueue, &dev->work_lcdrefresh);
}
#endif


#if 0
static void fn_work_lcdrefresh(struct work_struct *work)
{
    struct eRGlassfb_data *dev = container_of(work, struct eRGlassfb_data, work_lcdrefresh);

    ALOGV("%s", __func__);

    if(m_Connect == 0){
        return;
    }

    mutex_lock(&dev->mutex);
    aLogLevel = verbose;    /* Log level updating */

    if(m_Sent != NULL){
        ALOGI("%s: killing submitted urb!", __func__);
        usb_kill_urb(m_Sent);   /* to release blocked resources */
    }

    erglassfb_handle_lcdrefresh(dev->info->par, (char *)dev->info->fix.smem_start);
    mutex_unlock(&dev->mutex);
}
#endif


static void fn_CommandWork(struct work_struct *work)  
{
    struct eRGlassfb_data *dev = container_of(work, struct eRGlassfb_data, work_lcdrefresh);  
    COMMAND commandGlassFb;

    ALOGV("%s", __func__);

    if(m_Connect == 0){
        return;
    }

    aLogLevel = verbose;    /* Log level updating */

    mutex_lock(&dev->mutex);

    if (m_Sent != NULL)
    {
        if(CounterWaitCompletation++ > ((500 / m_LcdRefresh))+1){
            ALOGV("\nFIORE fn_work_lcdrefresh CounterWaitCompletation=%d\n", CounterWaitCompletation);
            usb_kill_urb(m_FrameBufferUrb);
////            eRGlassfb_urb_completion(m_FrameBufferUrb);
            CounterWaitCompletation = 0;
        }
    }
    else {
        p_lock(__func__);           /* unlock is called in eRGlassfb_urb_completion() */
        m_Sent = m_FrameBufferUrb;  /* m_Sent will reset in eRGlassfb_urb_completion() */
        CounterWaitCompletation = 0;
 
        if(command_is()){
            command_get(&commandGlassFb);
            SendCommandFb(dev, &commandGlassFb);
        } else {
            SendFrameBuffer(dev);
        }
    }

    mutex_unlock(&dev->mutex);
}

static int eRGlassfb_usb_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
    struct usb_device *usbdev;
    struct eRGlassfb_data *dev = 0;
    int retval = -ENOMEM;
    struct urb_node *unode;
    char *buf;

    ALOGV("%s", __func__);

    /* usb initialization */
    usbdev = interface_to_usbdev(interface);

    dev = kzalloc(sizeof(*dev), GFP_KERNEL);
    if (dev == NULL) {
        ALOGE("%s: eRGlassfb_usb_probe: failed alloc of dev struct!", __func__);
        goto error;
    }

    kref_init(&dev->kref); /* matching kref_put in usb .disconnect fn */

    mutex_init(&dev->mutex);

    dev->udev = usbdev;
    dev->gdev = &usbdev->dev; /* our generic struct device * */
    usb_set_intfdata(interface, dev);

    ALOGV("%s:\n"
            "\t%s %s - serial #%s\n"
            "\tvid_%04X&pid_%04x&rev_%04X driver's eRGlassfb_data struct at %p\n"
            "\tverbose enable = %d",
            __func__,
            usbdev->manufacturer, usbdev->product, usbdev->serial,
            usbdev->descriptor.idVendor, usbdev->descriptor.idProduct, usbdev->descriptor.bcdDevice, dev,
            verbose);
    dev->sku_pixel_limit = WIDTH * HEIGHT;

    if (!eRGlassfb_parse_vendor_descriptor(dev, interface)) {
        ALOGE("%s: firmware not recognized. Assume incompatible device", __func__);
        goto error;
    }

#if 0
    if (!eRGlassfb_alloc_urb_list(dev, WRITES_IN_FLIGHT, MAX_TRANSFER)) {
        ALOGE("%s: eRGlassfb_alloc_urb_list failed", __func__);
        retval = -ENOMEM;
        goto error;
    }
#endif

    /* getting device lock functions */
    if ((p_lock = symbol_get(visionar_lock)) == NULL) {
        ALOGE("%s: visionar_lock function not found", __func__);
    }
    if ((p_unlock = symbol_get(visionar_unlock)) == NULL) {
        ALOGE("%s: visionar_unlock function not found", __func__);
    }

    kref_get(&dev->kref); /* matching kref_put in free_framebuffer_work */

    unode = kzalloc(sizeof(struct urb_node), GFP_KERNEL);
    if (!unode){
        return -ENOMEM;
    }
    
    unode->dev = dev;

    m_FrameBufferUrb = usb_alloc_urb(0, GFP_KERNEL);
    buf = usb_alloc_coherent(dev->udev, MAX_TRANSFER, GFP_KERNEL, &m_FrameBufferUrb->transfer_dma);
    if (!buf) {
        usb_free_urb(m_FrameBufferUrb);
        return -ENOMEM;
    }
    /* urb->transfer_buffer_length set to actual before submit */
    usb_fill_bulk_urb(m_FrameBufferUrb, dev->udev, usb_sndbulkpipe(dev->udev, 1), buf, MAX_TRANSFER, eRGlassfb_urb_completion, unode);

    command_init();

    /* We don't register a new USB class. Our client interface is fbdev */

    /* Workitem keep things fast & simple during USB enumeration */
    INIT_DELAYED_WORK(&dev->init_framebuffer_work, eRGlassfb_init_framebuffer_work);
    schedule_delayed_work(&dev->init_framebuffer_work, 0);

    m_Connect = 1;

    return 0;

error:
    if (dev) {
        kref_put(&dev->kref, eRGlassfb_free); /* ref for framebuffer */
        kref_put(&dev->kref, eRGlassfb_free); /* last ref from kref_init */

        /* dev has been deallocated. Do not dereference */
    }

    return retval;
}


void eRGlassfb_init_framebuffer_work(struct work_struct *work)
{
    struct eRGlassfb_data *dev = container_of(work, struct eRGlassfb_data, init_framebuffer_work.work);
    int retval;
    int i;
    static struct fb_info *info;

    ALOGV("%s", __func__);

    /* allocates framebuffer driver structure, not framebuffer memory */
    info = framebuffer_alloc(0, dev->gdev);
    if (!info) {
        retval = -ENOMEM;
        ALOGE("%s: framebuffer_alloc failed", __func__);
        goto error;
    }

    info->node = 0;
    dev->info = info;
    m_pInfo = info;
    info->par = dev;
    info->pseudo_palette = dev->pseudo_palette;
    info->fbops = &visionarfb_ops;

    retval = fb_alloc_cmap(&info->cmap, 256, 0);
    if (retval < 0) {
        ALOGE("%s: fb_alloc_cmap failed %x", __func__, retval);
        goto error;
    }

    INIT_LIST_HEAD(&info->modelist);

    retval = eRGlassfb_setup_modes(dev, info, NULL, 0);
    if (retval != 0) {
        ALOGE("%s: unable to find common mode for display and adapter", __func__);
        goto error;
    }

    /* ready to begin using device */
    atomic_set(&dev->usb_active, 1);
    eRGlassfb_select_std_channel(dev);

    eRGlassfb_ops_check_var(&info->var, info);
    eRGlassfb_ops_set_par(info);

    retval = register_framebuffer(info);
    if (retval < 0) {
        ALOGE("%s: register_framebuffer failed %d", __func__, retval);
        goto error;
    }

    for (i = 0; i < ARRAY_SIZE(fb_device_attrs); i++) {
        retval = device_create_file(info->dev, &fb_device_attrs[i]);
        if (retval) {
            ALOGW("%s: device_create_file failed %d", __func__, retval);
        }
    }

    retval = device_create_bin_file(info->dev, &edid_attr);
    if (retval) {
        ALOGW("%s: device_create_bin_file failed %d", __func__, retval);
    }

    memset((char *)dev->info->fix.smem_start, 0x00, (WIDTH * HEIGHT * info->var.bits_per_pixel / 8) );
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
    setup_timer(&dev->timer_lcdrefresh, fn_timer_lcdrefresh, (unsigned long)dev);
#else
    timer_setup(&dev->timer_lcdrefresh, fn_timer_lcdrefresh, 0);
    /* the third argument may include TIMER_* flags */
    /* ... */
#endif
    mod_timer(&dev->timer_lcdrefresh, jiffies + msecs_to_jiffies(m_LcdRefresh));
    INIT_WORK(&dev->work_lcdrefresh, fn_CommandWork);  
    dev->workqueue = create_singlethread_workqueue("eRGlassfb");
    if (dev->workqueue == NULL) {
        ALOGE("%s: eRGlassfb workqueue fail", __func__);
        goto error;
    }

    ALOGI("%s: VisionAR USB device /dev/fb%d attached. %dx%d resolution. Using %dK framebuffer memory\n",
            __func__,
            info->node, info->var.xres, info->var.yres,
            ((dev->backing_buffer) ? info->fix.smem_len * 2 : info->fix.smem_len) >> 10);

    return;

error:
    eRGlassfb_free_framebuffer(dev);
}


static void eRGlassfb_usb_disconnect(struct usb_interface *interface)
{
    struct eRGlassfb_data *dev;
    struct fb_info *info;
    int i;

    ALOGV("%s", __func__);

    m_Connect = 0;
    dev = usb_get_intfdata(interface);
    eRGlassfb_free_framebuffer_work(&dev->init_framebuffer_work.work);
    msleep(250+100);

    dev = usb_get_intfdata(interface);
    info = dev->info;

    if(m_Sent != NULL){
        ALOGI("%s: killing submitted urb!", __func__);
        usb_kill_urb(m_Sent);   /* to release blocked resources */
    }

    del_timer_sync(&dev->timer_lcdrefresh);

    /* we virtualize until all fb clients release. Then we free */
    dev->virtualized = true;

    /* When non-active we'll update virtual framebuffer, but no new urbs */
    atomic_set(&dev->usb_active, 0);

    if (dev->workqueue == NULL) {
        flush_workqueue(dev->workqueue);
        destroy_workqueue(dev->workqueue);
    }

    /* release device lock functions */
    if (p_lock != NULL) {
        symbol_put(visionar_lock);
        p_lock = NULL;
    }
    if (p_unlock != NULL) {
        symbol_put(visionar_unlock);
        p_unlock = NULL;
    }

#if 0
    /* this function will wait for all in-flight urbs to complete */
    eRGlassfb_free_urb_list(dev);
#endif

    if (info) {
        //remove udlfb's sysfs interfaces
        for (i = 0; i < ARRAY_SIZE(fb_device_attrs); i++)
            device_remove_file(info->dev, &fb_device_attrs[i]);
        device_remove_bin_file(info->dev, &edid_attr);
////        unregister_framebuffer(info);
////        unlink_framebuffer(info);
    }

    usb_set_intfdata(interface, NULL);
    dev->udev = NULL;
    dev->gdev = NULL;

    /* release reference taken by kref_init in probe() */
    kref_put(&dev->kref, eRGlassfb_free);

    /* consider eRGlassfb_data freed */

    return;
}


static struct usb_driver eRGlassfb_driver = {
    .name = "VisionAR FB",
    .probe = eRGlassfb_usb_probe,
    .disconnect = eRGlassfb_usb_disconnect,
#if defined(PREMOLI)
    int (*unlocked_ioctl) (struct usb_interface *intf, unsigned int code, void *buf);
    int (*suspend) (struct usb_interface *intf, pm_message_t message);
    int (*resume) (struct usb_interface *intf);
    int (*reset_resume)(struct usb_interface *intf);
    int (*pre_reset)(struct usb_interface *intf);
    int (*post_reset)(struct usb_interface *intf);
#endif
    .id_table = id_table,
#if defined(PREMOLI)
    const struct attribute_group **dev_groups;

    struct usb_dynids dynids;
    struct usbdrv_wrap drvwrap;
    unsigned int no_dynamic_id:1;
    unsigned int supports_autosuspend:1;
    unsigned int disable_hub_initiated_lpm:1;
    unsigned int soft_unbind:1;
#endif
};

module_usb_driver(eRGlassfb_driver);


/*****************************************************************************
 * URB Management
 *****************************************************************************/

static void eRGlassfb_urb_completion(struct urb *urb)
{
    struct urb_node *unode = urb->context;
    struct eRGlassfb_data *dev = unode->dev;
#if 0
    unsigned long flags;
#endif

    ALOGV("%s(%p)", __func__, urb);

    if ((urb == m_Sent) || (m_Sent == NULL))
    {
        m_Sent = NULL;

        if (p_unlock != NULL)
        {
            p_unlock(__func__);
        }
    }

    /* sync/async unlink faults aren't errors */
    if (urb->status) {
        if (!(urb->status == -ENOENT || urb->status == -ECONNRESET || urb->status == -EINPROGRESS || urb->status == -ESHUTDOWN)) {
            ALOGE("%s - nonzero write bulk status received: %d", __func__, urb->status);
            atomic_set(&dev->lost_pixels, 1);
        }
    }

#if 0
    urb->transfer_buffer_length = dev->urbs.size; /* reset to actual */

    spin_lock_irqsave(&dev->urbs.lock, flags);
    list_add_tail(&unode->entry, &dev->urbs.list);
    dev->urbs.available++;
    spin_unlock_irqrestore(&dev->urbs.lock, flags);

    up(&dev->urbs.limit_sem);
#endif
}

#if 0
void eRGlassfb_free_urb_list(struct eRGlassfb_data *dev)
{
    int count = dev->urbs.count;
    struct list_head *node;
    struct urb_node *unode;
    struct urb *urb;
    int ret;
    unsigned long flags;

    ALOGV("%s", __func__);

    /* keep waiting and freeing, until we've got 'em all */
    while (count--) {
        /* Getting interrupted means a leak, but ok at disconnect */
        ret = down_interruptible(&dev->urbs.limit_sem);
        if (ret)
            break;

        spin_lock_irqsave(&dev->urbs.lock, flags);

        node = dev->urbs.list.next; /* have reserved one with sem */
        list_del_init(node);

        spin_unlock_irqrestore(&dev->urbs.lock, flags);

        unode = list_entry(node, struct urb_node, entry);
        urb = unode->urb;

        /* Free each separately allocated piece */
        usb_free_coherent(urb->dev, dev->urbs.size, urb->transfer_buffer, urb->transfer_dma);
        usb_free_urb(urb);
        kfree(node);
    }

    dev->urbs.count = 0;
}


int eRGlassfb_alloc_urb_list(struct eRGlassfb_data *dev, int count, size_t size)
{
    int i = 0;
    struct urb *urb;
    struct urb_node *unode;
    char *buf;

    ALOGV("%s", __func__);

    spin_lock_init(&dev->urbs.lock);

    dev->urbs.size = size;
    INIT_LIST_HEAD(&dev->urbs.list);

    while (i < count) {
        unode = kzalloc(sizeof(struct urb_node), GFP_KERNEL);
        if (!unode)
            break;
        unode->dev = dev;

        INIT_DELAYED_WORK(&unode->release_urb_work, eRGlassfb_release_urb_work);

        urb = usb_alloc_urb(0, GFP_KERNEL);
        if (!urb) {
            kfree(unode);
            break;
        }
        unode->urb = urb;

        buf = usb_alloc_coherent(dev->udev, MAX_TRANSFER, GFP_KERNEL, &urb->transfer_dma);
        if (!buf) {
            kfree(unode);
            usb_free_urb(urb);
            break;
        }

        /* urb->transfer_buffer_length set to actual before submit */
        usb_fill_bulk_urb(urb, dev->udev, usb_sndbulkpipe(dev->udev, 1), buf, size, eRGlassfb_urb_completion, unode);
        urb->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;

        list_add_tail(&unode->entry, &dev->urbs.list);

        i++;
    }

    sema_init(&dev->urbs.limit_sem, i);
    dev->urbs.count = i;
    dev->urbs.available = i;

    ALOGI("%s: allocated %d %d byte urbs", __func__, i, (int) size);

    return i;
}

struct urb *eRGlassfb_get_urb(struct eRGlassfb_data *dev)
{
    //int ret = 0;
    struct list_head *entry;
    struct urb_node *unode;
    struct urb *urb = NULL;
    unsigned long flags;

    ALOGV("%s", __func__);

    /* Wait for an in-flight buffer to complete and get re-queued */
    /*ret = down_timeout(&dev->urbs.limit_sem, GET_URB_TIMEOUT);
    if (ret) {
        atomic_set(&dev->lost_pixels, 1);
        pr_warn("wait for urb interrupted: %x available: %d\n",
               ret, dev->urbs.available);
        goto error;
    }*/

    spin_lock_irqsave(&dev->urbs.lock, flags);

    // BUG_ON(list_empty(&dev->urbs.list)); /* reserved one with limit_sem */
    entry = dev->urbs.list.next;
    list_del_init(entry);
    dev->urbs.available--;

    unode = list_entry(entry, struct urb_node, entry);
    urb = unode->urb;
    spin_unlock_irqrestore(&dev->urbs.lock, flags);

    return urb;
}

int eRGlassfb_submit_urb(struct eRGlassfb_data *dev, struct urb *urb, size_t len)
{
    int ret;

    ALOGV("%s(%p, %lu)", __func__, urb, len);

    if(m_Sent != NULL){
        ALOGE("%s: previous urb is not completed!", __func__);
        eRGlassfb_urb_completion(urb); /* because no one else will */
        return -EINVAL;
    }

    BUG_ON(len > dev->urbs.size);

    urb->transfer_buffer_length = len; /* set to actual payload len */
    if ((p_lock == NULL) || (p_lock(__func__) == 0)) {
        if ((ret = usb_submit_urb(urb, GFP_KERNEL)) != 0) {
            eRGlassfb_urb_completion(urb); /* because no one else will */

            atomic_set(&dev->lost_pixels, 1);
            ALOGV("%s: usb_submit_urb error %x", __func__, ret);
        }
        else {
            m_Sent = urb;
        }
    }
    else {
        ALOGE("%s: error on device locking", __func__);
        ret = -ENOTBLK;
    }

    return ret;
}
#endif

/*****************************************************************************/


/*****************************************************************************
 *  Modularization
 *****************************************************************************/

MODULE_VERSION("V4.0");
MODULE_AUTHOR("ETL Team <support@eyetechlab.com>");
MODULE_DESCRIPTION("VisionAR framebuffer driver");
MODULE_LICENSE("GPL");

/*****************************************************************************/
