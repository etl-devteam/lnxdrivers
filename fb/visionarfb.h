#ifndef EGLASSFB_H
#define EGLASSFB_H

#define ERGLASSFB_IOCTL_LCD_REFRESH     0xAA
#define ERGLASSFB_IOCTL_RETURN_EDID     0xAD

struct urb_node {
    struct list_head entry;
    struct eRGlassfb_data *dev;
    struct delayed_work release_urb_work;
    struct urb *urb;
};

struct urb_list {
    struct list_head list;
    spinlock_t lock;
    struct semaphore limit_sem;
    int available;
    int count;
    size_t size;
};

struct eRGlassfb_data {
    struct mutex mutex;      /* locks this structure */
    struct usb_device *udev;
    struct device *gdev; /* &udev->dev */
    struct fb_info *info;
    struct urb_list urbs;
    struct kref kref;
    char *backing_buffer;
    int fb_count;
    bool virtualized; /* true when physical usb device not present */
    struct delayed_work init_framebuffer_work;
    struct delayed_work free_framebuffer_work;
    atomic_t usb_active; /* 0 = update virtual buffer, but no usb traffic */
    atomic_t lost_pixels; /* 1 = a render op failed. Need screen refresh */
    char *edid; /* null until we read edid from hw or get from sysfs */
    size_t edid_size;
    int sku_pixel_limit;
    u32 pseudo_palette[256];
    
    struct timer_list timer_lcdrefresh;
    struct work_struct work_lcdrefresh;
    struct workqueue_struct *workqueue;
    
    atomic_t contrast;
    atomic_t brightness;
};

#define NR_USB_REQUEST_I2C_SUB_IO 0x02
#define NR_USB_REQUEST_CHANNEL 0x12

/* -BULK_SIZE as per usb-skeleton. Can we get full page and avoid overhead? */
#define BULK_SIZE 512
#define MAX_TRANSFER (PAGE_SIZE * 16 - BULK_SIZE)
#define WRITES_IN_FLIGHT (1)

#define MAX_VENDOR_DESCRIPTOR_SIZE 256

#define GET_URB_TIMEOUT     (HZ)
#define FREE_URB_TIMEOUT    (HZ*2)

#endif
