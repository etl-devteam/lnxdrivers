/*
 * visionartest.cpp
 *
 *  Created on: Aug 6, 2021
 *      Author: andre
 */

#include <cstdio>
#include <cstring>
#include <unistd.h>
#include "visionarusb.h"
#include <libusb-1.0/libusb.h>
#include <iostream>
#include <fstream>
#include <linux/input.h>

#define WIDTH   419
#define HEIGHT  138

using namespace std;

static const char *resources[] = {
	"../resources/ETL.bmp",
	"../resources/visionar.bmp",
	"../resources/test.bmp"
};

static const char *ev_code_key[] = {
    "BTN_LEFT",
    "BTN_RIGHT",
    "BTN_MIDDLE",
    "BTN_SIDE",
    "BTN_EXTRA",
    "BTN_FORWARD",
    "BTN_BACK",
    "BTN_TASK",
    "SWIPE DOWN",
    "SWIPE UP",
};


static bool connected = false;

static unsigned char pixels[4*WIDTH*HEIGHT];

static void testCb (VisionAREvent ev)
{
    printf("%s(%s)\n", __func__, ((ev == VISIONAR_CONNECTION_EVENT)?"CONNECTED":"DISCONNECTED"));

    if (ev == VISIONAR_CONNECTION_EVENT)
    {
        connected = true;
    }
}

static void testTouchpad (unsigned char val)
{
    printf("%s(%s)\n", __func__, ev_code_key[val-KEY_F1]);
}

static void BMP2Pixels(const char *filename, unsigned char *pixels)
{
    char header[14];
    unsigned int address = 0;
    char *buffer = new char [4*WIDTH];

    ifstream bmp (filename, ifstream::binary);

    bmp.read(&header[0], sizeof(header));
    for (int i = 4; i>0; i--) {
        address <<= 8;
        address |= (header[10+i-1] & 0xFF);
    }
    bmp.seekg (address, bmp.beg);

    for (int row = 0; row < HEIGHT; row++)
    {
        bmp.read(buffer, 4*WIDTH);
        memcpy(&pixels[4*(HEIGHT-1-row)*WIDTH], buffer, 4*WIDTH);
    } /* for (row) */

    bmp.close();

    delete buffer;
}

static void fillPixels(int type, unsigned char *pel)
{
    int row, col;

    if (type == 1)
    {
        printf("%s: UP\n", __func__);

        for (row = 0; row < HEIGHT/2; row++) {
            for (col = 0; col < WIDTH; col++) {
                pel[4*(row*WIDTH+col)+0] = 0xFF;
                pel[4*(row*WIDTH+col)+1] = 0xFF;
                pel[4*(row*WIDTH+col)+2] = 0xFF;
                pel[4*(row*WIDTH+col)+3] = 0xFF;
            }
        }
        for (row = HEIGHT/2; row < HEIGHT; row++) {
            for (col = 0; col < WIDTH; col++) {
                pel[4*(row*WIDTH+col)+0] = 0x00;
                pel[4*(row*WIDTH+col)+1] = 0x00;
                pel[4*(row*WIDTH+col)+2] = 0x00;
                pel[4*(row*WIDTH+col)+3] = 0x00;
            }
        }
    }
    else
    {
        printf("%s: DOWN\n", __func__);

        for (row = 0; row < HEIGHT/2; row++) {
            for (col = 0; col < WIDTH; col++) {
                pel[4*(row*WIDTH+col)+0] = 0x00;
                pel[4*(row*WIDTH+col)+1] = 0x00;
                pel[4*(row*WIDTH+col)+2] = 0x00;
                pel[4*(row*WIDTH+col)+3] = 0x00;
            }
        }
        for (row = HEIGHT/2; row < HEIGHT; row++) {
            for (col = 0; col < WIDTH; col++) {
                pel[4*(row*WIDTH+col)+0] = 0xFF;
                pel[4*(row*WIDTH+col)+1] = 0xFF;
                pel[4*(row*WIDTH+col)+2] = 0xFF;
                pel[4*(row*WIDTH+col)+3] = 0xFF;
            }
        }
    }

}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    int counter = 20;
    int resIndex = 0;
    int contrast = 0;
    int brightness = 0;
    float data[3];
    unsigned char ver[6];
    unsigned int status;
    unsigned char num;
    unsigned short codes[32];

    Startup(testCb);
    StartTouchpad(testTouchpad, 250);

    do {
        while (counter-- > 0)
        {
            if (connected) break;

            sleep(5);
        }

        if (Haptic(200))
        {
            printf("Haptic: OK\n");
        }
        else
        {
            printf("Haptic: FAILED\n");
        }

        if (GetApplVers(&ver[0]))
        {
            printf("GetApplVers: %u.%u.%u (%02u/%02u/%04u)\n",
                    ver[0], ver[1], ver[2], ver[3], ver[4], (2000+ver[5]));
        }
        else
        {
            printf("GetApplVers: FAILED\n");
        }

        BMP2Pixels(resources[0], &pixels[0]);
        if (WriteImg(pixels))
        {
            printf("WriteImg(%s): OK\n", resources[0]);
        }
        else
        {
            printf("WriteImg(%s): FAILED\n", resources[0]);
        }
        sleep(2);

        BMP2Pixels(resources[1], &pixels[0]);
        if (WriteImg(pixels))
        {
            printf("WriteImg(%s): OK\n", resources[1]);
        }
        else
        {
            printf("WriteImg(%s): FAILED\n", resources[1]);
        }
        sleep(2);

        BMP2Pixels(resources[2], &pixels[0]);
        if (WriteImg(pixels))
        {
            printf("WriteImg(%s): OK\n", resources[2]);
        }
        else
        {
            printf("WriteImg(%s): FAILED\n", resources[2]);
        }

        if (GetImuAcc(&data[0]))
        {
            printf("GetImuAcc: %.4f %.4f %.4f\n", data[0], data[1], data[2]);
        }
        else
        {
            printf("GetImuAcc: FAILED\n");
        }

        if (GetImuGyro(&data[0]))
        {
            printf("GetImuGyro: %.4f %.4f %.4f\n", data[0], data[1], data[2]);
        }
        else
        {
            printf("GetImuGyro: FAILED\n");
        }

        if (GetImuMag(&data[0]))
        {
            printf("GetImuMag: %.4f %.4f %.4f\n", data[0], data[1], data[2]);
        }
        else
        {
            printf("GetImuMag: FAILED\n");
        }

        if (GetStatus(&status))
        {
            printf("GetStatus: %08Xh\n", status);
        }
        else
        {
            printf("GetStatus: FAILED\n");
        }

        if (GetErrors(&num, &codes[0]))
        {
            printf("GetErrors: %u\n", num);
        }
        else
        {
            printf("GetErrors: FAILED\n");
        }

        fillPixels(1, &pixels[0]);
        if (WriteImg(pixels))
        {
            printf("WriteImg UP: OK\n");
        }
        else
        {
            printf("WriteImg UP: FAILED\n");
        }
        sleep(2);

        fillPixels(0, &pixels[0]);
        if (WriteImg(pixels))
        {
            printf("WriteImg DOWN: OK\n");
        }
        else
        {
            printf("WriteImg DOWN: FAILED\n");
        }

        if (LedCamera(VISIONAR_CAMERA_RED_COLOR))
        {
            printf("LedCamera: RED\n");
        }
        else
        {
            printf("LedCamera: FAILED\n");
        }
        sleep(1);

        if (LedCamera(VISIONAR_CAMERA_GREEN_COLOR))
        {
            printf("LedCamera: GREEN\n");
        }
        else
        {
            printf("LedCamera: FAILED\n");
        }
        sleep(1);

        if (LedCamera(VISIONAR_CAMERA_OFF_COLOR))
        {
            printf("LedCamera: OFF\n");
        }
        else
        {
            printf("LedCamera: FAILED\n");
        }
        sleep(1);

        connected = false;
        counter = 5;

    } while (0);

    StopTouchpad();
    CloseDev();
}

