/*
 * erglassusbc -- USB driver for eRGlass device
 *
 * Copyright (C) 2018 Dinema
 *
 */

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/uaccess.h>
#include <linux/input.h>
#include <linux/usb.h>
#include <linux/poll.h>
#include <linux/input.h>

#include "alog.h"


/* warning! need write-all permission so overriding check */
#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)


#define USB_ERGLASS_MINOR_BASE              176

#define ERGLASSSER_VENDOR_ID                0x0483
#define ERGLASSSER_VPRODUCT_ID              0xA306

static struct usb_device_id eRGlass_usb_table[] = {
    {
        .idVendor = ERGLASSSER_VENDOR_ID,
        .idProduct = ERGLASSSER_VPRODUCT_ID,
        .bInterfaceClass = 0xFF,
        .bInterfaceSubClass = 0x01,
        .bInterfaceProtocol = 0x00,
        .match_flags = USB_DEVICE_ID_MATCH_VENDOR | USB_DEVICE_ID_MATCH_PRODUCT | USB_DEVICE_ID_MATCH_INT_CLASS | USB_DEVICE_ID_MATCH_INT_SUBCLASS | USB_DEVICE_ID_MATCH_INT_PROTOCOL,
    },
    {},
};


/*****************************************************************************
 * ALog definitions
 *****************************************************************************/

static const char *TAG = "VAR_USB";

static int aLogLevel = ALOG_INFO_LEVEL;

/*****************************************************************************/

/**
 *    eRGlass_usb_debug_data
 */
static inline void eRGlass_usb_debug_data (const char *function, int size, const unsigned char *data)
{
    int i;

    if (aLogLevel < ALOG_DEBUG_LEVEL)
        return;

    for (i = 0; i < size; i += 8) {
        if (i+7 < size) {
            ALOGD("%s: data[%02X-%02X]: %02X %02X %02X %02X %02X %02X %02X %02X",
                  function,
                  i, i+7,
                  data[i], data[i+1], data[i+2], data[i+3], data[i+4], data[i+5], data[i+6], data[i+7]);
        }
        else if (i+6 < size) {
            ALOGD("%s: data[%02X-%02X]: %02X %02X %02X %02X %02X %02X %02X",
                  function,
                  i, i+6,
                  data[i], data[i+1], data[i+2], data[i+3], data[i+4], data[i+5], data[i+6]);
        }
        else if (i+5 < size) {
            ALOGD("%s: data[%02X-%02X]: %02X %02X %02X %02X %02X %02X",
                  function,
                  i, i+5,
                  data[i], data[i+1], data[i+2], data[i+3], data[i+4], data[i+5]);
        }
        else if (i+4 < size) {
            ALOGD("%s: data[%02X-%02X]: %02X %02X %02X %02X %02X",
                  function,
                  i, i+4,
                  data[i], data[i+1], data[i+2], data[i+3], data[i+4]);
        }
        else if (i+3 < size) {
            ALOGD("%s: data[%02X-%02X]: %02X %02X %02X %02X",
                  function,
                  i, i+3,
                  data[i], data[i+1], data[i+2], data[i+3]);
        }
        else if (i+2 < size) {
            ALOGD("%s: data[%02X-%02X]: %02X %02X %02X",
                  function,
                  i, i+2,
                  data[i], data[i+1], data[i+2]);
        }
        else if (i+1 < size) {
            ALOGD("%s: data[%02X-%02X]: %02X %02X",
                  function,
                  i, i+1,
                  data[i], data[i+1]);
        }
        else {
            ALOGD("%s: data[%02X]   : %02X", function, i, data[i]);
        }
    }
}

/*****************************************************************************/


/*****************************************************************************
 * Device lock
 *****************************************************************************/

static bool visionar_initialized = false;
static struct mutex visionar_mutex; /* device mutex to synch access to FB or USB requests */

int visionar_lock(const char *function) {
    if (!visionar_initialized) {
        visionar_initialized = true;
        mutex_init(&visionar_mutex);
    }

    return mutex_lock_interruptible(&visionar_mutex);
}
EXPORT_SYMBOL_GPL(visionar_lock);

void visionar_unlock(const char *function) {
    if (visionar_initialized)
    {
        if (mutex_is_locked(&visionar_mutex)) mutex_unlock(&visionar_mutex);
    }
}
EXPORT_SYMBOL_GPL(visionar_unlock);

/*****************************************************************************/


/* Module parameters */
static int verbose      = ALOG_INFO_LEVEL;
module_param(verbose, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(verbose, "Verbose enabled or not");

/* The defaults are chosen to work with the latest versions of leJOS and NQC.
 */

/* Some legacy software likes to receive packets in one piece.
 * In this case read_buffer_size should exceed the maximal packet length
 * (417 for datalog uploads), and packet_timeout should be set.
 */
static int read_buffer_size = 480;
module_param(read_buffer_size, int, 0);
MODULE_PARM_DESC(read_buffer_size, "Read buffer size");

/* Some legacy software likes to send packets in one piece.
 * In this case write_buffer_size should exceed the maximal packet length
 * (417 for firmware and program downloads).
 * A problem with long writes is that the following read may time out
 * if the software is not prepared to wait long enough.
 */
static int write_buffer_size = 480;
module_param(write_buffer_size, int, 0);
MODULE_PARM_DESC(write_buffer_size, "Write buffer size");

/* Some legacy software expects reads to contain whole LASM packets.
 * To achieve this, characters which arrive before a packet timeout
 * occurs will be returned in a single read operation.
 * A problem with long reads is that the software may time out
 * if it is not prepared to wait long enough.
 * The packet timeout should be greater than the time between the
 * reception of subsequent characters, which should arrive about
 * every 5ms for the standard 2400 baud.
 * Set it to 0 to disable.
 */
static int packet_timeout = 50;
module_param(packet_timeout, int, 0);
MODULE_PARM_DESC(packet_timeout, "Packet timeout in ms");

/* Some legacy software expects blocking reads to time out.
 * Timeout occurs after the specified time of read and write inactivity.
 * Set it to 0 to disable.
 */
static int read_timeout = 200;
module_param(read_timeout, int, 0);
MODULE_PARM_DESC(read_timeout, "Read timeout in ms");

/* As of kernel version 2.6.4 ehci-hcd uses an
 * "only one interrupt transfer per frame" shortcut
 * to simplify the scheduling of periodic transfers.
 * This conflicts with our standard 1ms intervals for in and out URBs.
 * We use default intervals of 2ms for in and 8ms for out transfers,
 * which is fast enough for 2400 baud and allows a small additional load.
 * Increase the interval to allow more devices that do interrupt transfers,
 * or set to 0 to use the standard interval from the endpoint descriptors.
 */
static int interrupt_in_interval = 2;
module_param(interrupt_in_interval, int, 0);
MODULE_PARM_DESC(interrupt_in_interval, "Interrupt in interval in ms");

static int interrupt_out_interval = 8;
module_param(interrupt_out_interval, int, 0);
MODULE_PARM_DESC(interrupt_out_interval, "Interrupt out interval in ms");

MODULE_DEVICE_TABLE (usb, eRGlass_usb_table);
static DEFINE_MUTEX(open_disc_mutex);

// Structure version eRGlass firmware 
struct erglass_swversion {
    u8 protocol_major;
    u8 protocol_minor;
    u8 boot_major;
    u8 boot_minor;
    u8 appl_major;
    u8 appl_minor;
    u8 appl_patch;
    u8 appl_day;
    u8 appl_month;
    u8 appl_year;
    u8 hw_revision;
};

// Structure imu
struct erglass_imu {
    int imu_accX;
    int imu_accY;
    int imu_accZ;
    int imu_gyroX;
    int imu_gyroY;
    int imu_gyroZ;
    int imu_magX;
    int imu_magY;
    int imu_magZ;
    int compass;    
};

// Structure keys
struct erglass_key {
     unsigned char key[4];
};

// Structure leds
struct erglass_led {
    unsigned char ledred;
    unsigned char ledgreen;
    unsigned char ledblue;      
};

// Structure i2c write
struct erglass_i2cwrite {
    unsigned char       deviceaddress;
    unsigned char       deviceconfiguration;
    unsigned short int  registeraddress;
    unsigned char       numdata;
    unsigned char       data[32];
};

// Structure i2c read
struct erglass_i2cread {
    unsigned char       numdata;
    unsigned char       data[32];
};

// Structure to hold all of our device specific stuff 
struct eRGlass_usb {
    struct mutex        lock;       // locks this structure
    struct usb_device*  udev;       // save off the usb device pointer
    unsigned char       minor;      // the starting minor number for this device 

    int                 open_count;    // number of times this port has been opened 

    char                *read_buffer;
    size_t              read_buffer_length; 
    size_t              read_packet_length; 
    spinlock_t          read_buffer_lock;
    int                 packet_timeout_jiffies;
    unsigned long       read_last_arrival;

    wait_queue_head_t   read_wait;
    wait_queue_head_t   write_wait;

    char                *interrupt_in_buffer;
    struct usb_endpoint_descriptor* interrupt_in_endpoint;
    struct urb          *interrupt_in_urb;
    int                 interrupt_in_interval;
    int                 interrupt_in_running;
    int                 interrupt_in_done;
    unsigned long       interrupt_in_timeout;

    char                *interrupt_out_buffer;
    struct usb_endpoint_descriptor* interrupt_out_endpoint;
    struct urb          *interrupt_out_urb;
    int                 interrupt_out_interval;
    int                 interrupt_out_busy;
    
    struct timer_list       timer_read;
    struct work_struct      work_read;
    struct workqueue_struct *workqueue;

    struct input_dev        *keys_dev;
    
    int                         swversion_valid;
    struct erglass_swversion    swversion;
    struct erglass_imu          imu;    
    int                         status;
    int                         errorcode;
    int                         asl;    
    int                         haptic;
    struct erglass_key          keyb;
    struct erglass_led          led;
    struct erglass_i2cread      i2c_read;
};

// code key
const unsigned char tb_eRGlassKeys[] = { KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10 };


#define ERGLASS_TIME                20
#define ERGLASS_TIMER_READ          msecs_to_jiffies(ERGLASS_TIME)

// local function prototypes
// static ssize_t eRGlass_usb_read(struct file *file, char __user *buffer, size_t count, loff_t *ppos);
// static ssize_t eRGlass_usb_write(struct file *file, const char __user *buffer, size_t count, loff_t *ppos);
static inline void eRGlass_usb_delete(struct eRGlass_usb *dev);
static int eRGlass_usb_open(struct inode *inode, struct file *file);
static int eRGlass_usb_release(struct inode *inode, struct file *file);
static unsigned int eRGlass_usb_poll(struct file *file, poll_table *wait);
static long eRGlass_usb_ioctl(struct file *file, unsigned int cmd, unsigned long arg);

static void eRGlass_usb_abort_transfers(struct eRGlass_usb *dev);
static void eRGlass_usb_check_for_read_packet(struct eRGlass_usb *dev);
static void eRGlass_usb_interrupt_in_callback(struct urb *urb);
static void eRGlass_usb_interrupt_out_callback(struct urb *urb);

static int  eRGlass_usb_probe(struct usb_interface *interface, const struct usb_device_id *id);
static void eRGlass_usb_disconnect(struct usb_interface *interface);


// file operations needed when we register this driver 
static const struct file_operations eRGlass_usb_fops = {
    .owner =            THIS_MODULE,
    //.read  =            eRGlass_usb_read,
    //.write =            eRGlass_usb_write,
    .open =             eRGlass_usb_open,
    .unlocked_ioctl =   eRGlass_usb_ioctl,
    .release =          eRGlass_usb_release,
    .poll =             eRGlass_usb_poll,
};

static char *eRGlass_usb_devnode(struct device *dev, umode_t *mode)
{
    if (mode != NULL)
    {
        // to grant permissions to all users
        *mode = S_IRUGO | S_IWUGO;
    }
    return kasprintf(GFP_KERNEL, "usb/%s", dev_name(dev));
}

/*
 * usb class driver info in order to get a minor number from the usb core,
 * and to have the device registered with the driver core
 */
static struct usb_class_driver eRGlass_usb_class = {
    .name =         "VisionAR%d",
    .devnode =      eRGlass_usb_devnode,
    .fops =         &eRGlass_usb_fops,
    .minor_base =   USB_ERGLASS_MINOR_BASE,
};

/* usb specific object needed to register this driver with the usb subsystem */
static struct usb_driver eRGlass_usb_driver = {
    .name =         "VisionAR USB",
    .probe =        eRGlass_usb_probe,
    .disconnect =   eRGlass_usb_disconnect,
    .id_table =     eRGlass_usb_table,    
};

// serial command
#define ERGLASSER_CMD_GETVERSION            0x81
#define ERGLASSER_CMD_GETSTATUS             0x90
#define ERGLASSER_CMD_GETERRORS             0x91
#define ERGLASSER_CMD_GENERAL               0xA0
#define ERGLASSER_CMD_RGB                   0xA1
#define ERGLASSER_CMD_HAPTIC                0xA2
#define ERGLASSER_CMD_I2CWRITE              0xB0
#define ERGLASSER_CMD_I2CREAD               0xB1

// serial command bit value
#define BIT_ERROR_PRESENCE                  0x0100

// IOCTL command
#define IOCTL_ERGLASS_CMD_GETVERSION        ERGLASSER_CMD_GETVERSION
#define IOCTL_ERGLASS_CMD_GETSTATUS         ERGLASSER_CMD_GETSTATUS
#define IOCTL_ERGLASS_CMD_GETERRORS         ERGLASSER_CMD_GETERRORS
#define IOCTL_ERGLASS_CMD_GENERAL           ERGLASSER_CMD_GENERAL
#define IOCTL_ERGLASS_CMD_I2CWRITE          ERGLASSER_CMD_I2CWRITE
#define IOCTL_ERGLASS_CMD_I2CREAD_REQUEST   ERGLASSER_CMD_I2CREAD+1
#define IOCTL_ERGLASS_CMD_I2CREAD           ERGLASSER_CMD_I2CREAD

////////////////////////////////////////////////////////////////////////
// structure for communication with eRGlass firmware
#define HEADER_LEN      5
#define DATA_LEN(l)     (2+l)

#pragma pack(push, 1)
// frame communication 
typedef struct {
    uint8_t     _SOH;
    uint8_t     control;
    uint16_t    total;
    uint8_t     header_chk;
    uint8_t     command;
    uint8_t     data[0x100];
} FRAME;

// general pipe 
typedef struct {
    void          *pfirst;
    void          *pw;
    void          *pr;
    void          *plast;
    unsigned char szUserType;
} PIPE;

// command store
typedef struct {
    unsigned char   command;
    unsigned char   data[32];
    unsigned char   len;
} COMMAND;
#pragma pack(pop)

static unsigned int m_Connect=0;
static FRAME m_frame;
static PIPE pipeCommand;
static COMMAND commandBuffer[10];

//////////////////////////////////////////////////////////////////////////////
static void command_init(void)
{
    ALOGV("%s", __func__);

    pipeCommand.pfirst = (void *)&commandBuffer[0];
    pipeCommand.pw =(void *)&commandBuffer[0];;
    pipeCommand.pr =(void *)&commandBuffer[0];;
    pipeCommand.plast =(void *)&commandBuffer[9];
    pipeCommand.szUserType = sizeof(COMMAND);    
}

//////////////////////////////////////////////////////////////////////////////
static bool command_is(void)
{
    return (pipeCommand.pr != pipeCommand.pw);    
}

//////////////////////////////////////////////////////////////////////////////
static void command_put(COMMAND cmd)
{
    ALOGV("%s(%02Xh)", __func__, cmd.command);

    memcpy((unsigned char *)pipeCommand.pw, (unsigned char *)&cmd, pipeCommand.szUserType);

    if (pipeCommand.pw == pipeCommand.plast)
        pipeCommand.pw = pipeCommand.pfirst;
    else
        pipeCommand.pw = (void *)((char *)(pipeCommand.pw + pipeCommand.szUserType));
}

//////////////////////////////////////////////////////////////////////////////
static void command_get(COMMAND *cmd)
{
    ALOGV("%s", __func__);

    memcpy((unsigned char *)cmd, (unsigned char *)pipeCommand.pr, pipeCommand.szUserType);

    if (pipeCommand.pr == pipeCommand.plast)
        pipeCommand.pr = pipeCommand.pfirst;
    else
        pipeCommand.pr = (void *)((char *)(pipeCommand.pr + pipeCommand.szUserType));
}

//////////////////////////////////////////////////////////////////////////////
int eRGlass_usb_prepareframe(char command, const char *pdata, int len)
{
    int ii, framelen;
    char *p;

    ALOGV("%s(%02Xh)", __func__, command);

    m_frame._SOH = 0x01;
    m_frame.control = 0x01;
    m_frame.total = DATA_LEN(len);
    m_frame.header_chk = 0x00;
    m_frame.command = command;

    if(len != 0)
        memcpy(m_frame.data, pdata, len);

    m_frame.data[len] = 0x00;

    // checksum header
    p = (char *)&m_frame._SOH;
    for (ii = 0; ii < 4; ii++)
        m_frame.header_chk ^= *p++;

    // checksum data
    p = (char *)&m_frame.command;
    for (ii = 0; ii < len+1; ii++)
        m_frame.data[len] ^= *p++;

    framelen = HEADER_LEN+DATA_LEN(len);

    ALOGD("%s:\n"
            "\tSOH             =   %02Xh\n"
            "\tcontrol         =   %02Xh\n"
            "\ttotal bytes     = %04Xh\n"
            "\theader checksum =   %02Xh\n"
            "\tcommand         =   %02Xh\n"
            "\tdata checksum   =   %02Xh",
            __func__,
            m_frame._SOH, m_frame.control, m_frame.total, m_frame.header_chk, m_frame.command, (int)m_frame.data[len]);
    eRGlass_usb_debug_data (__func__, m_frame.total, m_frame.data);

    return framelen;
}

//////////////////////////////////////////////////////////////////////////////
int eRGlass_usb_process(struct eRGlass_usb *dev, char *buffer, int len)
{
    FRAME *pFrameRX;
    char *p;
    uint8_t header_chk, data_chk;
    int ii;
    int swipeUp, swipeDw, doubleTap;

    ALOGV("%s", __func__);

    if (len > HEADER_LEN) {
        pFrameRX = (FRAME *)buffer;

        // checksum header
        p = (char *)&pFrameRX->_SOH;
        header_chk = 0;
        for (ii = 0; ii < 4; ii++)
            header_chk ^= *p++;

        if (pFrameRX->header_chk == header_chk) {
            // checksum data
            p = (char *)&pFrameRX->command;
            data_chk = 0;
            for (ii = 0; ii < pFrameRX->total-1; ii++)
                data_chk ^= *p++;

            if (pFrameRX->data[pFrameRX->total-2] == data_chk) {
               
                switch(pFrameRX->command) {
                    case ERGLASSER_CMD_GETVERSION:
                        dev->swversion.protocol_major = pFrameRX->data[0];
                        dev->swversion.protocol_minor = pFrameRX->data[1];
                        dev->swversion.boot_major = pFrameRX->data[2];
                        dev->swversion.boot_minor = pFrameRX->data[3];
                        dev->swversion.appl_major = pFrameRX->data[4];
                        dev->swversion.appl_minor = pFrameRX->data[5];
                        dev->swversion.appl_patch = pFrameRX->data[6];
                        dev->swversion.appl_day = pFrameRX->data[7];
                        dev->swversion.appl_month = pFrameRX->data[8];
                        dev->swversion.appl_year = pFrameRX->data[9];
                        dev->swversion.hw_revision = 'A';
                        if (pFrameRX->total > 12)
                            dev->swversion.hw_revision = pFrameRX->data[10];
			dev->swversion_valid = 1;
                        break;

                    case ERGLASSER_CMD_GETSTATUS:
                        p = &pFrameRX->data[0];
                        dev->status = cpu_to_le32p((u32 *)p);           p+=sizeof(u32);
                        dev->imu.imu_accX = cpu_to_le16p((u16 *)p);     p+=sizeof(u16);
                        dev->imu.imu_accY = cpu_to_le16p((u16 *)p);     p+=sizeof(u16);
                        dev->imu.imu_accZ = cpu_to_le16p((u16 *)p);     p+=sizeof(u16);
                        dev->imu.imu_gyroX = cpu_to_le16p((u16 *)p);    p+=sizeof(u16);
                        dev->imu.imu_gyroY = cpu_to_le16p((u16 *)p);    p+=sizeof(u16);
                        dev->imu.imu_gyroZ = cpu_to_le16p((u16 *)p);    p+=sizeof(u16);
                        dev->imu.imu_magX = cpu_to_le16p((u16 *)p);     p+=sizeof(u16);
                        dev->imu.imu_magY = cpu_to_le16p((u16 *)p);     p+=sizeof(u16);
                        dev->imu.imu_magZ = cpu_to_le16p((u16 *)p);     p+=sizeof(u16);
                        dev->asl = cpu_to_le16p((u16 *)p);              p+=sizeof(u16);
                        dev->keyb.key[0] = *p;                          p+=sizeof(u8);
                        dev->keyb.key[1] = *p;                          p+=sizeof(u8);
                        dev->keyb.key[2] = *p;                          p+=sizeof(u8);
                        dev->keyb.key[3] = *p;                          p+=sizeof(u8);
                        dev->imu.compass = cpu_to_le16p((u16 *)p);      p+=sizeof(u16); 

                        swipeUp = swipeDw = doubleTap = 0;
                        for (ii = 0; ii < 4; ii++) {
                            if (dev->keyb.key[ii]) {
                                ALOGD("%s: KEY %d %02X", __func__, ii+1, dev->keyb.key[ii]);
                            }

                            if (dev->keyb.key[ii] & 0x10) {
                                ALOGD("%s: KEY DOUBLE TAP", __func__);
                            }

                            if (dev->keyb.key[ii] & 0x20) {
                                swipeDw = 1;
                                ALOGD("%s: KEY SWIPE DOWN", __func__);
                            }

                            if (dev->keyb.key[ii] & 0x40) {
                                swipeUp = 1;
                                ALOGD("%s: KEY SWIPE UP", __func__);
                            }
                            dev->keyb.key[ii] &= 0x1F;
                        }

                        input_report_key(dev->keys_dev, KEY_F1, (dev->keyb.key[0] && dev->keyb.key[0] <= 0x0F));
                        input_report_key(dev->keys_dev, KEY_F2, (dev->keyb.key[1] && dev->keyb.key[1] <= 0x0F));
                        input_report_key(dev->keys_dev, KEY_F3, (dev->keyb.key[2] && dev->keyb.key[2] <= 0x0F));
                        input_report_key(dev->keys_dev, KEY_F4, (dev->keyb.key[3] && dev->keyb.key[3] <= 0x0F));
                        input_report_key(dev->keys_dev, KEY_F5, dev->keyb.key[0] >= 0x10);  // double tap
                        input_report_key(dev->keys_dev, KEY_F6, dev->keyb.key[1] >= 0x10);  // double tap
                        input_report_key(dev->keys_dev, KEY_F7, dev->keyb.key[2] >= 0x10);  // double tap
                        input_report_key(dev->keys_dev, KEY_F8, dev->keyb.key[3] >= 0x10);  // double tap
                        input_report_key(dev->keys_dev, KEY_F9,  swipeDw);
                        input_report_key(dev->keys_dev, KEY_F10, swipeUp);

                        input_sync(dev->keys_dev);
                        break;

                    case ERGLASSER_CMD_GETERRORS:
                    {
                        unsigned char num_errors;
                        p = &pFrameRX->data[0];

                        num_errors = *p; p+=sizeof(u8);
                        if (num_errors) {
                            dev->errorcode = cpu_to_le16p((u16 *)p); p+=sizeof(u16);
                        } else {
                            dev->errorcode = 0;
                        }
                    }
                        break;

                    case ERGLASSER_CMD_I2CREAD:
                    {
                        int ii;
                        p = &pFrameRX->data[0];
                        dev->i2c_read.numdata = *p; p+=sizeof(u8);
                        if (dev->i2c_read.numdata > 32)
                            dev->i2c_read.numdata = 32;

                        for (ii = 0; ii < dev->i2c_read.numdata; ii++) {
                            dev->i2c_read.data[ii] = *p; p+=sizeof(u8);
                        }
                    }
                        break;
 
            default:
                        break;
                }
            }
        }
    }
    return 0;
}

/**
 *    eRGlass_usb_delete
 */
static inline void eRGlass_usb_delete (struct eRGlass_usb *dev)
{
    ALOGV("%s", __func__);

    eRGlass_usb_abort_transfers (dev);

    /* free data structures */
    usb_free_urb(dev->interrupt_in_urb);
    usb_free_urb(dev->interrupt_out_urb);
    kfree (dev->read_buffer);
    kfree (dev->interrupt_in_buffer);
    kfree (dev->interrupt_out_buffer);
    kfree (dev);
}


/**
 *    eRGlass_usb_open
 */
static int eRGlass_usb_open (struct inode *inode, struct file *file)
{
    struct eRGlass_usb *dev = NULL;
    int subminor;
    int retval = 0;
    struct usb_interface *interface;

    ALOGV("%s", __func__);

    nonseekable_open(inode, file);
    subminor = iminor(inode);

    interface = usb_find_interface (&eRGlass_usb_driver, subminor);

    if (!interface) {
        ALOGE("%s: error, can't find device for minor %d", __func__, subminor);
        retval = -ENODEV;
        goto exit;
    }

    mutex_lock(&open_disc_mutex);
    dev = usb_get_intfdata(interface);

    if (!dev) {
        mutex_unlock(&open_disc_mutex);
        retval = -ENODEV;
        goto exit;
    }

    /* lock this device */
    if (mutex_lock_interruptible(&dev->lock)) {
        mutex_unlock(&open_disc_mutex);
            retval = -ERESTARTSYS;
        goto exit;
    }


    /* allow opening only once */
    if (dev->open_count) {
        mutex_unlock(&open_disc_mutex);
        retval = -EBUSY;
        goto unlock_exit;
    }
    dev->open_count = 1;
    mutex_unlock(&open_disc_mutex);

    /* initialize dev data */
    dev->swversion_valid = 0;

    /* initialize in direction */
/*     
    dev->read_buffer_length = 0;
    dev->read_packet_length = 0;
    usb_fill_int_urb (dev->interrupt_in_urb,
              dev->udev,
              usb_rcvintpipe(dev->udev, dev->interrupt_in_endpoint->bEndpointAddress),
              dev->interrupt_in_buffer,
              usb_endpoint_maxp(dev->interrupt_in_endpoint),
              eRGlass_usb_interrupt_in_callback,
              dev,
              dev->interrupt_in_interval);

    dev->interrupt_in_running = 1;
    dev->interrupt_in_done = 0;
    mb();

    retval = usb_submit_urb (dev->interrupt_in_urb, GFP_KERNEL);
    if (retval) {
        dev_err(&dev->udev->dev,
            "Couldn't submit interrupt_in_urb %d\n", retval);
        dev->interrupt_in_running = 0;
        dev->open_count = 0;
        goto unlock_exit;
    }
*/
    /* save device in the file's private structure */
    file->private_data = dev;

unlock_exit:
    mutex_unlock(&dev->lock);

exit:
    return retval;
}

/**
 *    eRGlass_usb_release
 */
static int eRGlass_usb_release (struct inode *inode, struct file *file)
{
    struct eRGlass_usb *dev;
    int retval = 0;

    ALOGV("%s", __func__);

    dev = file->private_data;

    if (dev == NULL) {
        ALOGV("%s: object is NULL", __func__);
        retval = -ENODEV;
        goto exit_nolock;
    }

    mutex_lock(&open_disc_mutex);
    if (mutex_lock_interruptible(&dev->lock)) {
        retval = -ERESTARTSYS;
        goto exit;
    }

    if (dev->open_count != 1) {
        ALOGE("%s: device not opened exactly once", __func__);
        retval = -ENODEV;
        goto unlock_exit;
    }
    if (dev->udev == NULL) {
        /* the device was unplugged before the file was released */

        /* unlock here as eRGlass_usb_delete frees dev */
        mutex_unlock(&dev->lock);
        eRGlass_usb_delete (dev);
        goto exit;
    }

    /* wait until write transfer is finished */
    if (dev->interrupt_out_busy) {
        wait_event_interruptible_timeout (dev->write_wait, !dev->interrupt_out_busy, 2 * HZ);
    }
    eRGlass_usb_abort_transfers (dev);
    dev->open_count = 0;

    unlock_exit:
        mutex_unlock(&dev->lock);

    exit:
        mutex_unlock(&open_disc_mutex);

    exit_nolock:

    return retval;
}


/**
 *    eRGlass_usb_abort_transfers
 *      aborts transfers and frees associated data structures
 */
static void eRGlass_usb_abort_transferFiore (struct eRGlass_usb *dev)
{
    ALOGV("%s", __func__);

    if (dev == NULL) {
        ALOGV("%s: dev is null", __func__);
        return;
    }
    usb_kill_urb(dev->interrupt_in_urb);
    usb_kill_urb(dev->interrupt_out_urb);
    visionar_unlock(__func__);
}


static void eRGlass_usb_abort_transfers (struct eRGlass_usb *dev)
{
    ALOGV("%s", __func__);

    if (dev == NULL) {
        ALOGV("%s: dev is null", __func__);
        return;
    }

    /* shutdown transfer */
    if (dev->interrupt_in_running) {
        dev->interrupt_in_running = 0;
        mb();
        if (dev->udev)
            usb_kill_urb (dev->interrupt_in_urb);
    }
    if (dev->interrupt_out_busy && dev->udev)
        usb_kill_urb(dev->interrupt_out_urb);
}


/**
 *    eRGlass_usb_check_for_read_packet
 *
 *      To get correct semantics for signals and non-blocking I/O
 *      with packetizing we pretend not to see any data in the read buffer
 *      until it has been there unchanged for at least
 *      dev->packet_timeout_jiffies, or until the buffer is full.
 */
static void eRGlass_usb_check_for_read_packet (struct eRGlass_usb *dev)
{
    ALOGV("%s", __func__);

    spin_lock_irq (&dev->read_buffer_lock);
    if (!packet_timeout
        || time_after(jiffies, dev->read_last_arrival + dev->packet_timeout_jiffies)
        || dev->read_buffer_length == read_buffer_size) {
        dev->read_packet_length = dev->read_buffer_length;
    }
    dev->interrupt_in_done = 0;
    spin_unlock_irq (&dev->read_buffer_lock);
}


/**
 *    eRGlass_usb_poll
 */
static unsigned int eRGlass_usb_poll (struct file *file, poll_table *wait)
{
    struct eRGlass_usb *dev;
    unsigned int mask = 0;

    ALOGV("%s", __func__);

    dev = file->private_data;

    if (!dev->udev)
        return POLLERR | POLLHUP;

    poll_wait(file, &dev->read_wait, wait);
    poll_wait(file, &dev->write_wait, wait);

    eRGlass_usb_check_for_read_packet(dev);
    if (dev->read_packet_length > 0) {
        mask |= POLLIN | POLLRDNORM;
    }
    if (!dev->interrupt_out_busy) {
        mask |= POLLOUT | POLLWRNORM;
    }

    return mask;
}


#if 0
/* useless functions */
/**
 *    eRGlass_usb_read
 */
static ssize_t eRGlass_usb_read (struct file *file, char __user *buffer, size_t count, loff_t *ppos)
{
    struct eRGlass_usb *dev;
    size_t bytes_to_read;
    int i;
    int retval = 0;
    unsigned long timeout = 0;

    ALOGV("%s", __func__);

    dev = file->private_data;

    /* lock this object */
    if (mutex_lock_interruptible(&dev->lock)) {
        retval = -ERESTARTSYS;
        goto exit;
    }

    /* verify that the device wasn't unplugged */
    if (dev->udev == NULL) {
        retval = -ENODEV;
        ALOGE("%s: No device or device unplugged %d", __func__, retval);
        goto unlock_exit;
    }

    /* verify that we actually have some data to read */
    if (count == 0) {
        ALOGE("%s: read request of 0 bytes", __func__);
        goto unlock_exit;
    }

    if (read_timeout) {
        timeout = jiffies + read_timeout * HZ / 1000;
    }

    /* wait for data */
    eRGlass_usb_check_for_read_packet (dev);
    while (dev->read_packet_length == 0) {
        if (file->f_flags & O_NONBLOCK) {
            retval = -EAGAIN;
            goto unlock_exit;
        }
        retval = wait_event_interruptible_timeout(dev->read_wait, dev->interrupt_in_done, dev->packet_timeout_jiffies);
        if (retval < 0) {
            goto unlock_exit;
        }

        /* reset read timeout during read or write activity */
        if (read_timeout
            && (dev->read_buffer_length || dev->interrupt_out_busy)) {
            timeout = jiffies + read_timeout * HZ / 1000;
        }
        /* check for read timeout */
        if (read_timeout && time_after (jiffies, timeout)) {
            retval = -ETIMEDOUT;
            goto unlock_exit;
        }
        eRGlass_usb_check_for_read_packet (dev);
    }

    /* copy the data from read_buffer into userspace */
    bytes_to_read = min(count, dev->read_packet_length);

    if (copy_to_user (buffer, dev->read_buffer, bytes_to_read)) {
        retval = -EFAULT;
        goto unlock_exit;
    }

    spin_lock_irq (&dev->read_buffer_lock);
    dev->read_buffer_length -= bytes_to_read;
    dev->read_packet_length -= bytes_to_read;
    for (i=0; i<dev->read_buffer_length; i++) {
        dev->read_buffer[i] = dev->read_buffer[i+bytes_to_read];
    }
    spin_unlock_irq (&dev->read_buffer_lock);

    retval = bytes_to_read;

unlock_exit:
    /* unlock the device */
    mutex_unlock(&dev->lock);

exit:
    return retval;
}


/**
 *    eRGlass_usb_write
 */
static ssize_t eRGlass_usb_write (struct file *file, const char __user *buffer, size_t count, loff_t *ppos)
{
    struct eRGlass_usb *dev;
    size_t bytes_to_write;
    int retval = 0;

    ALOGV("%s", __func__);

    dev = file->private_data;

    /* lock this object */
    if (mutex_lock_interruptible(&dev->lock)) {
        retval = -ERESTARTSYS;
        goto exit;
    }

    /* verify that the device wasn't unplugged */
    if (dev->udev == NULL) {
        retval = -ENODEV;
        ALOGE("%s: No device or device unplugged %d", __func__, retval);
        goto unlock_exit;
    }

    /* verify that we actually have some data to write */
    if (count == 0) {
        ALOGE("%s: write request of 0 bytes", __func__);
        goto unlock_exit;
    }

    /* wait until previous transfer is finished */
    while (dev->interrupt_out_busy) {
        if (file->f_flags & O_NONBLOCK) {
            retval = -EAGAIN;
            goto unlock_exit;
        }
        retval = wait_event_interruptible (dev->write_wait, !dev->interrupt_out_busy);
        if (retval) {
            goto unlock_exit;
        }
    }

    /* write the data into interrupt_out_buffer from userspace */
    bytes_to_write = min_t(int, count, write_buffer_size);
    ALOGV("%s: count = %Zd, bytes_to_write = %Zd", __func__, count, bytes_to_write);

    if (copy_from_user (dev->interrupt_out_buffer, buffer, bytes_to_write)) {
        retval = -EFAULT;
        goto unlock_exit;
    }

    /* send off the urb */
    usb_fill_int_urb(dev->interrupt_out_urb,
             dev->udev,
             usb_sndintpipe(dev->udev, dev->interrupt_out_endpoint->bEndpointAddress),
             dev->interrupt_out_buffer,
             bytes_to_write,
             eRGlass_usb_interrupt_out_callback,
             dev,
             dev->interrupt_out_interval);

    dev->interrupt_out_busy = 1;
    wmb();

    retval = usb_submit_urb (dev->interrupt_out_urb, GFP_KERNEL);
    if (retval) {
        dev->interrupt_out_busy = 0;
        ALOGE("%s: Couldn't submit interrupt_out_urb %d", retval);
        goto unlock_exit;
    }
    retval = bytes_to_write;
    
unlock_exit:
    /* unlock the device */
    mutex_unlock(&dev->lock);

exit:
    return retval;
}
#endif


/**
 *    eRGlass_usb_interrupt_in_callback
 */
static void eRGlass_usb_interrupt_in_callback (struct urb *urb)
{
    struct eRGlass_usb *dev = urb->context;
    int status = urb->status;
#if 0
    int retval;
#endif

    visionar_unlock(__func__);

    if(m_Connect == 0){
        return;
    }

    ALOGV("%s: status = %d", __func__, status);

    eRGlass_usb_debug_data(__func__, urb->actual_length, urb->transfer_buffer);

    if (status) {
#if 0
        if (status == -ENOENT ||
            status == -ECONNRESET ||
            status == -ESHUTDOWN) {
            goto exit;
        } else {
            ALOGE("%s: nonzero status received: %d", __func__, status);
            goto resubmit; /* maybe we can recover */
        }
#else
        ALOGV("%s: nonzero status received: %d", __func__, status);
        dev->interrupt_in_urb->hcpriv = NULL;
        goto exit;
#endif
    }

    if (urb->actual_length > 0) {
        char buffer[512];

        spin_lock (&dev->read_buffer_lock);
        if (dev->read_buffer_length + urb->actual_length < read_buffer_size) {
            memcpy (dev->read_buffer + dev->read_buffer_length,
                dev->interrupt_in_buffer,
                urb->actual_length);
            dev->read_buffer_length += urb->actual_length;
            dev->read_last_arrival = jiffies;

            ALOGV("%s: address buffer=%p: received %lu bytes", __func__, dev->read_buffer, dev->read_buffer_length);
            eRGlass_usb_debug_data(__func__, dev->read_buffer_length, dev->read_buffer);

            if (dev->read_buffer_length < sizeof(buffer)) {
                if (copy_to_user (buffer, dev->read_buffer, dev->read_buffer_length) == 0) {
                   eRGlass_usb_process(dev, buffer, dev->read_buffer_length);
                }
            }

            ALOGV("%s: received %d bytes", __func__, urb->actual_length);
        } else {
            ALOGW("%s: read_buffer overflow, %d bytes dropped", __func__, urb->actual_length);
        }
        spin_unlock (&dev->read_buffer_lock);
    }

#if 0
resubmit:
    /* resubmit if we're still running */
    if (dev->interrupt_in_running && dev->udev) {
        retval = usb_submit_urb (dev->interrupt_in_urb, GFP_ATOMIC);
        if (retval){
            ALOGV("%s: usb_submit_urb failed (%d)", __func__, retval);
        }
    }
#endif

exit:
    dev->interrupt_in_done = 1;
    wake_up_interruptible (&dev->read_wait);

    eRGlass_usb_debug_data(__func__, urb->actual_length, urb->transfer_buffer);
}


/**
 *    eRGlass_usb_interrupt_out_callback
 */
void eRGlass_usb_interrupt_out_callback (struct urb *urb)
{
    struct eRGlass_usb *dev = urb->context;
    int status = urb->status;

    if(m_Connect == 0){
        return;
    }

    ALOGV("%s: status %d", __func__, status);
    eRGlass_usb_debug_data(__func__, urb->actual_length, urb->transfer_buffer);

    /* sync/async unlink faults aren't errors */
    if (status && !(status == -ENOENT ||
            status == -ECONNRESET ||
            status == -ESHUTDOWN)) {
        ALOGE("%s - nonzero write bulk status received: %d", __func__, status);
    }

    dev->interrupt_out_busy = 0;
    wake_up_interruptible(&dev->write_wait);

    eRGlass_usb_debug_data(__func__, urb->actual_length, urb->transfer_buffer);
}

/**
 *  eRGlass_usb_ioctl
 */
static long eRGlass_usb_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    struct eRGlass_usb *dev;
    char *p;
    int value;
    COMMAND eRGlassCommand;

    dev = file->private_data;
    if (dev == NULL)
        return -ENODEV;

    ALOGV("%s: cmd=%d value=%ld", __func__, cmd, arg);

    if (cmd == IOCTL_ERGLASS_CMD_GETVERSION) {
        struct erglass_swversion *pVersion;
        pVersion = (struct erglass_swversion *)arg;

        ALOGV("%s: firmware eRGlass B%d.%02d V%d.%02d.%02d %02d.%02d.%02d HW=%c",
               __func__,
               dev->swversion.boot_major, dev->swversion.boot_minor,
               dev->swversion.appl_major, dev->swversion.appl_minor, dev->swversion.appl_patch,
               dev->swversion.appl_day, dev->swversion.appl_month, dev->swversion.appl_year,
               dev->swversion.hw_revision);

        if (copy_to_user(pVersion, &dev->swversion, sizeof(struct erglass_swversion)))
            return -EFAULT;
    } else if (cmd == IOCTL_ERGLASS_CMD_GETSTATUS) {
        int *pstatus;
        pstatus = (int *)arg;

        ALOGV("%s: status %d [%08X]", __func__, dev->status, dev->status);

        if (copy_to_user(pstatus, &dev->status, sizeof(int)))
            return -EFAULT;
    } else if (cmd == IOCTL_ERGLASS_CMD_GETERRORS) {
        int *perrorcode;
        perrorcode = (int *)arg;

        ALOGV("%s: code error %d", __func__, dev->errorcode);

        if (copy_to_user(perrorcode, &dev->errorcode, sizeof(int)))
            return -EFAULT;        
    } else if (cmd == IOCTL_ERGLASS_CMD_GENERAL) {
        value = arg;

        p = &eRGlassCommand.data[0];
        *(u16 *)p = cpu_to_le16(value); 

        ALOGV("%s: command %d", __func__, value);

        eRGlassCommand.command = ERGLASSER_CMD_GENERAL;
        eRGlassCommand.len = 2;
        command_put(eRGlassCommand);       
    } else if (cmd == IOCTL_ERGLASS_CMD_I2CWRITE) {
        struct erglass_i2cwrite *pi2cwrite;
        char *p;
        int ii;

        pi2cwrite = (struct erglass_i2cwrite *)arg;

        ALOGV("%s: i2c write request:\n"
                "\tdeviceaddress       = %02Xh\n"
                "\tdeviceconfiguration = %02Xh\n"
                "\tregisteraddress     = %02Xh\n"
                "\tnumdata             = %02Xh",
                __func__,
                pi2cwrite->deviceaddress, pi2cwrite->deviceconfiguration, pi2cwrite->registeraddress, pi2cwrite->numdata);
        eRGlass_usb_debug_data(__func__, pi2cwrite->numdata, pi2cwrite->data);

        eRGlassCommand.command = ERGLASSER_CMD_I2CWRITE;
        p = (char *)&eRGlassCommand.data[0];
        *(u8 *)p = pi2cwrite->deviceaddress;                    p+=sizeof(u8);
        *(u8 *)p = pi2cwrite->deviceconfiguration;              p+=sizeof(u8);
        *(u16 *)p = cpu_to_le16(pi2cwrite->registeraddress);    p+=sizeof(u16);
        *(u8 *)p = pi2cwrite->numdata;                          p+=sizeof(u8);
         for (ii = 0; ii < pi2cwrite->numdata; ii++) {
            if (ii >= 32)
                break;
           *(u8 *)p = pi2cwrite->data[ii];
            p+=sizeof(u8);
        }
        eRGlassCommand.len = 5+pi2cwrite->numdata;
        command_put(eRGlassCommand);

    } else if (cmd == IOCTL_ERGLASS_CMD_I2CREAD_REQUEST) {
        struct erglass_i2cwrite *pi2cwrite;
        char *p;

        pi2cwrite = (struct erglass_i2cwrite *)arg;

        ALOGV("%s: i2c read request:\n"
                "\tdeviceaddress       = %02Xh\n"
                "\tdeviceconfiguration = %02Xh\n"
                "\tregisteraddress     = %02Xh\n"
                "\tnumdata             = %02Xh",
                __func__,
                pi2cwrite->deviceaddress, pi2cwrite->deviceconfiguration, pi2cwrite->registeraddress, pi2cwrite->numdata);

        eRGlassCommand.command = ERGLASSER_CMD_I2CREAD;
        p = (char *)&eRGlassCommand.data[0];
        *(u8 *)p = pi2cwrite->deviceaddress;                    p+=sizeof(u8);
        *(u8 *)p = pi2cwrite->deviceconfiguration;              p+=sizeof(u8);
       *(u16 *)p = cpu_to_le16(pi2cwrite->registeraddress);    p+=sizeof(u16);
        *(u8 *)p = pi2cwrite->numdata; 
        eRGlassCommand.len = 5;
        command_put(eRGlassCommand);
    } else if (cmd == IOCTL_ERGLASS_CMD_I2CREAD) {
        struct erglass_i2cread *pi2cread;
        pi2cread = (struct erglass_i2cread *)arg;

        ALOGV("%s: i2c read request: total=%d", __func__, dev->i2c_read.numdata);
        eRGlass_usb_debug_data(__func__, dev->i2c_read.numdata, dev->i2c_read.data);

        if (copy_to_user(pi2cread, &dev->i2c_read, sizeof(struct erglass_i2cread)))
            return -EFAULT;
    }

    return 0;
}


/**
 *  eRGlass_usb_send
 */
static size_t eRGlass_usb_send(struct eRGlass_usb *dev, unsigned char *buf, size_t size)
{
    int retval = 0;
    size_t bytes_to_write;

    ALOGV("%s", __func__);

    // lock this object
    mutex_lock(&dev->lock);

    // verify that the device wasn't unplugged
    if (dev->udev == NULL) {
        retval = -ENODEV;
        ALOGE("%s: No device or device unplugged %d", __func__, retval);
        goto send_unlock_exit;
    }

    // verify that we actually have some data to write
    if (size == 0) {
        ALOGE("%s: write request of 0 bytes", __func__);
        goto send_unlock_exit;
    }

    // wait until previous transfer is finished 
    if (dev->interrupt_out_busy) {
        ALOGE("%s: abort transfer %d %d", __func__, dev->interrupt_out_busy, dev->interrupt_in_running);
        retval = -EAGAIN;
        dev->interrupt_out_busy = 0;
        eRGlass_usb_abort_transfers(dev);
        goto send_unlock_exit;
    }

    bytes_to_write = size;
    //memcpy(dev->interrupt_out_buffer, buf, bytes_to_write);
    if (copy_from_user (dev->interrupt_out_buffer, buf, bytes_to_write)) {
        retval = -EFAULT;
        ALOGE("%s: copy_from_user %d", __func__, retval);
        goto send_unlock_exit;
    }

    /* send off the urb */
    usb_fill_int_urb(dev->interrupt_out_urb,
             dev->udev,
             usb_sndintpipe(dev->udev, dev->interrupt_out_endpoint->bEndpointAddress),
             dev->interrupt_out_buffer,
             bytes_to_write,
             eRGlass_usb_interrupt_out_callback,
             dev,
             dev->interrupt_out_interval);

    dev->interrupt_out_busy = 1;
    wmb();

    retval = usb_submit_urb (dev->interrupt_out_urb, GFP_KERNEL);
    if (retval) {
       dev->interrupt_out_busy = 0;
       ALOGE("%s: couldn't submit interrupt_out_urb %d", __func__, retval);
       goto send_unlock_exit;
    }
    retval = bytes_to_write;

send_unlock_exit:
    mutex_unlock(&dev->lock);

    return retval;
}

static void led_update(struct eRGlass_usb *dev_eRGlass_usb)
{
    COMMAND eRGlassCommand;

    ALOGV("%s", __func__);

    eRGlassCommand.command = ERGLASSER_CMD_RGB;

    eRGlassCommand.data[0] = dev_eRGlass_usb->led.ledred; 
    eRGlassCommand.data[1] = dev_eRGlass_usb->led.ledgreen;
    eRGlassCommand.data[2] = dev_eRGlass_usb->led.ledblue;

    eRGlassCommand.len = 3;
    command_put(eRGlassCommand);
}

static ssize_t ledred_show(struct device *dev, struct device_attribute *a, char *buf)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%d\n%c", dev_eRGlass_usb->led.ledred, '\0');
}

static ssize_t ledred_store(struct device *dev, struct device_attribute *a, const char *buf, size_t count)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);
    long value;

    if (kstrtol(buf, 10, &value)) {
        ALOGW("%s(%s): kstrtol error", __func__, buf);
        return 0;
    }

    ALOGV("%s(%ld)", __func__, value);

    dev_eRGlass_usb->led.ledred = value;
    led_update(dev_eRGlass_usb);

    return count;
}

static ssize_t ledgreen_show(struct device *dev, struct device_attribute *a, char *buf)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%d\n%c", dev_eRGlass_usb->led.ledgreen, '\0');
}

static ssize_t ledgreen_store(struct device *dev, struct device_attribute *a, const char *buf, size_t count)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);
    long value;

    if (kstrtol(buf, 10, &value)) {
        ALOGW("%s(%s): kstrtol error", __func__, buf);
        return 0;
    }

    ALOGV("%s(%ld)", __func__, value);

    dev_eRGlass_usb->led.ledgreen = value;
    led_update(dev_eRGlass_usb);

    return count;
}

static ssize_t ledblue_show(struct device *dev, struct device_attribute *a, char *buf)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%d\n%c", dev_eRGlass_usb->led.ledblue, '\0');    
}

static ssize_t ledblue_store(struct device *dev, struct device_attribute *a, const char *buf, size_t count)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);
    long value;

    if (kstrtol(buf, 10, &value)) {
        ALOGW("%s(%s): kstrtol error", __func__, buf);
        return 0;
    }

    ALOGV("%s(%ld)", __func__, value);

    dev_eRGlass_usb->led.ledblue = value;
    led_update(dev_eRGlass_usb);

    return count;
}

static ssize_t haptic_store(struct device *dev, struct device_attribute *a, const char *buf, size_t count)
{
    COMMAND eRGlassCommand;
    char *p;
    long ms;

    if (kstrtol(buf, 10, &ms)) {
        ALOGW("%s(%s): kstrtol error", __func__, buf);
        return 0;
    }

    ALOGV("%s(%ld)", __func__, ms);

    eRGlassCommand.command = ERGLASSER_CMD_HAPTIC;

    p = &eRGlassCommand.data[0];
    *(u16 *)p = cpu_to_le16(1);    p+=sizeof(u16);
    *(u16 *)p = cpu_to_le16(ms);

    eRGlassCommand.len = 4;
    command_put(eRGlassCommand);

    return count;
}

static ssize_t imuacc_show(struct device *dev, struct device_attribute *a, char *buf)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%d,%d,%d\n%c", dev_eRGlass_usb->imu.imu_accX, dev_eRGlass_usb->imu.imu_accY, dev_eRGlass_usb->imu.imu_accZ, '\0');
}

static ssize_t imugyro_show(struct device *dev, struct device_attribute *a, char *buf)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%d,%d,%d\n%c", dev_eRGlass_usb->imu.imu_gyroX, dev_eRGlass_usb->imu.imu_gyroY, dev_eRGlass_usb->imu.imu_gyroZ, '\0');
}

static ssize_t imumag_show(struct device *dev, struct device_attribute *a, char *buf)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%d,%d,%d\n%c", (short int)dev_eRGlass_usb->imu.imu_magX, (short int)dev_eRGlass_usb->imu.imu_magY, (short int)dev_eRGlass_usb->imu.imu_magZ, '\0');
}

static ssize_t compass_show(struct device *dev, struct device_attribute *a, char *buf)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%d\n%c", dev_eRGlass_usb->imu.compass, '\0');
}

static ssize_t asl_show(struct device *dev, struct device_attribute *a, char *buf)
{
    struct usb_interface *intf = to_usb_interface(dev);
    struct eRGlass_usb *dev_eRGlass_usb = usb_get_intfdata(intf);

    ALOGV("%s", __func__);

    return snprintf(buf, PAGE_SIZE, "%d\n%c", dev_eRGlass_usb->asl, '\0');
}

static struct device_attribute usb_device_attrs[] = {
    __ATTR(ledred, S_IRUGO | S_IWUGO, ledred_show, ledred_store),
    __ATTR(ledgreen, S_IRUGO | S_IWUGO, ledgreen_show, ledgreen_store),
    __ATTR(ledblue, S_IRUGO | S_IWUGO, ledblue_show, ledblue_store),
    __ATTR(imuacc, S_IRUGO, imuacc_show, NULL),
    __ATTR(imugyro, S_IRUGO, imugyro_show, NULL),
    __ATTR(imumag, S_IRUGO, imumag_show, NULL),
    __ATTR(compass, S_IRUGO, compass_show, NULL),
    __ATTR(asl, S_IRUGO, asl_show, NULL),    
    __ATTR(haptic, S_IWUGO, NULL, haptic_store),
};

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
static void fn_timer_cmdwrite(unsigned long handle)
{
    struct eRGlass_usb *dev = (void *)handle;
    static int timeStatus;
    static int alternate;

    ALOGV("%s", __func__);

    if(m_Connect == 0){
        return;
    }
    timeStatus += ERGLASS_TIME;
    if (timeStatus >= 100) {
        COMMAND commandeRGlass;
        timeStatus = 0;

	if (dev->swversion_valid == 0)
	{
            commandeRGlass.command = ERGLASSER_CMD_GETVERSION;
            commandeRGlass.len = 0;
	}
	else
	{
            commandeRGlass.command = ERGLASSER_CMD_GETSTATUS;
            if (dev->status & BIT_ERROR_PRESENCE) {
                if (++alternate & 0x1)
                    commandeRGlass.command = ERGLASSER_CMD_GETERRORS;
            }
            commandeRGlass.len = 0;
	}
        command_put(commandeRGlass);
    }

    queue_work(dev->workqueue, &dev->work_read);
}
#else
static void fn_timer_cmdwrite(struct timer_list *t)
{
    struct eRGlass_usb *dev = from_timer(dev, t, timer_read);
    static int timeStatus;
    static int alternate;

    ALOGV("%s", __func__);

    if (dev == NULL) {
        ALOGE("%s: no device installed!", __func__);
    }

    if(m_Connect == 0){
        return;
    }

    timeStatus += ERGLASS_TIME;
    if (timeStatus >= 100) {
        COMMAND commandeRGlass;
        timeStatus = 0;

	if (dev->swversion_valid == 0)
	{
            commandeRGlass.command = ERGLASSER_CMD_GETVERSION;
            commandeRGlass.len = 0;
	}
	else
	{
            commandeRGlass.command = ERGLASSER_CMD_GETSTATUS;
            if (dev->status & BIT_ERROR_PRESENCE) {
                if (++alternate & 0x1)
                    commandeRGlass.command = ERGLASSER_CMD_GETERRORS;
            }
            commandeRGlass.len = 0;
	}
        command_put(commandeRGlass);
    }

    queue_work(dev->workqueue, &dev->work_read);
}
#endif

static int CounterWaitCompletation = 0;
static void fn_work_cmdwrite(struct work_struct *work)
{
    struct eRGlass_usb *dev = container_of(work, struct eRGlass_usb, work_read);

    ALOGV("%s", __func__);

    aLogLevel = verbose;    /* Log level updating */

    if (dev->interrupt_out_busy == 1) {
        mod_timer(&dev->timer_read, jiffies + ERGLASS_TIMER_READ);
        if(CounterWaitCompletation++ > 30){
            ALOGV("\nFIORE fn_work_cmdwrite CounterWaitCompletation=%d\n", CounterWaitCompletation);
            eRGlass_usb_abort_transferFiore(dev);
            CounterWaitCompletation = 0;
        }
        return;
    } // if (dev->interrupt_out_busy == 1)

        CounterWaitCompletation = 0;

        // Test se comando in corso
#if 0
        if(dev->interrupt_in_urb->hcpriv == NULL) {
#endif
        if (command_is())
        {
            COMMAND commandeRGlass;
            int len;
            int result;

            command_get(&commandeRGlass);

            len = eRGlass_usb_prepareframe(commandeRGlass.command, commandeRGlass.data, commandeRGlass.len);
            ALOGV("%s: send message %02X (len = %u)",
                    __func__,
                    (unsigned int)commandeRGlass.command,
                    (unsigned int)commandeRGlass.len);
            if (visionar_lock(__func__) == 0) {
                if ((result = eRGlass_usb_send(dev, (unsigned char *)&m_frame, len)) > 0) {
                    // initialize in direction
                    if (read_timeout) {
                        dev->interrupt_in_timeout = jiffies + read_timeout * HZ / 1000;
                    }
                    dev->read_buffer_length = 0;
                    dev->read_packet_length = 0;
                    usb_fill_int_urb (dev->interrupt_in_urb,
                        dev->udev,
                        usb_rcvintpipe(dev->udev, dev->interrupt_in_endpoint->bEndpointAddress),
                        dev->interrupt_in_buffer,
                        usb_endpoint_maxp(dev->interrupt_in_endpoint),
                        eRGlass_usb_interrupt_in_callback,
                        dev,
                        dev->interrupt_in_interval);

                    mb();

                    if ((result = usb_submit_urb (dev->interrupt_in_urb, GFP_KERNEL)) != 0){
                        eRGlass_usb_abort_transfers(dev);
                        ALOGE("%s: usb_submit_urb failure = %d", __func__, result);
                    }
                }
                else {
                    visionar_unlock(__func__);
                    ALOGE("%s: eRGlass_usb_send failure = %d", __func__, result);
                }
            }
            else {
                ALOGE("%s: error on device locking", __func__);
            }
        } // if (command_is())
#if 0
    } else {
        if ((read_timeout == 0) || (dev->interrupt_in_timeout == 0) || (time_after(jiffies, dev->interrupt_in_timeout) != 0)) {
            ALOGI("%s: URB busy: kill URB", __func__);
            usb_kill_urb (dev->interrupt_in_urb);
        }
    }
#endif
    mod_timer(&dev->timer_read, jiffies + ERGLASS_TIMER_READ);
}

/**
 *    eRGlass_usb_probe
 *
 *    Called by the usb core when a new device is connected that it thinks
 *    this driver might be interested in.
 */
static int eRGlass_usb_probe (struct usb_interface *interface, const struct usb_device_id *id)
{
    struct device *idev = &interface->dev;
    struct usb_device *udev = interface_to_usbdev(interface);
    struct eRGlass_usb *dev = NULL;
    struct usb_host_interface *iface_desc;
    struct usb_endpoint_descriptor* endpoint;
    int i;
    int retval = -ENOMEM;
    int result;

    ALOGV("%s", __func__);

    /* allocate memory for our device state and initialize it */

    dev = kmalloc (sizeof(struct eRGlass_usb), GFP_KERNEL);
    if (dev == NULL) {
        dev_err(idev, "Out of memory\n");
        goto exit;
    }

    mutex_init(&dev->lock);

    dev->udev = udev;
    dev->open_count = 0;

    dev->read_buffer = NULL;
    dev->read_buffer_length = 0;
    dev->read_packet_length = 0;
    spin_lock_init (&dev->read_buffer_lock);
    dev->packet_timeout_jiffies = packet_timeout * HZ / 1000;
    dev->read_last_arrival = jiffies;

    init_waitqueue_head (&dev->read_wait);
    init_waitqueue_head (&dev->write_wait);

    dev->interrupt_in_buffer = NULL;
    dev->interrupt_in_endpoint = NULL;
    dev->interrupt_in_urb = NULL;
    dev->interrupt_in_running = 0;
    dev->interrupt_in_done = 0;
    dev->interrupt_in_timeout = 0;

    dev->interrupt_out_buffer = NULL;
    dev->interrupt_out_endpoint = NULL;
    dev->interrupt_out_urb = NULL;
    dev->interrupt_out_busy = 0;

    iface_desc = interface->cur_altsetting;

    /* set up the endpoint information */
    for (i = 0; i < iface_desc->desc.bNumEndpoints; ++i) {
        endpoint = &iface_desc->endpoint[i].desc;

        if (usb_endpoint_xfer_int(endpoint)) {
            if (usb_endpoint_dir_in(endpoint))
                dev->interrupt_in_endpoint = endpoint;
            else
                dev->interrupt_out_endpoint = endpoint;
        }
    }
    if(dev->interrupt_in_endpoint == NULL) {
        dev_err(idev, "interrupt in endpoint not found\n");
        goto error;
    }
    if (dev->interrupt_out_endpoint == NULL) {
        dev_err(idev, "interrupt out endpoint not found\n");
        goto error;
    }

    dev->read_buffer = kmalloc (read_buffer_size, GFP_KERNEL);
    if (!dev->read_buffer) {
        dev_err(idev, "Couldn't allocate read_buffer\n");
        goto error;
    }
    dev->interrupt_in_buffer = kmalloc (usb_endpoint_maxp(dev->interrupt_in_endpoint), GFP_KERNEL);
    if (!dev->interrupt_in_buffer) {
        dev_err(idev, "Couldn't allocate interrupt_in_buffer\n");
        goto error;
    }
    dev->interrupt_in_urb = usb_alloc_urb(0, GFP_KERNEL);
    if (!dev->interrupt_in_urb) {
        dev_err(idev, "Couldn't allocate interrupt_in_urb\n");
        goto error;
    }
    dev->interrupt_out_buffer = kmalloc (write_buffer_size, GFP_KERNEL);
    if (!dev->interrupt_out_buffer) {
        dev_err(idev, "Couldn't allocate interrupt_out_buffer\n");
        goto error;
    }
    dev->interrupt_out_urb = usb_alloc_urb(0, GFP_KERNEL);
    if (!dev->interrupt_out_urb) {
        dev_err(idev, "Couldn't allocate interrupt_out_urb\n");
        goto error;
    }
    dev->interrupt_in_interval = interrupt_in_interval ? interrupt_in_interval : dev->interrupt_in_endpoint->bInterval;
    dev->interrupt_out_interval = interrupt_out_interval ? interrupt_out_interval : dev->interrupt_out_endpoint->bInterval;

    /* we can register the device now, as it is ready */
    usb_set_intfdata (interface, dev);

    retval = usb_register_dev (interface, &eRGlass_usb_class);

    if (retval) {
        /* something prevented us from registering this driver */
        dev_err(idev, "Not able to get a minor for this device.\n");
        usb_set_intfdata (interface, NULL);
        goto error;
    }
    dev->minor = interface->minor;

    /* let the user know what node this device is now attached to */
    dev_info(&interface->dev,
            "VisionAR%d now attached to major %d minor %d\n",
            dev->minor,
            USB_MAJOR,
            dev->minor);

    for (i = 0; i < ARRAY_SIZE(usb_device_attrs); i++) {
        retval = device_create_file(&interface->dev, &usb_device_attrs[i]);
        if (retval) {
            ALOGW("%s: device_create_file(%s) failed (%d)", __func__, usb_device_attrs[i].attr.name, retval);
        }
    }

    /****************/
    /* init command */
    /****************/
    command_init();
    // command to request version
    {
        COMMAND commandeRGlass;
        commandeRGlass.command = ERGLASSER_CMD_GETVERSION;
        commandeRGlass.len = 0;
        command_put(commandeRGlass);
    }

    /****************/
    /* init keys    */
    /****************/
    if ((dev->keys_dev = input_allocate_device()) == NULL) {
        goto error;
    }
    dev->keys_dev->evbit[0] = BIT_MASK(EV_KEY);
    dev->keys_dev->name = "VisionAR keys";
    dev->keys_dev->id.bustype = BUS_HOST;
    dev->keys_dev->id.vendor = ERGLASSSER_VENDOR_ID;
    dev->keys_dev->id.product = ERGLASSSER_VPRODUCT_ID;
    dev->keys_dev->id.version = 0x0100;
    dev->keys_dev->keycode = (unsigned char *)tb_eRGlassKeys;
    dev->keys_dev->keycodesize = sizeof(unsigned char);
    dev->keys_dev->keycodemax = ARRAY_SIZE(tb_eRGlassKeys);    
    for (i = 0; i < dev->keys_dev->keycodemax; i++) 
        set_bit(tb_eRGlassKeys[i], dev->keys_dev->keybit);

    clear_bit(0, dev->keys_dev->keybit);

    if ((result = input_register_device(dev->keys_dev))) {
        ALOGE("%s: Unable to register input device error: %d", __func__, result);
        input_free_device(dev->keys_dev);
        goto error;
    }

    /***************/
    /* timer       */
    /***************/
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
    setup_timer(&dev->timer_read, fn_timer_cmdwrite, (unsigned long)dev);
#else
    timer_setup(&dev->timer_read, fn_timer_cmdwrite, 0);
    /* the third argument may include TIMER_* flags */
    /* ... */
#endif
    mod_timer(&dev->timer_read, jiffies + ERGLASS_TIMER_READ);
    INIT_WORK(&dev->work_read, fn_work_cmdwrite);
    dev->workqueue = create_singlethread_workqueue("eRGlassUsb");
    if (dev->workqueue == NULL) {
        ALOGE("%s: eRGlass_usb workqueue fail", __func__);
        goto error; 
    }
    m_Connect = 1;

exit:
    return retval;

error:
    eRGlass_usb_delete(dev);
    return retval;
}


/**
 *    eRGlass_usb_disconnect
 *
 *    Called by the usb core when the device is removed from the system.
 */
static void eRGlass_usb_disconnect (struct usb_interface *interface)
{
    struct eRGlass_usb *dev;
    int minor;
    int i;

    ALOGV("%s", __func__);

    m_Connect = 0;
    msleep(ERGLASS_TIME*2);

    dev = usb_get_intfdata (interface);
    mutex_lock(&open_disc_mutex);
    usb_set_intfdata (interface, NULL);

    minor = dev->minor;

    /* give back our minor */
    usb_deregister_dev (interface, &eRGlass_usb_class);

    mutex_lock(&dev->lock);

    input_unregister_device(dev->keys_dev);
    del_timer_sync(&dev->timer_read);

    mutex_unlock(&open_disc_mutex);

    for (i = 0; i < ARRAY_SIZE(usb_device_attrs); i++)
        device_remove_file(&interface->dev, &usb_device_attrs[i]);

    /* if the device is not opened, then we clean up right now */
    if (!dev->open_count) {
        mutex_unlock(&dev->lock);
        eRGlass_usb_delete (dev);
    } else {
        dev->udev = NULL;
        /* wake up pollers */
        wake_up_interruptible_all(&dev->read_wait);
        wake_up_interruptible_all(&dev->write_wait);
        mutex_unlock(&dev->lock);
    }

    dev_info(&interface->dev, "VisionAR%d now disconnected\n", minor);
}

module_usb_driver(eRGlass_usb_driver);

MODULE_VERSION("V4.0");
MODULE_AUTHOR("ETL Team <support@eyetechlab.com>");
MODULE_DESCRIPTION("VisionAR USB driver");
MODULE_LICENSE("GPL");
